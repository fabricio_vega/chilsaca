<div role="tabpanel" class="tab-pane" id="deductionsPanel">
	<h2 class="text-center menu-item">Generar deduciones</h2>
	
	<div class="panel-group" id="deductionsAccordion" role="tablist" aria-multiselectable="true">
		<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="deductionsAccordionOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#deductionsAccordion1" href="#deductionsInputs" aria-expanded="true" aria-controls="deductionsInputs">
			          	<?php echo $FirstPane;?>
			        </a>
	      		</h4>
	    	</div>
		    <div id="deductionsInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      	<div class="panel-body">
			     	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Número de cédula</div>
				      		<input type="text" id="deductions_identification" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite Número de cédula">
				    	</div>
				    	<label for="" style="color:#999; font-size:11px">Nota:<strong>Presione la tecla enter para buscar el colaborador según cédula</strong></label>
				      	<br /><br />
			    	</div>
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Colaborador</div>
				      		<input type="text" id="deductions_employee" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Colaborador" disabled>
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Tipo de deducción</div>
				      		<select class="form-control" name="typeofdeduction" id="deductions_typeofdeduction">
				      			
				      		</select>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Fecha de deducción</div>
				      		<input type="date" id="deductions_date" value="" class="form-control" aria-describedby="sizing-addon1">
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Se deduce por </div>
				      		<span class="glyphicon" aria-hidden="false"> 
					      		<label class="text-color" style="cursor: pointer"><input type="radio" id="deductions_rb_ammount" name="radio" style="cursor: pointer" checked> Monto</label> 
					      		<label class="text-color" style="cursor: pointer"><input type="radio" id="deductions_rb_percentage" name="radio" style="cursor: pointer"> Porcentaje</label>
					      	</span>
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Cantidad a deducir</div>
				      		<input type="text" id="deductions_quantity" value="0" class="form-control" aria-describedby="sizing-addon1" placeholder="Cantidad">
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Nota</div>
				      		<textarea type="text" id="deductions_note" value="1" min="1" class="form-control" aria-describedby="sizing-addon1" placeholder="Anota algo importante"></textarea>
				    	</div>
				      	<br />
			    	</div>
					<!-- FINISH TYPE OF INVOICE INPUT -->
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="deductions_btn_save" class="btn btn-info btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
								Guardar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="deductions_btn_clear" class="btn btn-dafault btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								Limpiar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="deductions_btn_filter" class="btn btn-warning btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-filte" aria-hidden="true"></span>
								Filtrar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="deductions_btn_search" class="btn btn-primary btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
								Buscar colaborador
							</button>
			  			</div>
			  		</div>
		     	</div>
		    </div>
	  	</div>
	  	<!-- FINISH REGISTER AND MODIFY REGISTERS -->

	  	<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
    		<div class="panel-heading" role="tab" id="deductionsAccordionTwo">
      			<h4 class="panel-title">
	        		<a role="button" data-toggle="collapse" data-parent="#deductionsAccordion2" href="#deductionsDataGridView" aria-expanded="false" aria-controls="deductionsDataGridView">
          				<?php echo $SecondPane . "deducciones";?>
        			</a>
      			</h4>
	    	</div>
	    	<div id="deductionsDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover" onload="selectAll();">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common hidden">
											Id
										</th>
										<th class="th-common">
											Cédula
										</th>
										<th class="th-common">
											Colaborador
										</th>
										<th class="th-common hidden">
											Código de deducción
										</th>
										<th class="th-common">
											Tipo de deducción
										</th>
										<th class="th-common">
											Deducido por
										</th>
										<th class="th-common">
											Cantidad a deducir
										</th>
										<th class="th-common">
											Nota
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_deductions" style="cursor:pointer;">

								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>
					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="deductions_btn_modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="deductions_btn_delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH TABLE RESPONSIVE -->
  	</div>
</div>