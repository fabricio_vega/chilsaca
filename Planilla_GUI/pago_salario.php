<div role="tabpanel" class="tab-pane active" id="salarypayPanel">
	<!-- DATAGRIDVIEW PANEL -->
	<h2 class="text-center menu-item">Generar pago salario</h2>
	<div class="panel-group" id="salarypayAccordion" role="tablist" aria-multiselectable="true">
		<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="salarypayAccordionOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#salarypayAccordion1" href="#salarypayInputs" aria-expanded="true" aria-controls="salarypayInputs">
			          	<?php echo $FirstPane?>
			        </a>
	      		</h4>
	    	</div>
	    	<div id="salarypayInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
	      		<div class="panel-body">
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Cédula</div>
				      		<input type="text" id="payrollEmployee_identification" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite la cédula">
				    	</div>
				      	<label for="" style="color:#999; font-size:11px">Nota:<strong>Presione la tecla enter para buscar el colaborador según cédula</strong></label>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Nombre</div>
				      		<input type="text" id="payrollEmployee_name" class="form-control" aria-describedby="sizing-addon1" placeholder="Nombre" disabled>
				    	</div>
				      	<br /><br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Apellidos</div>
				      		<input type="text" id="payrollEmployee_surnames" class="form-control" aria-describedby="sizing-addon1" placeholder="Apellidos" disabled>
				    	</div>
				      	<br /><br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Fecha del recibo</div>
				      		<input type="date" id="payrollEmployee_date_invoice" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Fecha">
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Jornada extraordinaria</div>
				      		<input type="number" id="payrollEmployee_extra_pay" value="0" min="0" class="form-control" aria-describedby="sizing-addon1" placeholder="Monto de las horas extra">
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Salario correspondiente</div>
				      		<input type="number" id="payrollEmployee_salary" value="" min="1" class="form-control" aria-describedby="sizing-addon1" placeholder="Monto del salario" disabled>
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Deducciones en el periódo</div>
				      		<input type="number" id="payrollEmployee_deductions" value="" min="0" class="form-control" aria-describedby="sizing-addon1" placeholder="Deducciones en el periódo" disabled>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Incapacidad a cancelar</div>
				      		<input type="number" id="payrollEmployee_inabilities" value="" min="0" class="form-control" aria-describedby="sizing-addon1" placeholder="Incapacidad a cancelar" disabled>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="payrollEmployee_btn_save" class="btn btn-info btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
								Guardar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="payrollEmployee_btn_clear" class="btn btn-dafault btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								Limpiar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="payrollEmployee_btn_filter" class="btn btn-warning btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-filter" aria-hidden="true"></span>
								Filtrar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="payrollEmployee_btn_search" class="btn btn-primary btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
								Buscar colaborador
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
		<!-- FINISH REGISTER AND MODIFY REGISTERS -->
		
		<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
	    	<div class="panel-heading" role="tab" id="salarypayAccordionTwo">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#salarypayAccordion2" href="#salarypayDataGridView" aria-expanded="false" aria-controls="salarypayDataGridView">
			          	<?php echo $SecondPane . "pagos de salario"?>
			        </a>
	      		</h4>
	    	</div>
	    	<div id="salarypayDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common">
											Cédula
										</th>
										<th class="th-common">
											Colaborador
										</th>
										<th class="th-common">
											Fecha del recibo 
										</th>
										<th class="th-common">
											Salario correspondiente
										</th>
										<th class="th-common">
											Total deducciones generadas
										</th>
										<th class="th-common">
											Total incapacidades generadas
										</th>
										<th class="th-common">
											Total salario a cancelado
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_payrollEmployee" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>
					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="payrollEmployee_btn_modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="payrollEmployee_btn_delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH DATAGRIDVIEW PANEL -->
	</div>
</div>