<div role="tabpanel" class="tab-pane" id="departmentAccordionPanel">
	<!-- DATAGRIDVIEW PANEL -->
	<h2 class="text-center menu-item">Departamentos</h2>
	<div class="panel-group" id="departmentAccordion" role="tablist" aria-multiselectable="true">
		<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="departmentAccordionOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#departmentAccordion1" href="#departmentInputs" aria-expanded="true" aria-controls="departmentInputs">
			          	<?php echo $FirstPane?>
			        </a>
	      		</h4>
	    	</div>
	    	<div id="departmentInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
	      		<div class="panel-body">
	        		<input type="text" id="provider-identification-old" value="" name="provider-identification-old" class="form-control" style="display:none">
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 ">
			    		<input type="text" id="code-name" value="" class="form-control hidden" aria-describedby="sizing-addon1">
			    		<div class="input-group">
				      		<div class="input-group-addon">Nombre del departamento</div>
				      		<input type="text" id="department-name" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite el nombre">
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="department-btn-save" class="btn btn-info btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
								Guardar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="department-btn-clear" class="btn btn-dafault btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								Limpiar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="department-btn-filter" class="btn btn-warning btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-filte" aria-hidden="true"></span>
								Filtrar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
		<!-- FINISH REGISTER AND MODIFY REGISTERS -->
		
		<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
	    	<div class="panel-heading" role="tab" id="departmentAccordionTwo">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#departmentAccordion2" href="#departmentDataGridView" aria-expanded="false" aria-controls="departmentDataGridView">
			          	<?php echo $SecondPane . "departamentos"?>
			        </a>
	      		</h4>
	    	</div>
	    	<div id="departmentDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0"> 
										<th class="th-common hidden">
											Código del departamento
										</th>
										<th class="th-common">
											Departamento		
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->
								
								<!-- TABLE BODY -->
								<tbody id="tbody-department" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>
					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="department-btn-modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="department-btn-delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="department-btn-refresh" class="btn btn-primary btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
								Refrescar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH DATAGRIDVIEW PANEL -->
	</div>
</div>