<div role="tabpanel" class="tab-pane" id="inabilityPanel">

	<h2 class="text-center menu-item">Generar incapacidad</h2>
	<div class="panel-group" id="inabilityAccordion" role="tablist" aria-multiselectable="true">
		<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="inabilityAccordionOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#inabilityAccordion1" href="#inabilityInputs" aria-expanded="true" aria-controls="inabilityInputs">
			          	<?php echo $FirstPane;?>
			        </a>
	      		</h4>
	    	</div>
		    <div id="inabilityInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      	<div class="panel-body">
			     	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Número de cédula</div>
				      		<input type="text" id="inability_identification" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite Número de cédula">
				    	</div>
				    	<label for="" style="color:#999; font-size:11px">Nota:<strong>Presione la tecla enter para buscar el colaborador según cédula</strong></label>
				      	<br /><br />
			    	</div>
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Colaborador</div>
				      		<input type="text" id="inability_employee" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Colaborador" disabled>
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Tipo de incapacidad</div>
				      		<select class="form-control" name="typeofinability" id="inability_typeofinability">
				      			
				      		</select>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Aplica desde</div>
				      		<input type="date" id="inability_from" value="" class="form-control" aria-describedby="sizing-addon1">
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Aplica hasta</div>
				      		<input type="date" id="inability_to" value="" class="form-control" aria-describedby="sizing-addon1">
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Nota</div>
				      		<textarea type="text" id="inability_note" value="1" min="1" class="form-control" aria-describedby="sizing-addon1" placeholder="Anota algo importante"></textarea>
				    	</div>
				      	<br />
			    	</div>
					<!-- FINISH TYPE OF INVOICE INPUT -->
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_save" class="btn btn-info btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
								Guardar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_clear" class="btn btn-dafault btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								Limpiar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_filter" class="btn btn-warning btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-filte" aria-hidden="true"></span>
								Filtrar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_search" class="btn btn-primary btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
								Buscar colaborador
							</button>
			  			</div>
			  		</div>
		     	</div>
		    </div>
	  	</div>
	  	<!-- FINISH REGISTER AND MODIFY REGISTERS -->

	  	<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
    		<div class="panel-heading" role="tab" id="inabilityAccordionTwo">
      			<h4 class="panel-title">
	        		<a role="button" data-toggle="collapse" data-parent="#inabilityAccordion2" href="#inabilityDataGridView" aria-expanded="false" aria-controls="inabilityDataGridView">
          				<?php echo $SecondPane . "incapacidades";?>
        			</a>
      			</h4>
	    	</div>
	    	<div id="inabilityDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover" onload="selectAll();">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common hidden">
											Id
										</th>
										<th class="th-common">
											Cédula
										</th>
										<th class="th-common">
											Colaborador
										</th>
										<th class="th-common hidden">
											Código incapacidad
										</th>
										<th class="th-common">
											Tipo de incapacidad
										</th>
										<th class="th-common">
											Aplica desde
										</th>
										<th class="th-common">
											Aplica hasta
										</th>
										<th class="th-common">
											Nota
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_inability" style="cursor:pointer;">

								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>
					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH TABLE RESPONSIVE -->
  	</div>
</div>	