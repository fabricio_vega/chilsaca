<div role="tabpanel" class="tab-pane" id="colaboratorPanel">
	<!-- DATAGRIDVIEW PANEL -->
	<h2 class="text-center menu-item">Colaboradores</h2>
	
	<div class="panel-group" id="colaboratorAccordion" role="tablist" aria-multiselectable="true">
		<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="colaboratorAccordionOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#colaboratorAccordion1" href="#colaboratorInputs" aria-expanded="true" aria-controls="colaboratorInputs">
			          	<?php echo $FirstPane?>
			        </a>
	      		</h4>
	    	</div>
	    	<div id="colaboratorInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
	      		<div class="panel-body">
	        		<input type="text" id="provider-identification-old" value="" name="provider-identification-old" class="form-control" style="display:none">
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Cédula</div>
				      		<input type="text" id="employee_identification" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite la cédula">
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Nombre</div>
				      		<input type="text" id="employee_name" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite el nombre">
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Apellidos</div>
				      		<input type="text" id="employee_surnames" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite el teléfono">
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Fecha de ingreso</div>
				      		<input type="date" id="employee_entry_date" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite un correo electrónico">
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Puesto</div>
				      		<select class="form-control" name="job" id="employee_job">
				      			
				      		</select>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Tipo de pago</div>
				      		<select class="form-control" name="typeofpay" id="employee_typeofpay">
				      			
				      		</select>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Salario</div>
				      		<input type="number" id="employee_salary" value="0" min="1" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite el monto de crédito">
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Tiene licencia </div>
				      		<span class="glyphicon" aria-hidden="false"> 
					      		<label class="text-color" style="cursor: pointer"><input type="radio" id="employee_rb_havelicence_yes" name="radio" style="cursor: pointer" checked> Sí</label> 
					      		<label class="text-color" style="cursor: pointer"><input type="radio" id="employee_rb_havelicence_no" name="radio" style="cursor: pointer"> No</label>
					      	</span>
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Fecha exp. licencia</div>
				      		<input type="date" id="employee_exp_licence"  class="form-control" aria-describedby="sizing-addon1" placeholder="Digite los días de gracia">
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Modalidad</div>
				      		<span class="glyphicon" aria-hidden="false"> 
					      		<label class="text-color" style="cursor: pointer"><input type="radio" id="employee_rb_insurance" name="licence" style="cursor: pointer" checked> Asegurado</label> 
					      		<label class="text-color" style="cursor: pointer"><input type="radio" id="employee_rb_professionaservice" name="licence" style="cursor: pointer"> Servicios profesionales</label>
					      	</span>
			      		</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="employee_btn_save" class="btn btn-info btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
								Guardar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="employee_btn_clear" class="btn btn-dafault btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								Limpiar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="employee_btn_filter" class="btn btn-warning btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-filte" aria-hidden="true"></span>
								Filtrar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
		<!-- FINISH REGISTER AND MODIFY REGISTERS -->
		
		<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
	    	<div class="panel-heading" role="tab" id="colaboratorAccordionTwo">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#colaboratorAccordion2" href="#colaboratorDataGridView" aria-expanded="false" aria-controls="colaboratorDataGridView">
			          	<?php echo $SecondPane . "colaboradores"?>
			        </a>
	      		</h4>
	    	</div>
	    	<div id="colaboratorDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common">
											Cédula
										</th>
										<th class="th-common">
											Nombre		
										</th>
										<th class="th-common">
											Apellidos 
										</th>
										<th class="th-common">
											Fecha de ingreso
										</th>
										<th class="th-common hidden">
											Código de puesto
										</th>
										<th class="th-common">
											Puesto
										</th>
										<th class="th-common hidden">
											Código tipo de pago
										</th>
										<th class="th-common">
											Tipo de pago
										</th>
										<th class="th-common">
											Salario
										</th>
										<th class="th-common">
											¿Tiene licencia?
										</th>
										<th class="th-common">
											Fecha exp. licencia
										</th>
										<th class="th-common">
											Modalidad
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_employee" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>
					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="employee_btn_modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="employee_btn_delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="employee_btn_refresh" class="btn btn-primary btn-lg" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
								Refrescar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH DATAGRIDVIEW PANEL -->
	</div>
</div>