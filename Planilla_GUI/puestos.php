<div role="tabpanel" class="tab-pane" id="jobsPanel">
	<!-- DATAGRIDVIEW PANEL -->
	<h2 class="text-center menu-item">Puestos</h2>
	<div class="panel-group" id="jobsAccordion" role="tablist" aria-multiselectable="true">
		<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="jobsAccordionOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#jobsAccordion1" href="#jobsInputs" aria-expanded="true" aria-controls="jobsInputs">
			          	<?php echo $FirstPane?>
			        </a>
	      		</h4>
	    	</div>
	    	<div id="jobsInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
	      		<div class="panel-body">
	        		<input type="text" id="job-code" value="" class="form-control hidden">
			    	<div class="col-xs-12 col-md-6 col-lg-6">
			    		<div class="input-group">
				      		<div class="input-group-addon">Nombre del puesto</div>
				      		<input type="text" id="job-name" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite el nombre">
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-6 col-lg-6">
			    		<div class="input-group">
				      		<div class="input-group-addon">Departamento</div>
				      		<select class="form-control" name="job" id="job-departament">
				      			
				      		</select>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="job-btn-save" class="btn btn-info btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
								Guardar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="job-btn-clear" class="btn btn-dafault btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								Limpiar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="job-btn-filter" class="btn btn-warning btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-filte" aria-hidden="true"></span>
								Filtrar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
		<!-- FINISH REGISTER AND MODIFY REGISTERS -->
		
		<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
	    	<div class="panel-heading" role="tab" id="jobsAccordionTwo">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#jobsAccordion2" href="#jobsDataGridView" aria-expanded="false" aria-controls="jobsDataGridView">
			          	<?php echo $SecondPane . "puestos"?>
			        </a>
	      		</h4>
	    	</div>
	    	<div id="jobsDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common hidden">
											Código puesto
										</th>
										<th class="th-common">
											Puesto
										</th>
										<th class="th-common hidden">
											Código del departamento 
										</th>
										<th class="th-common">
											Departamento 
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody-job" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>
					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="job-btn-modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="job-btn-delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="job-btn-refresh" class="btn btn-primary btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
								Refrescar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH DATAGRIDVIEW PANEL -->
	</div>
</div>