<?php
	include 'header.php';
	include 'top-bar.php';
	include 'message_render.php';
	$FirstPane = 'Nuevo registro';
	$SecondPane = 'Listado de ';
?>
	<div class="container-fluid back-to-session bottom-footer">
		<!-- WEBSITE TITTLE -->
		<div class="row principal-tittle">
			<ol class="breadcrumb">
			  	<li>
			  		<a href="modules.php">
			  			<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
			  			Módulos
			  		</a>
			  	</li>
			  	<li class="active">Tesorería</li>
			</ol>
			
			<h1 class="text-center first_tittle">
				Módulo de tesoreria
			</h1>
		</div>
		<!-- FINISH WEBSITE TITTLE -->

		<!-- PRINCIPAL PANEL TO FACTURATION -->
		<div class="container-fluid added-bar">
			<div class="row add-articles">
				<!-- MENU BAR PANEL -->
				<nav class="navbar navbar-default">
				  	<div class="container-fluid">
				    	<!-- Brand and toggle get grouped for better mobile display -->
				    	<div class="navbar-header">
					      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					        	<span class="sr-only">Menú</span>
					        	<span class="icon-bar"></span>
					        	<span class="icon-bar"></span>
					        	<span class="icon-bar"></span>
					      	</button>
				    	</div>
				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      	<ul class="nav nav-tabs" role="tablist">
				      		<li role="presentation" class="active"><a href="#applyCareersPanel" role="tab" id="dropdown0-tab-tesoreria" data-toggle="tab" aria-controls="dropdown0">Cierre de cajas</a></li>
						</ul>
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>
				<!-- FINISH MENU BAR PANEL -->
	
				<!-- Tab panes -->
			  	<div class="tab-content">

			  	</div>
			</div>
		</div>
		<!-- FINISH PRINCIPAL PANEL TO FACTURATION -->

		
	</div>
<?php
	include "footer-copyright.php";
	include "bootstrap_script.php";
?>


<?php
	include 'footer.php';
?>