-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-07-2016 a las 18:05:31
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `chilsaca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `binnacle_vacations`
--

CREATE TABLE IF NOT EXISTS `binnacle_vacations` (
  `id` int(11) NOT NULL,
  `id_person` varchar(30) NOT NULL,
  `begin_vacation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `finish_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `notes` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deductions`
--

CREATE TABLE IF NOT EXISTS `deductions` (
  `id` int(11) NOT NULL,
  `id_employee` varchar(30) NOT NULL,
  `id_typeofdeduction` int(11) NOT NULL,
  `request_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount_to_deduct` double NOT NULL,
  `actual_salary` double NOT NULL,
  `is_percentaje` tinyint(1) NOT NULL DEFAULT '0',
  `note` varchar(150) DEFAULT NULL,
  `was_it_applied` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `id` int(11) NOT NULL,
  `department_name` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `department`
--

INSERT INTO `department` (`id`, `department_name`) VALUES
(1, 'Administrativo'),
(2, 'Operativo'),
(3, 'Financiero'),
(6, 'Contable');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `identification` varchar(30) NOT NULL,
  `employee_name` varchar(30) NOT NULL,
  `surnames` varchar(40) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_job` int(11) NOT NULL,
  `salary` double NOT NULL,
  `have_licence` tinyint(1) DEFAULT '0',
  `expired_licence` timestamp NULL DEFAULT NULL,
  `id_typeofpay` int(11) NOT NULL,
  `is_it_insured` tinyint(1) NOT NULL DEFAULT '1',
  `is_active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inability`
--

CREATE TABLE IF NOT EXISTS `inability` (
  `id` int(11) NOT NULL,
  `id_employee` varchar(30) NOT NULL,
  `id_typeofinability` int(11) NOT NULL,
  `actual_salary` double NOT NULL,
  `begin_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `finish_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `notes` varchar(150) DEFAULT NULL,
  `was_it_applied` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `job`
--

CREATE TABLE IF NOT EXISTS `job` (
  `id` int(11) NOT NULL,
  `id_department` int(11) NOT NULL,
  `job_name` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `job`
--

INSERT INTO `job` (`id`, `id_department`, `job_name`) VALUES
(3, 1, 'Administrador'),
(4, 2, 'Chofer de bús'),
(5, 6, 'Contador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payroll`
--

CREATE TABLE IF NOT EXISTS `payroll` (
  `id` int(11) NOT NULL,
  `id_employee` varchar(30) NOT NULL,
  `employee_salary` double NOT NULL,
  `id_typeofpay` int(11) NOT NULL,
  `total_deductions` double NOT NULL,
  `extra_journy_amount` double NOT NULL DEFAULT '0',
  `inabilities` double NOT NULL,
  `pendient` double NOT NULL,
  `is_it_insured` tinyint(1) NOT NULL,
  `worker_percentage` double NOT NULL,
  `begin_date` date NOT NULL,
  `finish_date` date NOT NULL,
  `is_applied` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `salary_increase`
--

CREATE TABLE IF NOT EXISTS `salary_increase` (
  `id` int(11) NOT NULL,
  `id_employee` varchar(30) NOT NULL,
  `date_request` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `amount` double NOT NULL,
  `is_percentaje` tinyint(1) NOT NULL DEFAULT '1',
  `actual_amount` double NOT NULL,
  `notes` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `typeofdeduction`
--

CREATE TABLE IF NOT EXISTS `typeofdeduction` (
  `id` int(11) NOT NULL,
  `deduction_name` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `typeofdeduction`
--

INSERT INTO `typeofdeduction` (`id`, `deduction_name`) VALUES
(1, 'Embargo salarial'),
(2, 'Préstamo de dinero'),
(3, 'Jornada discontinua'),
(4, 'Pensión');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `typeofinability`
--

CREATE TABLE IF NOT EXISTS `typeofinability` (
  `id` int(11) NOT NULL,
  `inability_name` varchar(40) NOT NULL,
  `pay_percentage` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `typeofinability`
--

INSERT INTO `typeofinability` (`id`, `inability_name`, `pay_percentage`) VALUES
(1, 'Por CCSS', 34),
(2, 'Por INS', 30),
(3, 'Licencia por maternidad', 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `typeofjourny`
--

CREATE TABLE IF NOT EXISTS `typeofjourny` (
  `id` int(11) NOT NULL,
  `typeOfJourny_name` varchar(40) NOT NULL,
  `equivalence_percentage` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `typeofpay`
--

CREATE TABLE IF NOT EXISTS `typeofpay` (
  `id` int(11) NOT NULL,
  `pay_name` varchar(40) NOT NULL,
  `equivalence_days` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `typeofpay`
--

INSERT INTO `typeofpay` (`id`, `pay_name`, `equivalence_days`) VALUES
(1, 'Diario', 1),
(2, 'Semanal', 6),
(3, 'Quincenal', 15),
(4, 'Mensual', 30);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(30) NOT NULL,
  `username` varchar(50) NOT NULL,
  `pass` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `pass`) VALUES
('1234', 'fvega', '81dc9bdb52d04dc20036dbd8313ed055');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `binnacle_vacations`
--
ALTER TABLE `binnacle_vacations`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_VACATION_EMPLOYEE` (`id_person`);

--
-- Indices de la tabla `deductions`
--
ALTER TABLE `deductions`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_DEDUCTION_EMPLOYEE` (`id_employee`), ADD KEY `FK_DEDUCTION_TYPEOFDEDUCTION` (`id_typeofdeduction`);

--
-- Indices de la tabla `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`identification`), ADD KEY `FK_EMPLOYEE_JOB` (`id_job`), ADD KEY `FK_EMPLOYEE_PAY` (`id_typeofpay`);

--
-- Indices de la tabla `inability`
--
ALTER TABLE `inability`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_INABILITY_EMPLOYEE` (`id_employee`), ADD KEY `FK_INABILITY_TYPEOFINABILITY` (`id_typeofinability`);

--
-- Indices de la tabla `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_JOB_DEPARTMENT` (`id_department`);

--
-- Indices de la tabla `payroll`
--
ALTER TABLE `payroll`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_PAYROLL_CCSS_EMPLOYEE` (`id_employee`), ADD KEY `FK_PAYROLL_CCSS_TYPEOFPPAY` (`id_typeofpay`);

--
-- Indices de la tabla `salary_increase`
--
ALTER TABLE `salary_increase`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_salary_EMPLOYEE` (`id_employee`);

--
-- Indices de la tabla `typeofdeduction`
--
ALTER TABLE `typeofdeduction`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `typeofinability`
--
ALTER TABLE `typeofinability`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `typeofjourny`
--
ALTER TABLE `typeofjourny`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `typeofpay`
--
ALTER TABLE `typeofpay`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `binnacle_vacations`
--
ALTER TABLE `binnacle_vacations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `deductions`
--
ALTER TABLE `deductions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `inability`
--
ALTER TABLE `inability`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `job`
--
ALTER TABLE `job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `payroll`
--
ALTER TABLE `payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `salary_increase`
--
ALTER TABLE `salary_increase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `typeofdeduction`
--
ALTER TABLE `typeofdeduction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `typeofinability`
--
ALTER TABLE `typeofinability`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `typeofjourny`
--
ALTER TABLE `typeofjourny`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `typeofpay`
--
ALTER TABLE `typeofpay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `binnacle_vacations`
--
ALTER TABLE `binnacle_vacations`
ADD CONSTRAINT `FK_VACATION_EMPLOYEE` FOREIGN KEY (`id_person`) REFERENCES `employee` (`identification`);

--
-- Filtros para la tabla `deductions`
--
ALTER TABLE `deductions`
ADD CONSTRAINT `FK_DEDUCTION_EMPLOYEE` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`identification`) ON DELETE NO ACTION ON UPDATE CASCADE,
ADD CONSTRAINT `FK_DEDUCTION_TYPEOFDEDUCTION` FOREIGN KEY (`id_typeofdeduction`) REFERENCES `typeofdeduction` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `employee`
--
ALTER TABLE `employee`
ADD CONSTRAINT `FK_EMPLOYEE_JOB` FOREIGN KEY (`id_job`) REFERENCES `job` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
ADD CONSTRAINT `FK_EMPLOYEE_PAY` FOREIGN KEY (`id_typeofpay`) REFERENCES `typeofpay` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `inability`
--
ALTER TABLE `inability`
ADD CONSTRAINT `FK_INABILITY_EMPLOYEE` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`identification`) ON DELETE NO ACTION ON UPDATE CASCADE,
ADD CONSTRAINT `FK_INABILITY_TYPEOFINABILITY` FOREIGN KEY (`id_typeofinability`) REFERENCES `typeofinability` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `job`
--
ALTER TABLE `job`
ADD CONSTRAINT `FK_JOB_DEPARTMENT` FOREIGN KEY (`id_department`) REFERENCES `department` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `payroll`
--
ALTER TABLE `payroll`
ADD CONSTRAINT `FK_PAYROLL_CCSS_EMPLOYEE` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`identification`) ON DELETE NO ACTION ON UPDATE CASCADE,
ADD CONSTRAINT `FK_PAYROLL_CCSS_TYPEOFPPAY` FOREIGN KEY (`id_typeofpay`) REFERENCES `typeofpay` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `salary_increase`
--
ALTER TABLE `salary_increase`
ADD CONSTRAINT `FK_salary_EMPLOYEE` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`identification`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
