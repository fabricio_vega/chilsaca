<?php
	include 'header.php';
	include 'top-bar.php';
	include 'message_render.php';
	$FirstPane  = 'Nuevo registro';
	$SecondPane = 'Listado de ';
?>
	<div class="container-fluid back-to-session bottom-footer">
		<!-- WEBSITE TITTLE -->
		<div class="row principal-tittle">
			<ol class="breadcrumb">
			  	<li>
			  		<a href="modules.php">
			  			<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
			  			Módulos
			  		</a>
			  	</li>
			  	<li class="active">Parámetros generales</li>
			</ol>
			
			<h1 class="text-center first_tittle">
				Módulo de parámetros generales
			</h1>
		</div>
		<!-- FINISH WEBSITE TITTLE -->
		
		<!-- PRINCIPAL PANEL TO FACTURATION -->
		<div class="container-fluid added-bar">
			<div class="row add-articles">
				<!-- TAB PANES -->
			  	<div class="tab-content">
			  		
			  	</div>
			  	<!-- END TAB PANES -->
			</div>
		</div>
		<!-- FINISH PRINCIPAL PANEL TO FACTURATION -->
	</div>
<?php
	include "footer-copyright.php";
	include "bootstrap_script.php";
	include 'footer.php';
?>