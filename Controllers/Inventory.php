<?php
	require_once('../DataAccess/Inventory.php');
	$action = $_POST['action'];

	switch($action)
	{
		case 'insert':
			$code               = $_POST['code'];
			$articleDescription = $_POST['articleDescription'];
			$articlePrice1      = $_POST['articlePrice1'];
			$articlePrice2      = $_POST['articlePrice2'];
			$articlePrice3      = $_POST['articlePrice3'];
			$articlePrice4      = $_POST['articlePrice4'];
			$articleFreeTax     = $_POST['articleFreeTax'];

			$instance = new inventory();
			$result    = $instance->Insert(
										$code, 
										$articleDescription,
										$articlePrice1,
										$articlePrice2,
										$articlePrice3,
										$articlePrice4,
										$articleFreeTax);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}

		break;

		case 'modify':
			$code               = $_POST['code'];
			$OldCode            = $_POST['OldCode'];
			$articleDescription = $_POST['articleDescription'];
			$articlePrice1      = $_POST['articlePrice1'];
			$articlePrice2      = $_POST['articlePrice2'];
			$articlePrice3      = $_POST['articlePrice3'];
			$articlePrice4      = $_POST['articlePrice4'];
			$articleFreeTax     = $_POST['articleFreeTax'];

			$instance = new inventory();
			$result    = $instance->Modify(
										$code, 
										$OldCode,
										$articleDescription,
										$articlePrice1,
										$articlePrice2,
										$articlePrice3,
										$articlePrice4,
										$articleFreeTax);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'delete':
			$code = $_POST['code'];

			$instance = new inventory();
			$result = $instance->Delete($code);
			if($result === false)
			{
				echo "correcto";
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;
		
		case 'select':
			$instance = new inventory();
			echo json_encode($instance->SelectAll(), true);
		break;

		case 'insertAddManual':
			$date        = $_POST['date'];
			$articleCode = $_POST['articleCode'];
			$quantity    = $_POST['quantity'];
			$note        = $_POST['note'];

			$instance = new inventory();
			$result   = $instance->InsertAddManualInventory(
														$date, 
														$articleCode,
														$quantity,
														$note);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'modifyAddManual':
			$Id          = $_POST['Id'];
			$date        = $_POST['date'];
			$articleCode = $_POST['articleCode'];
			$quantity    = $_POST['quantity'];
			$note        = $_POST['note'];

			$instance = new inventory();
			$result   = $instance->ModifyAddManualInventory(
														$Id,
														$date, 
														$articleCode, 
														$quantity, 
														$note);
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'deleteAddManual':
			$Id = $_POST['Id'];

			$instance = new inventory();
			$result = $instance->DeleteAddManualInventory($Id);
			if($result === false)
			{
				echo "correcto";
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'selectAllAddManual':
			$instance = new inventory();
			echo json_encode($instance->SelectAllAddManual(), true);
		break;

		case 'selectArticleFilter':
			$key = $_POST['key'];
			$instance = new inventory();
			echo json_encode($instance->SelectArticleFilter($key), true);
		break;

		case 'selectProviderFilter':
			$key = $_POST['key'];
			$instance = new inventory();
			echo json_encode($instance->SelectProviderFilter($key), true);
		break;
	}
?>