<?php
	require_once('../DataAccess/Provider.php');
	$action = $_POST['action'];

	switch($action)
	{
		case 'insert':
			$identification = $_POST['identification'];
			$name_suplier   = $_POST['name_suplier'];
			$phone          = $_POST['phone'];
			$email          = $_POST['email'];
			$ubication      = $_POST['ubication'];
			$credit_amount  = $_POST['credit_amount'];
			$credit_days    = $_POST['credit_days'];
			$tanks_days     = $_POST['tanks_days'];

			$instance = new provider();
			$result    = $instance->Insert(
										$identification, 
										$name_suplier,
										$phone,
										$email,
										$ubication,
										$credit_amount,
										$credit_days,
										$tanks_days);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'modify':
			$identification    = $_POST['identification'];
			$identificationOld = $_POST['identificationOld'];
			$name_suplier      = $_POST['name_suplier'];
			$phone             = $_POST['phone'];
			$email             = $_POST['email'];
			$ubication         = $_POST['ubication'];
			$credit_amount     = $_POST['credit_amount'];
			$credit_days       = $_POST['credit_days'];
			$tanks_days        = $_POST['tanks_days'];

			$instance = new provider();
			$result    = $instance->Modify(
										$identification,
										$identificationOld, 
										$name_suplier,
										$phone,
										$email,
										$ubication,
										$credit_amount,
										$credit_days,
										$tanks_days);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'delete':
			$identification = $_POST['identification'];

			$instance = new provider();
			$result = $instance->Delete($identification);
			if($result === false)
			{
				echo "correcto";
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'selectProvider':
			$instance = new provider();
			echo json_encode($instance->SelectAll(), true);
		break;

		case 'insertPayment':
			$number  = $_POST['paymentNumber'];
			$invoice = $_POST['paymentInvoice'];
			$date    = $_POST['paymentDate'];
			$amount  = $_POST['paymentAmount'];
			$note    = $_POST['paymentNote'];
			
			$instance = new provider();
			$result   = $instance->InsertPayment(
										$number, 
										$invoice,
										$date,
										$amount,
										$note);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'modifyPayment':
			$number    = $_POST['paymentNumber'];
			$numberOld = $_POST['paymentNumberOld'];
			$invoice   = $_POST['paymentInvoice'];
			$date      = $_POST['paymentDate'];
			$amount    = $_POST['paymentAmount'];
			$note      = $_POST['paymentNote'];
			$instance = new provider();
			$result   = $instance->ModifyPayment(
										$number, 
										$numberOld,
										$invoice,
										$date,
										$amount,
										$note);

			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'deletePayment':
			$number    = $_POST['paymentNumber'];
			$instance = new provider();
			$result   = $instance->DeletePayment($number);

			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'selectAllPayments':
			$instance = new provider();
			echo json_encode($instance->SelectAllPayments(), true);
		break;

		case 'invoiceProvider':
			$provider = $_POST['provider'];

			$instance = new provider();
			$result   = $instance->SelectAllInvoicesByFilter($provider);
			echo json_encode($result);
		break;

		case 'insertNC':
			$ncNumber = $_POST['ncNumber'];
			$date     = $_POST['date'];
			$invoice  = $_POST['invoice'];
			$amount   = $_POST['amount'];
			$note     = $_POST['note'];
			
			$instance = new provider();
			$result   = $instance->InsertNC(
										$ncNumber, 
										$date,
										$invoice,
										$amount,
										$note);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'modifyNC':
			$oldNumber = $_POST['oldNumber'];
			$ncNumber = $_POST['ncNumber'];
			$date     = $_POST['date'];
			$invoice  = $_POST['invoice'];
			$amount   = $_POST['amount'];
			$note     = $_POST['note'];
			
			$instance = new provider();
			$result   = $instance->ModifyNC(
										$oldNumber,
										$ncNumber, 
										$date,
										$invoice,
										$amount,
										$note);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;
		
		case 'deleteNC':
			$ncNumber     = $_POST['ncNumber'];
			
			$instance = new provider();
			$result   = $instance->DeleteNC($ncNumber);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'selectAllNC':
			$instance = new provider();
			echo json_encode($instance->SelectAllNC(), true);
		break;

		case 'insertND':
			$ndNumber = $_POST['ndNumber'];
			$date     = $_POST['date'];
			$invoice  = $_POST['invoice'];
			$amount   = $_POST['amount'];
			$note     = $_POST['note'];
			
			$instance = new provider();
			$result   = $instance->InsertND(
										$ndNumber, 
										$date,
										$invoice,
										$amount,
										$note);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'modifyND':
			$oldNumber = $_POST['oldNumber'];
			$ndNumber  = $_POST['ndNumber'];
			$date      = $_POST['date'];
			$invoice   = $_POST['invoice'];
			$amount    = $_POST['amount'];
			$note      = $_POST['note'];
			
			$instance = new provider();
			$result   = $instance->ModifyND(
										$oldNumber,
										$ndNumber, 
										$date,
										$invoice,
										$amount,
										$note);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'deleteND':
			$ndNumber     = $_POST['ndNumber'];
			
			$instance = new provider();
			$result   = $instance->DeleteND($ndNumber);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'selectAllND':
			$instance = new provider();
			echo json_encode($instance->SelectAllND(), true);
		break;
	}
?>