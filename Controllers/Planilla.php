<?php
		require_once('../DataAccess/Planilla.php');
	$action = $_POST["action"];

	switch($action){
		case 'insertDepartment':
			$name = $_POST['name'];
			$instance = new planilla();
			$result   = $instance->InsertDepartment($name);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'modifyDepartment':
			$code = $_POST['code'];
			$name = $_POST['name'];
			$instance = new planilla();
			$result   = $instance->ModifyDepartment($code, $name);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'deleteDepartment':
			$code = $_POST['code'];
			$instance = new planilla();
			$result   = $instance->DeleteDepartment($code);
			if($result === false)
			{
				echo "correcto";
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'selectDepartment':
			$instance = new planilla();
			echo json_encode($instance->SelectAllDepartment(), true);
		break;

		case 'SelectAllDepartmentByName':
			$name = $_POST['name'];
			$instance = new planilla();
			echo json_encode($instance->SelectAllDepartmentByName($name), true);
		break;

		case 'insertJob':
			$name = $_POST['name'];
			$department = $_POST['department'];
			$instance   = new planilla();
			$result     = $instance->InsertJob($name, $department);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'modifyJob':
			$code = $_POST['code'];
			$name = $_POST['name'];
			$department = $_POST['department'];
			$instance   = new planilla();
			$result     = $instance->ModifyJob($code, $name, $department);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'deleteJob':
			$code     = $_POST['code'];
			$instance = new planilla();
			$result   = $instance->DeleteJob($code);
			if($result === false)
			{
				echo "correcto";
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'selectJob':
			$instance = new planilla();
			echo json_encode($instance->SelectAllJob(), true);
		break;

		case 'selectjobdepartment':
			$instance = new planilla();
			echo json_encode($instance->SelectAllDepartment(), true);
		break;

		case 'SelectFilterJobByName':
			$name     = $_POST['name'];
			$instance = new planilla();
			echo json_encode($instance->SelectAllJobByName($name), true);
		break;

		case 'SelectTypeOfPay':
			$instance = new planilla();
			echo json_encode($instance->SelectAllTypeOfPay(), true);
		break;

		case 'insertTypeOfPay':
			$name = $_POST['name'];
			$equivalence = $_POST['equivalence'];
			$instance    = new planilla();
			$result      = $instance->InsertTypeOfPay($name, $equivalence);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'modifyTypeOfPay':
			$code = $_POST['code'];
			$name = $_POST['name'];
			$equivalence = $_POST['equivalence'];
			$instance    = new planilla();
			$result      = $instance->ModifyTypeOfPay($code, $name, $equivalence);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'deleteTypeOfPay':
			$code     = $_POST['code'];
			$instance = new planilla();
			$result   = $instance->DeleteTypeOfPay($code);
			if($result === false)
			{
				echo "correcto";
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'SelectTypeOfDeduction':
			$instance = new planilla();
			echo json_encode($instance->SelectAllTypeOfDeduction(), true);
		break;

		case 'insertTypeOfDeduction':
			$name = $_POST['name'];
			$instance = new planilla();
			$result   = $instance->InsertTypeOfDeduction($name);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'modifyTypeOfDeduction':
			$code = $_POST['code'];
			$name = $_POST['name'];
			$instance = new planilla();
			$result   = $instance->ModifyTypeOfDeduction($code, $name);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'deleteTypeOfDeduction':
			$code     = $_POST['code'];
			$instance = new planilla();
			$result   = $instance->DeleteTypeOfDeduction($code);
			if($result === false)
			{
				echo "correcto";
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'SelectTypeOfInability':
			$instance = new planilla();
			echo json_encode($instance->SelectAllTypeOfInability(), true);
		break;

		case 'insertTypeOfInability':
			$name       = $_POST['name'];
			$percentage = $_POST['percentage'];
			$instance   = new planilla();
			$result     = $instance->InsertTypeOfInability($name, $percentage);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'modifyTypeOfInability':
			$code       = $_POST['code'];
			$name       = $_POST['name'];
			$percentage = $_POST['percentage'];
			$instance   = new planilla();
			$result     = $instance->ModifyTypeOfInability($code, $name, $percentage);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'deleteTypeOfInability':
			$code     = $_POST['code'];
			$instance = new planilla();
			$result   = $instance->DeleteTypeOfInability($code);
			if($result === false)
			{
				echo "correcto";
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'SelectColaborators':
			$instance = new planilla();
			echo json_encode($instance->SelectAllColaborator(), true);
		break;

		case 'insertColaborator':
			$cedula        = $_POST['cedula'];
			$name          = $_POST['name'];
			$surname       = $_POST['surname'];
			$fechaIngreso  = $_POST['fechaIngreso'];
			$puesto        = $_POST['puesto'];
			$tipoPago      = $_POST['tipoPago'];
			$salario       = $_POST['salario'];
			$tieneLicencia = $_POST['tieneLicencia'];
			$fechaExp      = $_POST['fechaExp'];
			$modalidad     = $_POST['modalidad'];

			$instance = new planilla();
			$result   = $instance->InsertColaborator(
													$cedula, 
													$name, 
													$surname, 
													$fechaIngreso, 
													$puesto, 
													$tipoPago, 
													$salario, 
													$tieneLicencia, 
													$fechaExp, 
													$modalidad);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;

		case 'modifyColaborator':
			$cedulaToChange= $_POST['cedulaToChange'];
			$cedula        = $_POST['cedula'];
			$name          = $_POST['name'];
			$surname       = $_POST['surname'];
			$fechaIngreso  = $_POST['fechaIngreso'];
			$puesto        = $_POST['puesto'];
			$tipoPago      = $_POST['tipoPago'];
			$salario       = $_POST['salario'];
			$tieneLicencia = $_POST['tieneLicencia'];
			$fechaExp      = $_POST['fechaExp'];
			$modalidad     = $_POST['modalidad'];

			$instance = new planilla();
			$result   = $instance->ModifyColaborator(
													$cedulaToChange,
													$cedula, 
													$name, 
													$surname, 
													$fechaIngreso, 
													$puesto, 
													$tipoPago, 
													$salario, 
													$tieneLicencia, 
													$fechaExp, 
													$modalidad);
			
			if(!$result)
			{
				echo 'correcto';
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;
		
		case 'deleteColaborator':
			$code     = $_POST['code'];
			$instance = new planilla();
			$result   = $instance->DeleteColaborator($code);
			if($result === false)
			{
				echo "correcto";
			}
			else
			{
				echo $instance->GetErrorMessage();
			}
		break;
		case 'searchByIdColaborator':
			$code     = $_POST['code'];
			$instance = new planilla();
			echo json_encode($instance->SearchColaborator($code), true);
		break;
	}
?>