<!-- PANE ADD RUTAS -->
<div role="tabpanel" class="tab-pane active" id="bankInMoneyPanel">
	<h2 class="text-center menu-item">Ingreso de dinero</h2>
	
	<div class="panel-group" id="bankInMoneyAccordion" role="tablist" aria-multiselectable="true">
	  	<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="headingOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#bankInMoneyAccordion1" href="#bankInMoneyInputs" aria-expanded="true" aria-controls="bankInMoneyInputs">
			          	<?php echo $FirstPane;?>
			        </a>
	      		</h4>
	    	</div>
		    <div id="bankInMoneyInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      	<div class="panel-body">
		      		<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
			    		<div class="input-group">
				      		<div class="input-group-addon">Número de documento</div>
			    			<input type="text" id="appliedCareers_downTime" value="" class="form-control" aria-label="">
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
			    		<div class="input-group">
				      		<div class="input-group-addon">Fecha de movimiento</div>
			    			<input type="date" id="appliedCareers_downTime" value="" class="form-control" aria-label="">
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
			    		<div class="input-group">
				      		<div class="input-group-addon">Monto a ingresar</div>
			    			<input type="number" id="appliedCareers_downTime" value="" class="form-control" aria-label="">
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
			    		<div class="input-group">
				      		<div class="input-group-addon">Nota</div>
			    			<textarea type="number" id="appliedCareers_downTime" value="" class="form-control" aria-label=""></textarea>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
			    		<div class="input-group">
				      		<div class="input-group-addon">Cuenta bancaria</div>
			    			<select class="form-control" name="driver" id="appliedCareers_upTime">

				      		</select>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="liquidation_btn_save" class="btn btn-info btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
								Guardar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="liquidation_btn_clear" class="btn btn-dafault btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								Limpiar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="appliedCareers_btn_filter" class="btn btn-warning btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
							  	<span class="glyphicon glyphicon-filte" aria-hidden="true"></span>
								Filtrar
							</button>
			  			</div>
			  		</div>
		     	</div>
		    </div>
	  	</div>
	  	<!-- FINISH REGISTER AND MODIFY REGISTERS -->

	  	<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
    		<div class="panel-heading" role="tab" id="headingTwo">
      			<h4 class="panel-title">
	        		<a role="button" data-toggle="collapse" data-parent="#bankInMoneyAccordion2" href="#bankInMoneyDataGridView" aria-expanded="true" aria-controls="bankInMoneyDataGridView">
          				<?php echo $SecondPane . "ingresos de dinero";?>
        			</a>
      			</h4>
	    	</div>
	    	<div id="bankInMoneyDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
	    		<div class="panel-body">
	        		
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common hidden">
											Código ingreso
										</th>
										<th class="th-common">
											Número de documento
										</th>
										<th class="th-common">
											Fecha
										</th>
										<th class="th-common">
											Monto
										</th>
										<th class="th-common">
											Nota
										</th>
										<th class="th-common">
											Cuenta bancaria
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_appliedCareers" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>
					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH TABLE RESPONSIVE -->
	</div>
</div>
<!-- END PANE ADD RUTAS -->