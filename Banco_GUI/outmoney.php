<!-- PANE ADD RUTAS -->
<div role="tabpanel" class="tab-pane" id="bankOutMoneyPanel">
	<h2 class="text-center menu-item">Salida de dinero</h2>
	
	<div class="panel-group" id="bankOutMoneyAcordion" role="tablist" aria-multiselectable="true">
	  	<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="headingOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#bankOutMoneyAcordion1" href="#bankOutMoneyInputs" aria-expanded="true" aria-controls="bankOutMoneyInputs">
			          	<?php echo $FirstPane;?>
			        </a>
	      		</h4>
	    	</div>
		    <div id="bankOutMoneyInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      	<div class="panel-body">
		      		<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Número de documento</div>
			    			<input type="text" id="appliedCareers_downTime" value="" class="form-control" aria-label="">
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Fecha de documento</div>
			    			<input type="date" id="appliedCareers_downTime" value="" class="form-control" aria-label="">
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Cuenta bancaria</div>
			    			<select class="form-control" name="driver" id="appliedCareers_upTime">

				      		</select>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Pago a</div>
			    			<input type="text" id="appliedCareers_downTime" value="" class="form-control" aria-label="">
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Monto del pago</div>
			    			<input type="number" id="appliedCareers_downTime" value="" class="form-control" aria-label="">
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Nota</div>
			    			<textarea class="form-control" name="driver" id="appliedCareers_upTime">
				      		</textarea>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="liquidation_btn_save" class="btn btn-info btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
								Guardar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="liquidation_btn_clear" class="btn btn-dafault btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								Limpiar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="appliedCareers_btn_filter" class="btn btn-warning btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
							  	<span class="glyphicon glyphicon-filte" aria-hidden="true"></span>
								Filtrar
							</button>
			  			</div>
			  		</div>
		     	</div>
		    </div>
	  	</div>
	  	<!-- FINISH REGISTER AND MODIFY REGISTERS -->

	  	<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
    		<div class="panel-heading" role="tab" id="headingTwo">
      			<h4 class="panel-title">
	        		<a role="button" data-toggle="collapse" data-parent="#bankOutMoneyAcordion2" href="#bankOutMoneyDataGridView" aria-expanded="true" aria-controls="bankOutMoneyDataGridView">
          				<?php echo $SecondPane . "salidas de dinero";?>
        			</a>
      			</h4>
	    	</div>
	    	<div id="bankOutMoneyDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
	      		<div class="panel-body">
	      			<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common hidden">
											Código de la carrera
										</th>
										<th class="th-common hidden">
											Código de ruta
										</th>
										<th class="th-common">
											Ruta
										</th>
										<th class="th-common hidden">
											Código de chofer
										</th>
										<th class="th-common">
											Nombre del chófer
										</th>
										<th class="th-common">
											Placa del bús
										</th>
										<th class="th-common">
											Hora de subida
										</th>
										<th class="th-common">
											Hora de bajada
										</th>
										<th class="th-common">
											Total de ingresos
										</th>
										<th class="th-common">
											Total de gastos
										</th>
										<th class="th-common hidden">
											state
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_appliedCareers" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>

					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH TABLE RESPONSIVE -->
	</div>
</div>
<!-- END PANE ADD RUTAS -->