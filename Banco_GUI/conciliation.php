<!-- PANE ADD RUTAS -->
<div role="tabpanel" class="tab-pane" id="bankConciliationPanel">
	<h2 class="text-center menu-item">Conciliación bancaria</h2>
	
	<div class="panel-group" id="bankConciliationAccordion" role="tablist" aria-multiselectable="true">
	  	<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="headingOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#bankConciliationAccordion1" href="#bankConciliationInputs" aria-expanded="true" aria-controls="bankConciliationInputs">
			          	<?php echo $FirstPane;?>
			        </a>
	      		</h4>
	    	</div>
		    <div id="bankConciliationInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      	<div class="panel-body">
		      		<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
			    		<div class="input-group">
				      		<div class="input-group-addon">Cuenta bancaria</div>
			    			<select class="form-control" name="driver" id="appliedCareers_driver">

				      		</select>
				      	</div>
				      	<br />
			    	</div>
		      		<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
			    		<div class="input-group">
				      		<div class="input-group-addon">Cargar archivo</div>
			    			<input type="button" id="appliedCareers_downTime" value="" class="form-control" aria-label="">
			    			<div class="input-group-addon"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span></div>
				      	</div>
				      	<br />
			    	</div>
		     	</div>
		    </div>
	  	</div>
	  	<!-- FINISH REGISTER AND MODIFY REGISTERS -->

	  	<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
    		<div class="panel-heading" role="tab" id="headingTwo">
      			<h4 class="panel-title">
	        		<a role="button" data-toggle="collapse" data-parent="#bankConciliationAccordion2" href="#bankConciliationDataGridView" aria-expanded="true" aria-controls="bankConciliationDataGridView">
          				<?php echo $SecondPane . "ingresos manuales";?>
        			</a>
      			</h4>
	    	</div>
	    	<div id="bankConciliationDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common">
											Número de documento
										</th>
										<th class="th-common">
											Ingreso
										</th>
										<th class="th-common">
											Salida
										</th>
										<th class="th-common">
											Saldo
										</th>
										<th class="th-common info">
											
										</th>
										<th class="th-common">
											Número de documento
										</th>
										<th class="th-common">
											Ingreso
										</th>
										<th class="th-common">
											Salida
										</th>
										<th class="th-common">
											Saldo
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_appliedCareers" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH TABLE RESPONSIVE -->
	</div>
</div>
<!-- END PANE ADD RUTAS -->