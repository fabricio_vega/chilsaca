<!-- PANE ADD RUTAS -->
<div role="tabpanel" class="tab-pane" id="bankAccountsPanel">
	<h2 class="text-center menu-item">Cuentas bancarias</h2>
	
	<div class="panel-group" id="bankAccountsAccordion" role="tablist" aria-multiselectable="true">
	  	<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="headingOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#bankAccountsAccordion1" href="#bankAccountsInputs" aria-expanded="true" aria-controls="bankAccountsInputs">
			          	<?php echo $FirstPane;?>
			        </a>
	      		</h4>
	    	</div>
		    <div id="bankAccountsInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      	<div class="panel-body">
		      		<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-8">
			    		<div class="input-group">
				      		<div class="input-group-addon">Cuenta del activo</div>
			    			<input type="text" id="appliedCareers_downTime" value="" class="form-control" aria-label="">
				      	</div>
				      	<label for="" style="color:#999; font-size:11px">Nota:<strong>Si conoce la cuenta contable digitela en el campo y presion la tecla Enter</strong></label>
				      	<br />
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="liquidation_btn_save" class="btn btn-info btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
								Guardar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="liquidation_btn_clear" class="btn btn-dafault btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								Limpiar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="liquidation_btn_search" class="btn btn-primary btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
								Buscar cuenta contable
							</button>
			  			</div>
			  		</div>
		     	</div>
		    </div>
	  	</div>
	  	<!-- FINISH REGISTER AND MODIFY REGISTERS -->

	  	<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
    		<div class="panel-heading" role="tab" id="headingTwo">
      			<h4 class="panel-title">
	        		<a role="button" data-toggle="collapse" data-parent="#bankAccountsAccordion2" href="#bankAccountsDataGridView" aria-expanded="true" aria-controls="bankAccountsDataGridView">
          				<?php echo $SecondPane . "cuentas bancarias";?>
        			</a>
      			</h4>
	    	</div>
	    	<div id="bankAccountsDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common hidden">
											Código de cuenta bancaria
										</th>
										<th class="th-common">
											Cuenta contable
										</th>
										<th class="th-common">
											Nombre de la cuenta
										</th>
										<th class="th-common">
											Saldo
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_appliedCareers" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>
					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH TABLE RESPONSIVE -->
	</div>
</div>
<!-- END PANE ADD RUTAS -->