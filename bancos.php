<?php
	include 'header.php';
	include 'top-bar.php';
	include 'message_render.php';
	$FirstPane  = 'Nuevo registro';
	$SecondPane = 'Listado de ';
?>
	<div class="container-fluid back-to-session bottom-footer">
		<!-- WEBSITE TITTLE -->
		<div class="row principal-tittle">
			<ol class="breadcrumb">
			  	<li>
			  		<a href="modules.php">
			  			<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
			  			Módulos
			  		</a>
			  	</li>
			  	<li class="active">Bancos</li>
			</ol>
			
			<h1 class="text-center first_tittle">
				Módulo de bancos
			</h1>
		</div>
		<!-- FINISH WEBSITE TITTLE -->
		
		<div class="container-fluid added-bar">
			<div class="row add-articles">
				<!-- MENU BAR PANEL -->
				<nav class="navbar navbar-default">
				  	<div class="container-fluid">
				    	<!-- Brand and toggle get grouped for better mobile display -->
				    	<div class="navbar-header">
					      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					        	<span class="sr-only">Menú</span>
					        	<span class="icon-bar"></span>
					        	<span class="icon-bar"></span>
					        	<span class="icon-bar"></span>
					      	</button>
				    	</div>
				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      	<ul class="nav nav-tabs" role="tablist">
				      		<li role="presentation" class="dropdown"> 
				      			<a href="#" id="myTabDrop1-banco" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop1-contents-banco" aria-expanded="false">
				      				Saldos <span class="caret"></span>
				      			</a> 
			      				<ul class="dropdown-menu" aria-labelledby="myTabDrop1-banco" id="myTabDrop1-contents-banco"> 
			      					<li><a href="#bankAccountsPanel" role="tab" id="dropdown1-tab-banco" data-toggle="tab" aria-controls="dropdown1">Cuentas bancarias</a></li> 
			      				</ul> 
			      			</li>
			      			<li role="presentation" class="dropdown active"> 
				      			<a href="#" id="myTabDrop2-banco" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop2-contents-banco" aria-expanded="false">
				      				Gestiones <span class="caret"></span>
				      			</a> 
			      				<ul class="dropdown-menu active" aria-labelledby="myTabDrop2-banco" id="myTabDrop2-contents-banco">
			      					<li class="active"><a href="#bankInMoneyPanel" role="tab" id="dropdown2-tab-banco" data-toggle="tab" aria-controls="dropdown2">Ingreso de dinero</a></li>  
			      					<li><a href="#bankOutMoneyPanel" role="tab" id="dropdown3-tab-banco" data-toggle="tab" aria-controls="dropdown3">Salida de dinero</a></li> 
			      					<li><a href="#bankConciliationPanel" role="tab" id="dropdown4-tab-banco" data-toggle="tab" aria-controls="dropdown4">Conciliación bancaria</a></li> 
			      				</ul> 
			      			</li>
						</ul>
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>
				<!-- FINISH MENU BAR PANEL -->

				<!-- TAB PANES -->
			  	<div class="tab-content">
			  		<?php
			  			include 'Banco_GUI/bankAccounts.php';
			  			include 'Banco_GUI/conciliation.php';
			  			include 'Banco_GUI/inmoney.php';
			  			include 'Banco_GUI/outMoney.php';
					?>
			  	</div>
			  	<!-- END TAB PANES -->
			</div>
		</div>
	</div>
<?php
	include "footer-copyright.php";
	include "bootstrap_script.php";
	include 'footer.php';
?>