<!-- PANE  ADD BUCES -->
<div role="tabpanel" class="tab-pane" id="busPanel">
	<h2 class="text-center menu-item">Buces</h2>
	
	<div class="panel-group" id="accordionProviders" role="tablist" aria-multiselectable="true">
		<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="headingOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#accordionProviders1" href="#providerInputs" aria-expanded="true" aria-controls="providerInputs">
			          	<?php echo $FirstPane;?>
			        </a>
	      		</h4>
	    	</div>
	    	<div id="providerInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
	      		<div class="panel-body">
	        		<input type="text" id="provider-identification-old" value="" name="provider-identification-old" class="form-control" style="display:none">
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Número de placa</div>
				      		<input type="text" id="bus_placa" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite la placa">
				    	</div>
				    	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Nombre del bús</div>
				      		<input type="text" id="bus_name" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite la nombre">
				    	</div>
				      	<br />
			    	</div>
			    	
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="bus_btn_save" class="btn btn-info btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
								Guardar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="bus_btn_clear" class="btn btn-dafault btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								Limpiar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
		<!-- FINISH REGISTER AND MODIFY REGISTERS -->
		
		<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
	    	<div class="panel-heading" role="tab" id="headingTwo">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#accordionProviders2" href="#providerDataGridView" aria-expanded="false" aria-controls="providerDataGridView">
			          	<?php echo $SecondPane . "búces";?>
			        </a>
	      		</h4>
	    	</div>
	    	<div id="providerDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common hidden">
											Código
										</th>
										<th class="th-common">
											Placa del bús
										</th>
										<th class="th-common">
											Nombre del bús
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_bus" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>
					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="bus_btn_modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="bus_btn_delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH DATAGRIDVIEW PANEL -->
	</div>
</div>
<!-- END PANE ADD BUCES -->