<!-- PANE ADD RUTAS -->
<div role="tabpanel" class="tab-pane" id="applyCareersPanel">
	<h2 class="text-center menu-item">Carreras aplicadas</h2>
	
	<div class="panel-group" id="paymentAcordion" role="tablist" aria-multiselectable="true">
	  	<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="headingOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#paymentAcordion1" href="#paymentInputs" aria-expanded="true" aria-controls="paymentInputs">
			          	<?php echo $FirstPane;?>
			        </a>
	      		</h4>
	    	</div>
		    <div id="paymentInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      	<div class="panel-body">
		      		<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Ruta</div>
			    			<select class="form-control" name="driver" id="appliedCareers_route">

				      		</select>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Chófer</div>
			    			<select class="form-control" name="driver" id="appliedCareers_driver">

				      		</select>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Hora de bajada</div>
			    			<input type="time" id="appliedCareers_downTime" value="" class="form-control" aria-label="">
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Hora de subida</div>
			    			<select class="form-control" name="driver" id="appliedCareers_upTime">

				      		</select>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="appliedCareers_btn_clear" class="btn btn-dafault btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
							  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								Limpiar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="appliedCareers_btn_filter" class="btn btn-warning btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
							  	<span class="glyphicon glyphicon-filte" aria-hidden="true"></span>
								Filtrar
							</button>
			  			</div>
			  		</div>
		     	</div>
		    </div>
	  	</div>
	  	<!-- FINISH REGISTER AND MODIFY REGISTERS -->

	  	<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
    		<div class="panel-heading" role="tab" id="headingTwo">
      			<h4 class="panel-title">
	        		<a role="button" data-toggle="collapse" data-parent="#paymentAcordion2" href="#paymentDataGridView" aria-expanded="true" aria-controls="paymentDataGridView">
          				<?php echo $SecondPane . "ingresos manuales";?>
        			</a>
      			</h4>
	    	</div>
	    	<div id="paymentDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common hidden">
											Código de la carrera
										</th>
										<th class="th-common hidden">
											Código de ruta
										</th>
										<th class="th-common">
											Ruta
										</th>
										<th class="th-common hidden">
											Código de chofer
										</th>
										<th class="th-common">
											Nombre del chófer
										</th>
										<th class="th-common">
											Placa del bús
										</th>
										<th class="th-common">
											Hora de subida
										</th>
										<th class="th-common">
											Hora de bajada
										</th>
										<th class="th-common">
											Total de ingresos
										</th>
										<th class="th-common">
											Total de gastos
										</th>
										<th class="th-common hidden">
											state
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_appliedCareers" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH TABLE RESPONSIVE -->
	</div>
</div>
<!-- END PANE ADD RUTAS -->