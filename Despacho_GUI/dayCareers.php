<!-- PANE ADD RUTAS -->
<div role="tabpanel" class="tab-pane active" id="dayCareersPanel">
	<h2 class="text-center menu-item">Carreras del día</h2>
	
	<div class="panel-group" id="dayCareersAcordion" role="tablist" aria-multiselectable="true">
	  	<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="headingOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#dayCareersAcordion1" href="#dayCareersInputs" aria-expanded="true" aria-controls="dayCareersInputs">
			          	<?php echo $FirstPane;?>
			        </a>
	      		</h4>
	    	</div>
		    <div id="dayCareersInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      	<div class="panel-body">
		      		<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Ruta</div>
		    				<select class="form-control" name="driver" id="dayCareers_route"></select>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Chófer</div>
			    			<select class="form-control" name="driver" id="dayCareers_driver"></select>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Hora de bajada</div>
			    			<input type="time" id="dayCareers_downTime" value="" class="form-control" aria-label="">
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-4 col-lg-4">
			    		<div class="input-group">
				      		<div class="input-group-addon">Hora de subida</div>
			    			<select class="form-control" name="driver" id="dayCareers_upTime"></select>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="dayCareers_btn_save" class="btn btn-info btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
							  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
								Guardar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="dayCareers_btn_clear" class="btn btn-dafault btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
							  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								Limpiar
							</button>
			  			</div>
			  		</div>
		     	</div>
		    </div>
	  	</div>
	  	<!-- FINISH REGISTER AND MODIFY REGISTERS -->

	  	<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
    		<div class="panel-heading" role="tab" id="headingTwo">
      			<h4 class="panel-title">
	        		<a role="button" data-toggle="collapse" data-parent="#dayCareersAcordion2" href="#dayCareersDataGridView" aria-expanded="true" aria-controls="dayCareersDataGridView">
          				<?php echo $SecondPane . "ingresos manuales";?>
        			</a>
      			</h4>
	    	</div>
	    	<div id="dayCareersDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common hidden">
											Código de la carrera
										</th>
										<th class="th-common hidden">
											Código de ruta
										</th>
										<th class="th-common">
											Ruta
										</th>
										<th class="th-common hidden">
											Código de chofer
										</th>
										<th class="th-common">
											Nombre del chófer
										</th>
										<th class="th-common">
											Placa del bús
										</th>
										<th class="th-common">
											Hora de subida
										</th>
										<th class="th-common">
											Hora de bajada
										</th>
										<th class="th-common">
											Total de ingresos
										</th>
										<th class="th-common">
											Total de gastos
										</th>
										<th class="th-common hidden">
											state
										</th>
										<th class="th-common info">
											Acciones
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_dayCareers" style="cursor:pointer;">
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>
					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="dayCareers_btn_modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="dayCareers_btn_delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH TABLE RESPONSIVE -->
	</div>
</div>
<!-- END PANE ADD RUTAS -->