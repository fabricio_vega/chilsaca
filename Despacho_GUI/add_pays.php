<div class="modal fade options" id="articlePrices" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        		<span aria-hidden="true">&times;</span>
	        	</button>
	        	<h4 class="modal-title" id="myModalLabel">Precios de artículo</h4>
	      	</div>
	      	<div class="modal-body">
	        	<div class="panel panel-default panel-responsive">
				  	<div class="panel-heading">
						
				  	</div>
				  	<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="tbl_demo_grid1" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common">
											Precio
										</th>
										<th class="th-common">
											Código
										</th>
										<th class="th-common">
											Descripción		
										</th>
										<th class="th-common">
											Valor	
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="article-prices-table" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>
					<!-- FINISH TABLE RESPONSIVE -->
				</div>
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default" data-dismiss="modal">
	        		<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
	        		Cerrar
	        	</button>
	        	<button type="button" id="btn-selection-article-prices" class="btn btn-success">
	        		<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
	        		Seleccionar
	        	</button>
	      	</div>
    	</div>
  	</div>
</div>