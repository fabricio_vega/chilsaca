<!-- PANE ADD RUTAS -->
<div role="tabpanel" class="tab-pane" id="routesPanel">
	<h2 class="text-center menu-item">Rutas</h2>
	
	<div class="panel-group" id="paymentAcordion" role="tablist" aria-multiselectable="true">
	  	<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="headingOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#paymentAcordion1" href="#paymentInputs" aria-expanded="true" aria-controls="paymentInputs">
			          	<?php echo $FirstPane;?>
			        </a>
	      		</h4>
	    	</div>
		    <div id="paymentInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      	<div class="panel-body">
		      		<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
			    		<div class="input-group">
				      		<div class="input-group-addon">Nombre del lugar</div>
			    			<input type="text" id="route_name" value="" class="form-control" aria-label="" placeholder="Digite el nombre">
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
			    		<div class="input-group">
				      		<div class="input-group-addon">Kilómetros desde Ciudad Quesada</div>
			    			<input type="number" id="route_name" value="" class="form-control" aria-label="" placeholder="Digite los kilómetros">
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="route_btn_save" class="btn btn-info btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
							  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
								Guardar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="route_btn_clear" class="btn btn-dafault btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
							  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								Limpiar
							</button>
			  			</div>
			  		</div>
		     	</div>
		    </div>
	  	</div>
	  	<!-- FINISH REGISTER AND MODIFY REGISTERS -->

	  	<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
    		<div class="panel-heading" role="tab" id="headingTwo">
      			<h4 class="panel-title">
	        		<a role="button" data-toggle="collapse" data-parent="#paymentAcordion2" href="#paymentDataGridView" aria-expanded="true" aria-controls="paymentDataGridView">
          				<?php echo $SecondPane . "ingresos manuales";?>
        			</a>
      			</h4>
	    	</div>
	    	<div id="paymentDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common hidden">
											Código
										</th>
										<th class="th-common">
											Ruta
										</th>
										<th class="th-common">
											Kilómetros desde Ciudad quesada
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_route" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>
					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="route_btn_modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="route_btn_delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH TABLE RESPONSIVE -->
	</div>
</div>
<!-- END PANE ADD RUTAS -->