<?php
	include 'header.php';
	include 'top-bar.php';
	include 'message_render.php';
	$FirstPane = 'Nuevo registro';
	$SecondPane = 'Listado de ';
?>
	<div class="container-fluid back-to-session bottom-footer">
		
		<!-- WEBSITE TITTLE -->
		<div class="row principal-tittle">
			<ol class="breadcrumb">
			  	<li>
			  		<a href="modules.php">
			  			<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
			  			Módulos
			  		</a>
			  	</li>
			  	<li class="active">Combustible</li>
			</ol>
			<h1 class="text-center first_tittle">
				Módulo de combustible
			</h1>	
		</div>
		<!-- FINISH WEBSITE TITTLE -->

		<!-- PRINCIPAL PANEL -->
		<div class="container-fluid added-bar">
			<div class="row add-articles">
				<!-- MENU BAR PANEL -->
				<nav class="navbar navbar-default">
				  	<div class="container-fluid">
				    	<!-- Brand and toggle get grouped for better mobile display -->
				    	<div class="navbar-header">
					      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					        	<span class="sr-only">Toggle navigation</span>
					        	<span class="icon-bar"></span>
					        	<span class="icon-bar"></span>
					        	<span class="icon-bar"></span>
					      	</button>
				    	</div>
				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      	<ul class="nav nav-tabs" role="tablist">
				      		<li role="presentation" class="dropdown active"> 
				      			<a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop1-contents" aria-expanded="false">
				      				Facturas <span class="caret"></span>
				      			</a> 
			      				<ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents"> 
			      					<li class="active">
			      						<a href="#consume" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1">
			      							Consumo excesivo
			      						</a>
			      					</li> 
			      					<li><a href="#new_invoice" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2">Nueva factura</a></li> 
			      				</ul> 
			      			</li>
			      			<li role="presentation" class=""><a href="#reports" aria-controls="clients" role="tab" data-toggle="tab">Reportes</a></li>
						</ul>
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>
				<!-- FINISH MENU BAR PANEL -->
	
				<!-- Tab panes -->
			  	<div class="tab-content">

					<?php 
				    	include 'Combustible_GUI/excesive_consume.php';
					    include 'Combustible_GUI/new_invoice.php';
				    ?>
					
			  	</div>
			</div>
		</div>
		<!-- FINISH PRINCIPAL -->
	</div>
<?php
	include "footer-copyright.php";
	include "bootstrap_script.php";
	include 'footer.php';
?>