<!-- PANE ADD RUTAS -->
<div role="tabpanel" class="tab-pane active" id="applyCareersPanel">
	<h2 class="text-center menu-item">Catálogo de cuentas contables</h2>
	
	<div class="panel-group" id="accountsAcordion" role="tablist" aria-multiselectable="true">
	  	<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="headingOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#accountsAcordion" href="#paymentInputs" aria-expanded="true" aria-controls="paymentInputs">
			          	<?php echo $FirstPane;?>
			        </a>
	      		</h4>
	    	</div>
		    <div id="paymentInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      	<div class="panel-body">
		      		<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
			    		<div class="input-group">
				      		<div class="input-group-addon">Código de cuenta contable</div>
			    			<input type="text" id="appliedCareers_downTime" value="" class="form-control" aria-label="" placeholder="Código">
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
			    		<div class="input-group">
				      		<div class="input-group-addon">Nombre de la cuenta contable</div>
			    			<input type="text" id="appliedCareers_downTime" value="" class="form-control" aria-label="" placeholder="Nombre de la cuenta contable">
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
			    		<div class="input-group">
				      		<div class="input-group-addon">Tipo</div>
			    			<select class="form-control" name="driver" id="appliedCareers_driver">
								<option value="0">1. Activo</option>
								<option value="1">2. Pasivo</option>
								<option value="2">3. Patrimonio</option>
								<option value="3">4. Ingreso</option>
								<option value="4">5. Gasto</option>
				      		</select>
				      	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="liquidation_btn_save" class="btn btn-info btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
								Guardar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="liquidation_btn_clear" class="btn btn-dafault btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								Limpiar
							</button>
			  			</div>
			  		</div>
		     	</div>
		    </div>
	  	</div>
	  	<!-- FINISH REGISTER AND MODIFY REGISTERS -->

	  	<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
    		<div class="panel-heading" role="tab" id="headingTwo">
      			<h4 class="panel-title">
	        		<a role="button" class="collapsed" data-toggle="collapse" data-parent="#accountsAcordion1" href="#accountAssets" aria-expanded="true" aria-controls="paymentDataGridView">
          				<?php echo $SecondPane . "activos";?>
        			</a>
      			</h4>
	    	</div>
	    	<div id="accountAssets" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common">
											Código de la cuenta
										</th>
										<th class="th-common">
											Nombre de la cuenta
										</th>
										<th class="th-common hidden">
											Tipo
										</th>
										<th class="th-common">
											Saldo
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_appliedCareers" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>

					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH TABLE RESPONSIVE -->

	  	<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-danger">
    		<div class="panel-heading" role="tab" id="headingThree">
      			<h4 class="panel-title">
	        		<a role="button" class="collapsed" data-toggle="collapse" data-parent="#accountsAcordion2" href="#accountPasive" aria-expanded="true" aria-controls="paymentDataGridView">
          				<?php echo $SecondPane . "pasivos";?>
        			</a>
      			</h4>
	    	</div>
	    	<div id="accountPasive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common">
											Código de la cuenta
										</th>
										<th class="th-common">
											Nombre de la cuenta
										</th>
										<th class="th-common hidden">
											Tipo
										</th>
										<th class="th-common">
											Saldo
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_appliedCareers" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>

					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH TABLE RESPONSIVE -->

	  	<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-info">
    		<div class="panel-heading" role="tab" id="headingForth">
      			<h4 class="panel-title">
	        		<a role="button" class="collapsed" data-toggle="collapse" data-parent="#accountsAcordion3" href="#accountPatrimonio" aria-expanded="true" aria-controls="paymentDataGridView">
          				<?php echo $SecondPane . "patrimonio";?>
        			</a>
      			</h4>
	    	</div>
	    	<div id="accountPatrimonio" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingForth">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common">
											Código de la cuenta
										</th>
										<th class="th-common">
											Nombre de la cuenta
										</th>
										<th class="th-common hidden">
											Tipo
										</th>
										<th class="th-common">
											Saldo
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_appliedCareers" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>

					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH TABLE RESPONSIVE -->

	  	<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-success">
    		<div class="panel-heading" role="tab" id="headingFive">
      			<h4 class="panel-title">
	        		<a role="button" class="collapsed" data-toggle="collapse" data-parent="#accountsAcordion4" href="#accountIngresos" aria-expanded="true" aria-controls="paymentDataGridView">
          				<?php echo $SecondPane . "ingresos";?>
        			</a>
      			</h4>
	    	</div>
	    	<div id="accountIngresos" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common">
											Código de la cuenta
										</th>
										<th class="th-common">
											Nombre de la cuenta
										</th>
										<th class="th-common hidden">
											Tipo
										</th>
										<th class="th-common">
											Saldo
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_appliedCareers" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>

					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH TABLE RESPONSIVE -->

	  	<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-primary">
    		<div class="panel-heading" role="tab" id="headingSix">
      			<h4 class="panel-title">
	        		<a role="button" class="collapsed" data-toggle="collapse" data-parent="#accountsAcordion5" href="#accountGastos" aria-expanded="true" aria-controls="paymentDataGridView">
          				<?php echo $SecondPane . "gastos";?>
        			</a>
      			</h4>
	    	</div>
	    	<div id="accountGastos" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common">
											Código de la cuenta
										</th>
										<th class="th-common">
											Nombre de la cuenta
										</th>
										<th class="th-common hidden">
											Tipo
										</th>
										<th class="th-common">
											Saldo
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_appliedCareers" style="cursor:pointer;">
									
								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>

					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="inability_btn_delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH TABLE RESPONSIVE -->
	</div>
</div>
<!-- END PANE ADD RUTAS -->