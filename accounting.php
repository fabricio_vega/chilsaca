<?php
	include 'header.php';
	include 'top-bar.php';
	include 'message_render.php';
	$FirstPane  = 'Nuevo registro';
	$SecondPane = 'Listado de ';
?>
	<div class="container-fluid back-to-session bottom-footer">
		<!-- WEBSITE TITTLE -->
		<div class="row principal-tittle">
			<ol class="breadcrumb">
			  	<li>
			  		<a href="modules.php">
			  			<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
			  			Módulos
			  		</a>
			  	</li>
			  	<li class="active">Contabilidad</li>
			</ol>
			
			<h1 class="text-center first_tittle">
				Módulo de contabilidad
			</h1>
		</div>
		<!-- FINISH WEBSITE TITTLE -->
		
		<!-- PRINCIPAL PANEL TO FACTURATION -->
		<div class="container-fluid added-bar">
			<div class="row add-articles">
		    	<nav class="navbar navbar-default">
				  	<div class="container-fluid">
				    	<!-- Brand and toggle get grouped for better mobile display -->
				    	<div class="navbar-header">
					      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					        	<span class="sr-only">Menú</span>
					        	<span class="icon-bar"></span>
					        	<span class="icon-bar"></span>
					        	<span class="icon-bar"></span>
					      	</button>
				    	</div>
				    	<!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      	<ul class="nav nav-tabs" role="tablist">
					      		<li role="presentation" class="active"><a href="#applyCareersPanel" role="tab" id="dropdown0-tab-accounting" data-toggle="tab" aria-controls="dropdown0">Cuentas contables</a></li>
					      		<li role="presentation" class="dropdown"> 
					      			<a href="#" id="myTabDrop1-accounting" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop1-contents-accounting" aria-expanded="false">
					      				Libros <span class="caret"></span>
					      			</a> 
				      				<ul class="dropdown-menu" aria-labelledby="myTabDrop1-accounting" id="myTabDrop1-contents-accounting"> 
				      					<li><a href="#" role="tab" id="dropdown1-tab-accounting" data-toggle="tab" aria-controls="dropdown1">Diario</a></li>
						            	<li role="separator" class="divider"></li>
						            	<li><a href="#" role="tab" id="dropdown2-tab-accounting" data-toggle="tab" aria-controls="dropdown2">Mayor</a></li>
						            	<li role="separator" class="divider"></li>
						            	<li><a href="#" role="tab" id="dropdown3-tab-accounting" data-toggle="tab" aria-controls="dropdown3">Balance de comprobación</a></li>
				      				</ul> 
				      			</li>
				      			<li role="presentation" class="dropdown"> 
					      			<a href="#" id="myTabDrop2-accounting" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop2-contents-accounting" aria-expanded="false">
					      				Estados financieros <span class="caret"></span>
					      			</a> 
				      				<ul class="dropdown-menu" aria-labelledby="myTabDrop2-accounting" id="myTabDrop2-contents-accounting">
				      					<li><a href="#" role="tab" id="dropdown5-tab-accounting" data-toggle="tab" aria-controls="dropdown4">Balance General</a></li>
						            	<li role="separator" class="divider"></li>
						            	<li><a href="#" role="tab" id="dropdown6-tab-accounting" data-toggle="tab" aria-controls="dropdown5">Estado de resultados</a></li>
						            	<li role="separator" class="divider"></li>
						            	<li><a href="#" role="tab" id="dropdown7-tab-accounting" data-toggle="tab" aria-controls="dropdown6">Balance de comprobación</a></li> 
				      				</ul> 
				      			</li>

				      			<li role="presentation" class="dropdown"> 
					      			<a href="#" id="myTabDrop3-accounting" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop3-contents-accounting" aria-expanded="false">
					      				Razones financieras <span class="caret"></span>
					      			</a> 
				      				<ul class="dropdown-menu" aria-labelledby="myTabDrop3-accounting" id="myTabDrop3-contents-accounting">
				      					<li><a href="#" role="tab" id="dropdown8-tab-accounting" data-toggle="tab" aria-controls="dropdown8">Razón de liquidez</a></li>
						            	<li role="separator" class="divider"></li>
						            	<li><a href="#" role="tab" id="dropdown9-tab-accounting" data-toggle="tab" aria-controls="dropdown9">Razón de ventas</a></li>
						            	<li role="separator" class="divider"></li>
						            	<li><a href="#" role="tab" id="dropdown10-tab-accounting" data-toggle="tab" aria-controls="dropdown10">Razón de endeudamiento</a></li>
				      				</ul> 
				      			</li>
							</ul>
					    </div><!-- /.navbar-collapse -->
			  		</div><!-- /.container-fluid -->
				</nav>
				<!-- TAB PANES -->
			  	<div class="tab-content">
			  		<?php
			  			include 'Contabilidad_GUI/cuentas.php';
					?>
			  	</div>
			  	<!-- END TAB PANES -->
			</div>
		</div>
		<!-- FINISH PRINCIPAL PANEL TO FACTURATION -->
	</div>
<?php
	include "footer-copyright.php";
	include "bootstrap_script.php";
	include 'footer.php';
?>