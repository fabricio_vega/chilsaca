<?php 
	include 'DataAccess/connection.php';
	include "header.php";
?>

<!--INICIA MODULOS DEL SISTEMA-->
	<div class="container-fluid">
		<!-- WEBSITE TITTLE -->
		<div class="row principal-tittle">
			<div class="row principal-tittle">
				<img class="logo" src="img/logo.png" alt="">
				<h2 class="text-center" style="opacity:0.7">Innovation Technology System</h2>
				<br><br>
			</div>
			<h1 class="text-center first_tittle">
				Inicio de sesión <br>Administrador
			</h1>

		</div>
		<!-- FINISH WEBSITE TITTLE -->

		<div class="col-md-6 col-md-offset-3">
			<div class="row">
				<div class="col-md-12 text-center login-module">
					<input type="text" placeholder="Identificación" class="login-input">
				</div>
				<div class="col-md-12 text-center login-module">
					<input type="password" placeholder="Contraseña" class="login-input">
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 text-center modules">
					<a href="modules.php" class="botton-modules">
						Iniciar sesión
					</a>
					<div class="border">
						
					</div>
				</div>
				<div class="col-md-6 text-center modules">
					<a href="" class="botton-modules">
						Limpiar
					</a>
					<div class="border">
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<!--TERMINA MODULOS DEL SISTEMA-->
<?php
	include "bootstrap_script.php";
	include 'footer.php';
?>