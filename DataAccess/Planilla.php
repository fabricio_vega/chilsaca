<?php
	class planilla{

		private $dataAccess;
		private $error;
		private $errorMessage;

		public function __construct()
		{
			require_once('DataAccess.php');
			$this->dataAccess = new DataAccess();
			$this->error = false;
			$this->errorMessage = "";
		}

		function GetErrorMessage()
		{

			return $this->errorMessage;
		}

		function InsertDepartment(
			$name)
		{
			$sql = 
			"INSERT INTO department (department_name) 
			VALUES ('$name')";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function ModifyDepartment(
			$code, 
			$name)
		{
			$sql = 
			"UPDATE department SET 
			department_name = '$name' 
			WHERE id = $code";
			$this->dataAccess->ExecuteSQL($sql);

 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function DeleteDepartment($code)
		{
			$sql = 
			"DELETE FROM department WHERE id = $code";

			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function SelectAllDepartment()
		{
			$sql = "SELECT * FROM department";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}

		function SelectAllDepartmentByName($name)
		{
			$sql = "SELECT * FROM department WHERE department_name LIKE '%$name%'";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}


		function InsertJob(
			$name, 
			$department)
		{
			$sql = 
			"INSERT INTO job (id_department, job_name) 
			VALUES ($department, '$name')";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function ModifyJob(
			$code, 
			$name,
			$department)
		{
			$sql = 
			"UPDATE job SET 
			job_name = '$name',
			id_department = $department
			WHERE id = $code";
			$this->dataAccess->ExecuteSQL($sql);

 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function DeleteJob($code)
		{
			$sql = 
			"DELETE FROM job WHERE id = $code";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function SelectAllJob()
		{
			$sql = "SELECT j.*, d.department_name FROM job j, department d WHERE j.id_department = d.id";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}

		function SelectAllJobByName($name)
		{
			$sql = "SELECT j.*, d.department_name FROM job j, department d WHERE j.id_department = d.id AND j.job_name LIKE '%$name%'";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}
		
		function SelectAllTypeOfPay()
		{
			$sql = "SELECT * FROM typeofpay";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}

		function InsertTypeOfPay(
			$name, 
			$equivalence)
		{
			$sql = 
			"INSERT INTO typeofpay (pay_name, equivalence_days) 
			VALUES ('$name', '$equivalence')";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function ModifyTypeOfPay(
			$code, 
			$name,
			$equivalence)
		{
			$sql = 
			"UPDATE typeofpay SET 
			pay_name = '$name',
			equivalence_days = $equivalence
			WHERE id = $code";
			$this->dataAccess->ExecuteSQL($sql);

 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function DeleteTypeOfPay($code)
		{
			$sql = 
			"DELETE FROM typeofpay WHERE id = $code";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function SelectAllTypeOfDeduction()
		{
			$sql = "SELECT * FROM typeofdeduction";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}

		function InsertTypeOfDeduction(
			$name)
		{
			$sql = 
			"INSERT INTO typeofdeduction (deduction_name) 
			VALUES ('$name')";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function ModifyTypeOfDeduction(
			$code, 
			$name)
		{
			$sql = 
			"UPDATE typeofdeduction SET 
			deduction_name = '$name' 
			WHERE id = $code";
			$this->dataAccess->ExecuteSQL($sql);

 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function DeleteTypeOfDeduction($code)
		{
			$sql = 
			"DELETE FROM typeofdeduction WHERE id = $code";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function SelectAllTypeOfInability()
		{
			$sql = "SELECT * FROM typeofinability";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}

		function InsertTypeOfInability(
			$name, 
			$percentage)
		{
			$sql = 
			"INSERT INTO typeofinability (inability_name, pay_percentage) 
			VALUES ('$name', '$percentage')";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function ModifyTypeOfInability(
			$code, 
			$name,
			$percentage)
		{
			$sql = 
			"UPDATE typeofinability SET 
			inability_name = '$name', pay_percentage = '$percentage'
			WHERE id = $code";
			$this->dataAccess->ExecuteSQL($sql);

 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function DeleteTypeOfInability($code)
		{
			$sql = 
			"DELETE FROM typeofinability WHERE id = $code";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function SelectAllColaborator()
		{
			$sql = "SELECT e.identification, 
			e.employee_name, 
			e.surnames, 
			e.entry_date, 
			e.id_job, 
			j.job_name, 
			e.id_typeofpay, 
			p.pay_name, 
			e.salary, 
			e.have_licence, 
			e.expired_licence, 
			e.is_it_insured 
			FROM employee e, job j, typeofpay p 
			WHERE e.id_job = j.id AND e.id_typeofpay = p.id AND e.is_active = true";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}

		function InsertColaborator(
			$cedula, 
			$name, 
			$surname, 
			$fechaIngreso, 
			$puesto, 
			$tipoPago, 
			$salario, 
			$tieneLicencia, 
			$fechaExp, 
			$modalidad)
		{
			$sql = 
			"INSERT INTO employee (identification, 
				employee_name, 
				surnames, 
				entry_date, 
				id_job, 
				salary, 
				have_licence, 
				expired_licence, 
				id_typeofpay, 
				is_it_insured, 
				is_active) 
			VALUES ('$cedula', 
				'$name', 
				'$surname', 
				'$fechaIngreso', 
				$puesto, 
				$salario,
				$tieneLicencia, 
				'$fechaExp', 
				$tipoPago,
				$modalidad, 
				true)";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function ModifyColaborator(
			$cedulaToChange,
			$cedula, 
			$name, 
			$surname, 
			$fechaIngreso, 
			$puesto, 
			$tipoPago, 
			$salario, 
			$tieneLicencia, 
			$fechaExp, 
			$modalidad)
		{
			$sql = 
			"UPDATE employee SET 
			identification  = '$cedula', 
			employee_name   = '$name', 
			surnames        = '$surname', 
			entry_date      = '$fechaIngreso', 
			id_job          = $puesto, 
			salary          = $salario, 
			have_licence    = $tieneLicencia, 
			expired_licence = '$fechaExp', 
			id_typeofpay    = $tipoPago, 
			is_it_insured   = $modalidad
			WHERE identification = '$cedulaToChange'";
			$this->dataAccess->ExecuteSQL($sql);

 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function DeleteColaborator($code)
		{
			$sql = 
			"DELETE FROM employee WHERE identification = '$code'";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}
		function SearchColaborator($code)
		{
			$sql = "SELECT employee_name, surnames, salary, 
			(SELECT SUM(actual_salary + actual_salary) FROM inability WHERE was_it_applied = false AND id_employee = '$code'), /*formula erronea, cambiar*/ 
			(SELECT amount_to_deduct FROM deductions WHERE was_it_applied = false AND id_employee = '$code') /*formula erronea, cambiar*/ 
			FROM employee WHERE identification = '$code' AND is_active = true";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}
	}
?>