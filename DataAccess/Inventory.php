<?php
	class inventory{

		private $dataAccess;
		private $error;
		private $errorMessage;

		public function __construct(){
			require_once('DataAccess.php');
			$this->dataAccess = new DataAccess();
			$this->error = false;
			$this->errorMessage = "";
		}

		function GetErrorMessage(){
			return $this->errorMessage;
		}

		function Insert(
			$code, 
			$articleDescription, 
			$articlePrice1, 
			$articlePrice2, 
			$articlePrice3, 
			$articlePrice4, 
			$articleFreeTax)
		{
			$sql = 
			"INSERT INTO article (code, description, price1, price2, price3, price4, free_tax) 
			VALUES ('$code', '$articleDescription', $articlePrice1, $articlePrice2, $articlePrice3, $articlePrice4, $articleFreeTax)";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}
		
		function Modify(
			$code, 
			$OldCode,
			$articleDescription, 
			$articlePrice1, 
			$articlePrice2, 
			$articlePrice3, 
			$articlePrice4, 
			$articleFreeTax)
		{
			$sql = 
			"UPDATE article SET 
			code        = '$code', 
			description = '$articleDescription', 
			price1      = $articlePrice1, 
			price2      = $articlePrice2, 
			price3      = $articlePrice3, 
			price4      = $articlePrice4, 
			free_tax    = $articleFreeTax 
			WHERE code  = '$OldCode'";
			$this->dataAccess->ExecuteSQL($sql);

 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function Delete($code)
		{
			$sql = 
			"DELETE FROM article WHERE code = '$code'";

			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}
		
		function SelectAll(){
			$sql = "SELECT * FROM article";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}

		function InsertAddManualInventory(
			$date, 
			$articleCode, 
			$quantity, 
			$note)
		{
			$sql = 
			"INSERT INTO inventory (dateMovement, article_code, income, note, typeOfMovement) 
			VALUES ('$date', '$articleCode', $quantity, '$note', 'Ima')";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function ModifyAddManualInventory(
			$Id,
			$date, 
			$articleCode, 
			$quantity, 
			$note)
		{
			$sql = 
			"UPDATE inventory SET 
			dateMovement = '$date', 
			article_code = '$articleCode', 
			income       = $quantity, 
			note         = '$note'
			WHERE id     = $Id";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function DeleteAddManualInventory($Id)
		{
			$sql = 
			"DELETE FROM inventory WHERE id = '$Id'";

			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function SelectAllAddManual()
		{
			$sql = 
			"SELECT i.id, i.dateMovement, i.article_code, a.description, 
			i.income, i.note 
			FROM inventory i, article a 
			WHERE 
			i.article_code = a.code AND 
			typeOfMovement = 'Ima'";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}


		function SelectArticleFilter($key)
		{
			$sql = "SELECT code, description, price1, price2, price3, price4, free_tax FROM article WHERE description LIKE '%$key%'";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}

		function SelectProviderFilter($key)
		{
			$sql = "SELECT identification, name_suplier FROM provider WHERE name_suplier LIKE '%$key%'";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}
	}
?>