<?php

	class DataAccess
	{

		private $conexion;
		private $error;
		private $errorMessage;
		private $var;

		public function __construct()
		{
			require_once('connection.php');
			$this->conexion = new conexion();
			$this->conexion->connected();
			$this->conexion->conexion->set_charset('utf8');
			$this->error = false;
			$this->errorMessage = "";
			$this->var = "error";
		}

		function IsError()
		{
			return $this->error;
		}

		function ErrorMessage()
		{
			return $this->errorMessage;
		}

		function ClearErrors()
		{
			$this->error = false;
			$this->errorMessage = "";
		}

		function ExecuteSQLGet($sql)
		{
			$this->ClearErrors();
			$result = $this->conexion->conexion->query($sql);
			$data = array();
			if($result->num_rows > 0)
			{
				while($re = $result->fetch_array(MYSQL_NUM)){
					$data[] = $re;
				}
			}
			else
			{
				$this->error = true;
				$this->errorMessage = $this->conexion->conexion->error;
				$data[0] = $this->var;
			}
			return $data;
			$this->conexion->close_conexion();
		}

		function ExecuteSQL($sql)
		{
			$this->ClearErrors();
			$result = $this->conexion->conexion->query($sql);
			if($result)
			{
				return false;
			}
			else
			{
				$this->error = true;
				$this->errorMessage = $this->conexion->conexion->error;
				return true;
			}
			$this->conexion->close_conexion();
		}
	}
?>