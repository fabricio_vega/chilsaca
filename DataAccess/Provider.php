<?php
	class provider{

		private $dataAccess;
		private $error;
		private $errorMessage;

		public function __construct()
		{
			require_once('DataAccess.php');
			$this->dataAccess = new DataAccess();
			$this->error = false;
			$this->errorMessage = "";
		}

		function GetErrorMessage()
		{

			return $this->errorMessage;
		}

		function Insert(
			$identification, 
			$name_suplier, 
			$phone, 
			$email, 
			$ubication, 
			$credit_amount, 
			$credit_days, 
			$tanks_days)
		{
			$sql = 
			"INSERT INTO provider (identification, name_suplier, phone, email, ubication, credit_amount, credit_days, tanks_days) 
			VALUES ('$identification', '$name_suplier', '$phone', '$email', '$ubication', $credit_amount, $credit_days, $tanks_days)";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}
		
		function InsertPayment(
			$number, 
			$invoice,
			$date,
			$amount,
			$note)
		{

			$sql = 
			"SELECT count(*) 
			FROM invoice 
			WHERE invoice_number = '$number';";
			$array = $this->dataAccess->ExecuteSQLGet($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				foreach ($array as $row) 
				{
					if($row[0] === '0')
					{
						$this->error = true;
						$this->errorMessage = "No existe la factura digitada";
						return true;
					}
				}
				$sql = 
				"INSERT INTO payment_invoice (payment_number, id_invoice, payment_date, emapayment_amountil, payment_note) 
				VALUES ('$number', '$invoice', '$date', $amount, '$note');";
				$this->dataAccess->ExecuteSQL($sql);

	 			if($this->dataAccess->IsError())
				{
					$this->error = true;
					$this->errorMessage = $this->dataAccess->ErrorMessage();
					return true;
				}
				else
				{
					return false; //No hay error
				}
			}
		}

		function Modify(
			$identification, 
			$identificationOld,
			$name_suplier, 
			$phone, 
			$email, 
			$ubication, 
			$credit_amount, 
			$credit_days, 
			$tanks_days)
		{
			$sql = 
			"UPDATE provider SET 
			identification = '$identification', 
			name_suplier   = '$name_suplier', 
			phone          = '$phone', 
			email          = '$email', 
			ubication      = '$ubication', 
			credit_amount  = $credit_amount, 
			credit_days    = $credit_days, 
			tanks_days     = $tanks_days  
			WHERE identification = '$identificationOld'";
			$this->dataAccess->ExecuteSQL($sql);

 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function ModifyPayment(
			$number, 
			$numberOld, 
			$invoice,
			$date,
			$amount,
			$note)
		{
			$sql = 
			"UPDATE payment_invoice SET 
			payment_number = '$number', 
			id_invoice     = '$invoice', 
			payment_date   = '$date', 
			payment_amount = '$amount', 
			payment_note   = '$note' 
			WHERE payment_number = '$numberOld';";

			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function Delete($identification)
		{
			$sql = 
			"DELETE FROM provider WHERE identification = '$identification'";

			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function DeletePayment($Number)
		{
			$sql = 
			"DELETE FROM payment_invoice WHERE payment_number = '$Number'";

			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}
		
		function SelectAll()
		{
			$sql = "SELECT * FROM provider";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}

		function SelectAllPayments()
		{
			$sql = "SELECT p.payment_number, p.id_invoice, 
			(SELECT name_suplier FROM provider WHERE identification = 'i.id_provider'),
			(SELECT amount FROM invoice WHERE invoice_number = 'p.id_invoice'),
			p.payment_date, p.payment_amount, p.payment_note
			FROM payment_invoice p, invoice i 
			WHERE p.id_invoice = i.invoice_number ORDER BY i.id_provider";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}

		function SelectAllInvoicesByFilter($provider)
		{
			$sql = 
			"SELECT i.invoice_number, p.name_suplier, i.amount 
			FROM invoice i, provider p 
			WHERE 
			i.id_provider = p.identification AND 
			i.is_credit = true AND 
			p.name_suplier LIKE '%$provider%'";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}

		function SelectAllInvoices()
		{
			$sql = "SELECT * FROM invoice WHERE is_credit = true";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}

		function SelectCreditDayProvider($IdProvider)
		{
			$sql = "SELECT credit_days, tanks_days 
			FROM invoice 
			WHERE identification = 'IdProvider')";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}

		function SelectAllPaymentsByProvider($IdInvoice)
		{
			$sql = 
			"SELECT suma(payment_amount) 
			FROM payment_invoice 
			WHERE id_invoice = '$IdInvoice'";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}

		function InsertNC(
					$ncNumber, 
					$date,
					$invoice,
					$amount,
					$note)
		{
			$sql = 
			"INSERT INTO credit_note_invoice (id, note_date, invoice_number, note_amount, note) 
			VALUES ('$ncNumber', '$date', '$invoice', $amount, '$note')";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function ModifyNC(
				$OldncNumber,
				$ncNumber, 
				$date,
				$invoice,
				$amount,
				$note)
		{
			$sql = 
			"UPDATE credit_note_invoice SET  
			id = '$ncNumber', 
			note_date = '$date', 
			invoice_number = '$invoice', 
			note_amount = $amount, 
			note '$note'= 
			WHERE id = '$OldncNumber';";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function DeleteNC($ncNumber)
		{
			$sql = 
			"DELETE FROM credit_note_invoice WHERE id = '$ncNumber'";

			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}
		
		function SelectAllNC()
		{
			$sql = "SELECT c.id, c.note_date, p.name_suplier, c.invoice_number, c.note_amount, c.note 
			FROM credit_note_invoice c, provider p, invoice i
			WHERE c.invoice_number = i.invoice_number AND 
			i.id_provider = p.identification";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}

		function InsertND(
				$ndNumber, 
				$date,
				$invoice,
				$amount,
				$note)
		{
			$sql = 
			"INSERT INTO debit_note_invoice (id, note_date, invoice_number, note_amount, note) 
			VALUES ('$ndNumber', '$date', '$invoice', $amount, '$note')";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function ModifyND(
				$OldndNumber,
				$ndNumber, 
				$date,
				$invoice,
				$amount,
				$note)
		{
			$sql = 
			"UPDATE debit_note_invoice SET  
			id = '$ndNumber', 
			note_date = '$date', 
			invoice_number = '$invoice', 
			note_amount = $amount, 
			note '$note'= 
			WHERE id = '$OldndNumber';";
			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function DeleteND($ndNumber)
		{
			$sql = 
			"DELETE FROM debit_note_invoice WHERE id = '$ndNumber'";

			$this->dataAccess->ExecuteSQL($sql);
 			if($this->dataAccess->IsError())
			{
				$this->error = true;
				$this->errorMessage = $this->dataAccess->ErrorMessage();
				return true;
			}
			else
			{
				return false; //No hay error
			}
		}

		function SelectAllND()
		{
			$sql = "SELECT c.id, c.note_date, p.name_suplier, c.invoice_number, c.note_amount, c.note 
			FROM debit_note_invoice c, provider p, invoice i
			WHERE c.invoice_number = i.invoice_number AND 
			i.id_provider = p.identification";
			return  $this->dataAccess->ExecuteSQLGet($sql);
		}
	}
?>