<?php
	include "header.php";
	include "top-bar.php";
?>
<!--INICIA MODULOS DEL SISTEMA-->
	<div class="container-fluid back-to-session">
		<!-- WEBSITE TITTLE -->
		<div class="row principal-tittle">
			<ol class="breadcrumb">
			  	<li class="active">
			  		<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
			  		Módulos</li>
			</ol>
			
			<h1 class="text-center first_tittle">
				Módulos del sistema
			</h1>
		</div>
		<!-- FINISH WEBSITE TITTLE -->
		<div class="container-fluid added-bar">
			<div class="row add-articles">
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center modules">
					<a href="despacho.php" class="botton-modules">
						<span class="glyphicon glyphicon-bed" aria-hidden="true"></span>
						Despacho
					</a>
					<div class="border">
						
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center modules">
					<a href="proveeduria.php" class="botton-modules">
						<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
						Proveeduría
					</a>
					<div class="border">
						
					</div>
				</div>
				
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center modules">
					<a href="accounting.php" class="botton-modules">
						<span class="glyphicon glyphicon-stats" aria-hidden="true"></span>
						Contabilidad
					</a>
					<div class="border">
						
					</div>
				</div>

				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center modules">
					<a href="bancos.php" class="botton-modules">
						<span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span>
						Bancos
					</a>
					<div class="border">
						
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center modules">
					<a href="tesoreria.php" class="botton-modules">
						<span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span>
						Tesorería
					</a>
					<div class="border">
						
					</div>
				</div>

				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center modules">
					<a href="combustible.php" class="botton-modules">
						<span class="glyphicon glyphicon-scale" aria-hidden="true"></span>
							Combustible
					</a>
					<div class="border">
						
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center modules">
					<a href="planilla.php" class="botton-modules">
						<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
						Planilla
					</a>
					<div class="border">
						
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center modules">
					<a href="users.php" class="botton-modules">
						<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
						Usuarios del sistema
					</a>
					<div class="border">
						
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center modules">
					<a href="parameters.php" class="botton-modules">
						<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
						Parámetros generales
					</a>
					<div class="border">
						
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<!--TERMINA MODULOS DEL SISTEMA-->
	<br>
	<br>
	<br>
<?php
	include "footer-without-copyright.php";
	include "bootstrap_script.php";
	include 'footer.php';
?>