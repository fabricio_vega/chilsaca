<?php
	include 'header.php';
	include 'top-bar.php';
	include 'add_pays.php';
	include 'message_render.php';
	$FirstPane  = 'Nuevo registro';
	$SecondPane = 'Listado de ';
?>
	<div class="container-fluid back-to-session bottom-footer">
		<!-- WEBSITE TITTLE -->
		<div class="row principal-tittle">
			<ol class="breadcrumb">
			  	<li>
			  		<a href="modules.php">
			  			<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
			  			Módulos
			  		</a>
			  	</li>
			  	<li class="active">Despacho</li>
			</ol>
			
			<h1 class="text-center first_tittle">
				Módulo de despacho
			</h1>
		</div>
		<!-- FINISH WEBSITE TITTLE -->
		
		<div class="container-fluid added-bar">
			<div class="row add-articles">
				<!-- MENU BAR PANEL -->
				<nav class="navbar navbar-default">
				  	<div class="container-fluid">
				    	<!-- Brand and toggle get grouped for better mobile display -->
				    	<div class="navbar-header">
					      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					        	<span class="sr-only">Menú</span>
					        	<span class="icon-bar"></span>
					        	<span class="icon-bar"></span>
					        	<span class="icon-bar"></span>
					      	</button>
				    	</div>
				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      	<ul class="nav nav-tabs" role="tablist">
				      		<li role="presentation" class="dropdown"> 
				      			<a href="#" id="myTabDrop1-despacho" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop1-contents" aria-expanded="false">
				      				Mantenimiento <span class="caret"></span>
				      			</a> 
			      				<ul class="dropdown-menu" aria-labelledby="myTabDrop1-despacho" id="myTabDrop1-contents-despacho"> 
			      					<li><a href="#routesPanel" role="tab" id="dropdown1-tab-despacho" data-toggle="tab" aria-controls="dropdown1">Rutas</a></li> 
			      					<li><a href="#busPanel" role="tab" id="dropdown2-tab-despacho" data-toggle="tab" aria-controls="dropdown2">Buces</a></li> 
			      					<li><a href="#DriversPanel" role="tab" id="dropdown3-tab-despacho" data-toggle="tab" aria-controls="dropdown3">Choferes</a></li> 
			      					<li><a href="#inHoursPanel" role="tab" id="dropdown4-tab-despacho" data-toggle="tab" aria-controls="dropdown4">Horarios de subida</a></li>
			      				</ul> 
			      			</li>
			      			<li role="presentation" class="dropdown active"> 
				      			<a href="#" id="myTabDrop2-despacho" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop2-contents-despacho" aria-expanded="false">
				      				Gestiones <span class="caret"></span>
				      			</a> 
			      				<ul class="dropdown-menu active" aria-labelledby="myTabDrop2-despacho" id="myTabDrop2-contents-despacho">
			      					<li class="active"><a href="#dayCareersPanel" role="tab" id="dropdown7-tab-despacho" data-toggle="tab" aria-controls="dropdown7">Carreras del día</a></li>  
			      					<li><a href="#checkedCareersPanel" role="tab" id="dropdown8-tab-despacho" data-toggle="tab" aria-controls="dropdown8">Carreras revisadas</a></li> 
			      					<li><a href="#applyCareersPanel" role="tab" id="dropdown9-tab-despacho" data-toggle="tab" aria-controls="dropdown9">Carreras aplicadas</a></li> 
			      				</ul> 
			      			</li>
			      			<li><a href="#">Cerrar carreras del día</a></li>
						</ul>
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>
				<!-- FINISH MENU BAR PANEL -->

				<!-- TAB PANES -->
			  	<div class="tab-content">
			  		<?php
			  			include 'Despacho_GUI/buces.php';
			  			include 'Despacho_GUI/chofer.php';
			  			include 'Despacho_GUI/rutas.php';
			  			include 'Despacho_GUI/horarios.php';
			  			include 'Despacho_GUI/dayCareers.php';
			  			include 'Despacho_GUI/checkedCareers.php';
			  			include 'Despacho_GUI/AppliedCareers.php';
					?>
			  	</div>
			  	<!-- END TAB PANES -->
			</div>
		</div>
	</div>
<?php
	include "footer-copyright.php";
	include "bootstrap_script.php";
	include 'footer.php';
?>