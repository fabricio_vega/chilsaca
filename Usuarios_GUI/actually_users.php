<div role="tabpanel" class="tab-pane active" id="actualUsersPanel">
	<h2 class="text-center menu-item">Usuarios actuales</h2>
	
	<div class="panel-group" id="actualUsersAccordion" role="tablist" aria-multiselectable="true">
		<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="bonusAccordionOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#actualUsersAccordion1" href="#actualUsersInputs" aria-expanded="true" aria-controls="actualUsersInputs">
			          	<?php echo $FirstPane;?>
			        </a>
	      		</h4>
	    	</div>
		    <div id="actualUsersInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      	<div class="panel-body">
		      		<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Nombre de usuario</div>
				      		<input type="text" id="actualUsers_username" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite nombre de usuario">
				    	</div>
				    	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Tipo de rol</div>
				      		<select class="form-control" name="actualUsers" id="actualUsers_typeOfRol">
      						</select>
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Contraseña</div>
				      		<input type="password" id="actualUsers_password" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite una contraseña">
				    	</div>
				      	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Repetir contraseña</div>
				      		<input type="password" id="actualUsers_repeatPassword" class="form-control" aria-describedby="sizing-addon1" placeholder="Repeta la contraseña">
				    	</div>
				      	<br />
			    	</div>
					<!-- FINISH TYPE OF INVOICE INPUT -->
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="actualUsers_btn_save" class="btn btn-info btn-lg">
							  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
								Guardar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="actualUsers_btn_clear" class="btn btn-dafault btn-lg">
							  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								Limpiar
							</button>
			  			</div>
			  		</div>
		     	</div>
		    </div>
	  	</div>
	  	<!-- FINISH REGISTER AND MODIFY REGISTERS -->

	  	<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
    		<div class="panel-heading" role="tab" id="bonusAccordionTwo">
      			<h4 class="panel-title">
	        		<a role="button" data-toggle="collapse" data-parent="#actualUsersAccordio2n" href="#actualUsersDataGridView" aria-expanded="false" aria-controls="actualUsersDataGridView">
          				<?php echo $SecondPane . "usuarios";?>
        			</a>
      			</h4>
	    	</div>
	    	<div id="actualUsersDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover" onload="selectAll();">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common hidden">
											Código
										</th>
										<th class="th-common">
											Nombre de usuario
										</th>
										<th class="th-common">
											Tipo de rol
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_actualUsers" style="cursor:pointer;">

								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>
					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="actualUsers_btnmodify" class="btn btn-success btn-lg">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="actualUsers_btndelete" class="btn btn-danger btn-lg">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH TABLE RESPONSIVE -->
  	</div>
</div>