<div role="tabpanel" class="tab-pane" id="rolsPanel">
	<h2 class="text-center menu-item">Usuarios actuales</h2>
	
	<div class="panel-group" id="rolsAccordion" role="tablist" aria-multiselectable="true">
		<!-- REGISTER AND MODIFY REGISTERS -->
	  	<div class="panel panel-info">
	    	<div class="panel-heading" role="tab" id="bonusAccordionOne">
	      		<h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#rolsAccordion1" href="#rolsInputs" aria-expanded="true" aria-controls="rolsInputs">
			          	<?php echo $FirstPane;?>
			        </a>
	      		</h4>
	    	</div>
		    <div id="rolsInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      	<div class="panel-body">
		      		<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Nombre del Roll</div>
				      		<input type="text" id="rols_name" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite nombre de usuario">
				    	</div>
				    	<br />
			    	</div>
			    	<div class="col-xs-12 col-md-12 col-lg-12">
			    		<div class="input-group">
				      		<div class="input-group-addon">Seleccione los permisos</div>
				      		<span class="glyphicon" aria-hidden="false"> 
					      		<label class="text-color" style="cursor: pointer"><input type="checkbox" id="rols_see_registers" name="licence" style="cursor: pointer"> Visualizar registros</label>
					      		<br />
					      		<label class="text-color" style="cursor: pointer"><input type="checkbox" id="rols_add_regiters" name="licence" style="cursor: pointer"> Agregar registros</label> 
					      		<br />
					      		<label class="text-color" style="cursor: pointer"><input type="checkbox" id="rols_delete_regiters" name="licence" style="cursor: pointer"> Eliminar registros</label>
					      		<br />
					      		<label class="text-color" style="cursor: pointer"><input type="checkbox" id="rols_modify_regiters" name="licence" style="cursor: pointer"> Modificar registros</label>
					      		<br />
					      		<label class="text-color" style="cursor: pointer"><input type="checkbox" id="rols_generate_reports" name="licence" style="cursor: pointer"> Generar reportes</label>
					      		<br />
					      		<label class="text-color" style="cursor: pointer"><input type="checkbox" id="rols_modulo_despacho" name="licence" style="cursor: pointer">Ingreso a módulo de despacho</label>
					      		<br />
					      		<label class="text-color" style="cursor: pointer"><input type="checkbox" id="rols__modulo_proveeduria" name="licence" style="cursor: pointer">Ingreso a módulo de proveeduría</label>
					      		<br />
					      		<label class="text-color" style="cursor: pointer"><input type="checkbox" id="rols__modulo_contabilidad" name="licence" style="cursor: pointer">Ingreso a módulo de contabilidad</label>
					      		<br />
					      		<label class="text-color" style="cursor: pointer"><input type="checkbox" id="rols__modulo_bancos" name="licence" style="cursor: pointer">Ingreso a módulo de bancos</label>
					      		<br />
					      		<label class="text-color" style="cursor: pointer"><input type="checkbox" id="rols__modulo_tesoreria" name="licence" style="cursor: pointer">Ingreso a módulo de tesorería</label>
					      		<br />
					      		<label class="text-color" style="cursor: pointer"><input type="checkbox" id="rols__modulo_combustible" name="licence" style="cursor: pointer">Ingreso a módulo de combustible</label>
					      		<br />
					      		<label class="text-color" style="cursor: pointer"><input type="checkbox" id="rols__modulo_planilla" name="licence" style="cursor: pointer">Ingreso a módulo de planilla</label>
					      		<br />
					      		<label class="text-color" style="cursor: pointer"><input type="checkbox" id="rols__modulo_usuarios_sistema" name="licence" style="cursor: pointer">Ingreso a módulo de usuarios del sistema</label>
					      		<br />
					      		<label class="text-color" style="cursor: pointer"><input type="checkbox" id="rols__modulo_parametros_generales" name="licence" style="cursor: pointer">Ingreso a módulo de parámetros generales</label>
					      	</span>
				      	</div>
				      	<br />
			    	</div>
					<!-- FINISH TYPE OF INVOICE INPUT -->
			    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="rols_btn_save" class="btn btn-info btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
								Guardar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="rols_btn_clear" class="btn btn-dafault btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								Limpiar
							</button>
			  			</div>
			  		</div>
		     	</div>
		    </div>
	  	</div>
	  	<!-- FINISH REGISTER AND MODIFY REGISTERS -->

	  	<!-- DATAGRIDVIEW PANEL -->
	  	<div class="panel panel-warning">
    		<div class="panel-heading" role="tab" id="bonusAccordionTwo">
      			<h4 class="panel-title">
	        		<a role="button" data-toggle="collapse" data-parent="#rolsAccordion2" href="#rolsDataGridView" aria-expanded="false" aria-controls="rolsDataGridView">
          				<?php echo $SecondPane . "roles";?>
        			</a>
      			</h4>
	    	</div>
	    	<div id="rolsDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
	      		<div class="panel-body">
	        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
				  	<div id="source_code_content" class="tab-content">	
						<div id="tbl_container_demo_grid1" class="table-responsive">
							<table id="list" class="table table-bordered table-hover" onload="selectAll();">
								<!-- TABLE HEAD -->
								<thead>
									<tr id="tbl_demo_grid1_tr_0">
										<th class="th-common hidden">
											Código
										</th>
										<th class="th-common">
											Nombre del Rol
										</th>
										<th class="th-common">
											Visualizar
										</th>
										<th class="th-common">
											Agregar
										</th>
										<th class="th-common">
											Modificar
										</th>
										<th class="th-common">
											Generar reportes
										</th>
										<th class="th-common">
											Despacho
										</th>
										<th class="th-common">
											Proveeduría
										</th>
										<th class="th-common">
											Contabilidad
										</th>
										<th class="th-common">
											Bancos
										</th>
										<th class="th-common">
											Tesorería
										</th>
										<th class="th-common">
											Combusible
										</th>
										<th class="th-common">
											Planilla
										</th>
										<th class="th-common">
											Usuarios del sistema
										</th>
										<th class="th-common">
											Parámetros generales
										</th>
									</tr>
								</thead>
								<!-- FINISH TABLE HEAD -->

								<!-- TABLE BODY -->
								<tbody id="tbody_rols" style="cursor:pointer;">

								</tbody>
								<!-- FINISH TABLE BODY -->
							</table>
						</div>
					</div>
					<!-- FINISH TABLE RESPONSIVE -->
					<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="rols_btnmodify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								Modificar
							</button>
			  			</div>
			  			<div class="col-xs-12 col-md-6 col-lg-6">
			  				<button type="button" id="rols_btndelete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
							  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								Eliminar
							</button>
			  			</div>
			  		</div>
	        	</div>
	    	</div>
	  	</div>
	  	<!-- FINISH TABLE RESPONSIVE -->
  	</div>
</div>