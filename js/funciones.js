$(document).ready(function(){
	var inicio = $(".menu-principal div a.inicio button");
	var colaboradores = $(".menu-principal div a.colaboradores button");
	var permisos = $(".menu-principal div a.permisos button");
	var puestos = $(".menu-principal div a.puestos button");
	var acercade = $(".menu-principal div a.acercade button");
	
    var sPath = window.location.pathname;
    var pgurl = sPath.substring(sPath.lastIndexOf('/') + 1);
    if(pgurl == 'index.php' || 
    	pgurl == 'nombramientos-nuevo.php'||
    	pgurl == 'nombramientos-activos.php' ||
    	pgurl == 'nombramientos-pasivos.php' ||
    	pgurl == 'nombramientos-sin_nombramientos.php'){
    	inicio.addClass("active");
    	var nombra = $("ul.nav li.nombra");
    	var nuevo = $("ul.nav li.nuevo");
    	var activo = $("ul.nav li.activo");
    	var pasivo = $("ul.nav li.pasivo");
    	var sin = $("ul.nav li.sin");
    	
        if(pgurl == 'index.php'){
            nombra.addClass("active");
    	}else{
    		nombra.removeClass("active");
    	}
    	
        if(pgurl == 'nombramientos-nuevo.php'){
    		nuevo.addClass("active");
    	}else{
    		nuevo.removeClass("active");
    	}
    	
        if(pgurl == 'nombramientos-activos.php'){
    		activo.addClass("active");
    	}else{
    		activo.removeClass("active");
    	}
    	
        if(pgurl == 'nombramientos-pasivos.php'){
    		pasivo.addClass("active");
    	}else{
    		pasivo.removeClass("active");
    	}
    	
        if(pgurl == 'nombramientos-sin_nombramientos.php'){
    		sin.addClass("active");
    	}else{
    		sin.removeClass("active");
    	}
    }else{
    	inicio.removeClass("active");
    }

    if(pgurl == 'personal.php' || pgurl == 'personal-lista.php'){
    	colaboradores.addClass("active");
        var nuevo = $("ul.nav li.nuevo");
        var lista = $("ul.nav li.lista");

        if(pgurl == 'personal.php'){
            nuevo.addClass('active');
        }else {
            nuevo.removeClass('active');
        }

        if(pgurl == 'personal-lista.php'){
            lista.addClass('active');
        }else {
            lista.removeClass('active');
        }
    }else{
    	colaboradores.removeClass("active");
    }

    if(pgurl == 'puestos.php' || pgurl == 'puestos-lista.php'){
    	puestos.addClass("active");
        var nuevo = $("ul.nav li.nuevo");
        var lista = $("ul.nav li.lista");

        if(pgurl == 'puestos.php'){
            nuevo.addClass('active');
        }else {
            nuevo.removeClass('active');
        }

        if(pgurl == 'puestos-lista.php'){
            lista.addClass('active');
        }else {
            lista.removeClass('active');
        }
    }else{
    	puestos.removeClass("active");
    }
    
    if(pgurl == 'permisos.php' || pgurl == 'permisos-lista.php' || pgurl == 'permisos-nuevo.php'){
    	permisos.addClass("active");
        var nuevo = $("ul.nav li.nuevo");
        var lista = $("ul.nav li.lista");
        var permiso = $("ul.nav li.permiso");

        if(pgurl == 'permisos.php'){
            nuevo.addClass('active');
        }else {
            nuevo.removeClass('active');
        }

        if(pgurl == 'permisos-lista.php'){
            lista.addClass('active');
        }else {
            lista.removeClass('active');
        }

        if(pgurl == 'permisos-nuevo.php'){
            permiso.addClass('active');
        }else {
            permiso.removeClass('active');
        }
    }else{
    	permisos.removeClass("active");
    }
    
    if(pgurl == 'acercade.php'){
    	acercade.addClass("active");
        
    }else{
    	acercade.removeClass("active");
    }
});

