(function(){
	var isModify = false,
		action   = false;

	/****************************** DEPARTAMENTOS **********************************/
	var codename             = "",
		departmentName       = document.getElementById("department-name"),
		departmentBtnSave    = document.getElementById("department-btn-save"),
		departmentBtnClear   = document.getElementById("department-btn-clear"),
		departmentBtnFilter  = document.getElementById("department-btn-filter"),
		departmentBtnModify  = document.getElementById("department-btn-modify"),
		departmentBtnDelete  = document.getElementById("department-btn-delete"),
		departmentBtnRefresh = document.getElementById("department-btn-refresh"),
		tbodyDepartment      = document.getElementById("tbody-department");

	var departmentNameFunction      = function(){
		departmentName.className = "form-control";
		departmentName.setAttribute("placeholder", "Digite el nombre");
	};
	var departmentBtnSaveFunction   = function(){
		if(departmentName.value === "")
		{
			departmentName.className = "form-control error";
			departmentName.setAttribute("placeholder", "Campor requerido");
			return;
		}
		if(isModify){
			$.ajax({
				url:  "Controllers/Planilla.php",
				type: "POST",
				data: "code="  + codeName       + 
					  "&name=" + departmentName.value + 
					  "&action=modifyDepartment"
			}).done(function(message){
				if(message === 'correcto'){
					selectAllDepartaments();
					Alert.render("Departamento modificado satisfactoriamente");
				}else{
					Alert.render(message);
					return;
				}
			});
		}else{
			$.ajax({
				url:  "Controllers/Planilla.php",
				type: "POST",
				data: "name=" + departmentName.value + 
					  "&action=insertDepartment"
			}).done(function(message){
				if(message === 'correcto'){
					selectAllDepartaments();
					Alert.render("Departamento agregado satisfactoriamente");
				}else{
					Alert.render(message);
					return;
				}
			});
		}

		isModify = false;
		departmentBtnClearFunction();
		selectAllDepartaments();
	};
	var departmentBtnClearFunction  = function(){
		departmentName.value = "";
		codeName       = "";
		departmentName.className = "form-control";
		departmentName.setAttribute("placeholder", "Digite el nombre");
		isModify = false;
	};
	var departmentBtnFilterFunction = function(){
		if(departmentName.value === "error"){
			departmentName.className = "form-control error";
			departmentName.setAttribute("placeholder", "Campor requerido");
			return;
		}
		$.ajax({
			url:  "Controllers/Planilla.php",
			type: "POST",
			data: "name=" + departmentName.value + 
			"&action=SelectAllDepartmentByName"
		}).done(function(message){
			var x = JSON.parse(message);
			if(x[0] != 'error'){
				if ( tbodyDepartment.hasChildNodes() )
				{
					while ( tbodyDepartment.childNodes.length >= 1 )
					{
						tbodyDepartment.removeChild( tbodyDepartment.firstChild );
					}
				}
				for (var i = 0; i < x.length; i++) {
					var row = x[i];
					var hilera      = document.createElement("tr"), 
				    	celda1      = document.createElement("td"),
				      	celda2      = document.createElement("td"),
				      	textoCelda1 = document.createTextNode(row[0]),
				      	textoCelda2 = document.createTextNode(row[1]);

				      	celda1.className = "hidden";
				      	celda1.appendChild(textoCelda1);
				      	hilera.appendChild(celda1);
				      	celda2.appendChild(textoCelda2);
				      	hilera.appendChild(celda2);
				    	tbodyDepartment.appendChild(hilera);
				};
				for (var i = 0; i < tbodyDepartment.rows.length; i++) {
					tbodyDepartment.rows[i].addEventListener("click", tbodyDepartmentFunction);
				};
				
			}else{
				Alert.render(x);
				return;
			}
		});
	};
	var departmentBtnModifyFunction = function(){
		var cont = true;
		for (var i = 0; i < tbodyDepartment.rows.length; i++) {
			if(tbodyDepartment.rows[i].className === "warning"){
				isModify = true;
				codeName             = tbodyDepartment.rows[i].cells[0].innerHTML;
				departmentName.value = tbodyDepartment.rows[i].cells[1].innerHTML;
				cont = true;
				break;
			}else{
				cont = false;
			}
		};
		if(!cont){
			Alert.render("Debe seleccionar la fila que desea modificar");
		}else{
			departmentName.focus();
		}
	};
	var departmentBtnDeleteFunction = function(){
		for (var i = 0; i < tbodyDepartment.rows.length; i++) {
			if(tbodyDepartment.rows[i].className === "warning"){
				codeName = tbodyDepartment.rows[i].cells[0].innerHTML;
				$.ajax({
					url: "Controllers/Planilla.php",
					type: "POST",
					data: "code=" + codeName + 
					"&action=deleteDepartment"
				}).done(function(message){
					if(message === "correcto"){
						selectAllDepartaments();
						Alert.render("Departamento eliminado satisfactoriamente");
						return;
					}else{
						Alert.render(message);
						return;
					}
				});
				return;
			}
		};
		Alert.render("Debe seleccionar la fila que desea eliminar");
	};
	var departmentBtnRefreshFunction   = function(){
		selectAllDepartaments();
	};
	var tbodyDepartmentFunction     = function(){
		for (var i = 0; i < tbodyDepartment.rows.length; i++) {
			var cont = i%2;
				if(cont!=0){
					tbodyDepartment.rows[i].className = "info";
				}else{
					tbodyDepartment.rows[i].className = "";
				}
		};
		this.className = "warning";
	};

	var selectAllDepartaments        = function(){
		$.ajax({
			url:  "Controllers/Planilla.php",
			type: "POST",
			data: "action=selectDepartment"
		}).done(function(message){
			var x = JSON.parse(message);
			if(x[0] != 'error'){
				if ( tbodyDepartment.hasChildNodes() )
				{
					while ( tbodyDepartment.childNodes.length >= 1 )
					{
						tbodyDepartment.removeChild( tbodyDepartment.firstChild );
					}
				}
				for (var i = 0; i < x.length; i++) {
					var count = i%2;
					var row = x[i];
					var hilera      = document.createElement("tr"), 
				    	celda1      = document.createElement("td"),
				      	celda2      = document.createElement("td"),
				      	textoCelda1 = document.createTextNode(row[0]),
				      	textoCelda2 = document.createTextNode(row[1]);

				      	celda1.className = "hidden";
				      	celda1.appendChild(textoCelda1);
				      	hilera.appendChild(celda1);
				      	celda2.appendChild(textoCelda2);
				      	hilera.appendChild(celda2);
				      	if(count!=0){
			      			hilera.setAttribute("class", "info");
				      	}
				    	tbodyDepartment.appendChild(hilera);
				};
				for (var i = 0; i < tbodyDepartment.rows.length; i++) {
					tbodyDepartment.rows[i].addEventListener("click", tbodyDepartmentFunction);
				};
				
			}else{
				Alert.render(x);
				return;
			}
		});
	};

	departmentName.addEventListener("click", departmentNameFunction);
	departmentBtnSave.addEventListener("click", departmentBtnSaveFunction);
	departmentBtnClear.addEventListener("click", departmentBtnClearFunction);
	departmentBtnFilter.addEventListener("click", departmentBtnFilterFunction);
	departmentBtnModify.addEventListener("click", departmentBtnModifyFunction);
	departmentBtnDelete.addEventListener("click", departmentBtnDeleteFunction);
	departmentBtnRefresh.addEventListener("click", departmentBtnRefreshFunction);
	selectAllDepartaments();
	/************************ TERMINA DEPARTAMENTOS *******************************/

	/*********************************** PUESTOS **********************************/
	var jobcode        = document.getElementById("job-code"),
		jobName        = document.getElementById("job-name"),
		jobDepartament = document.getElementById("job-departament"),
		jobBtnSave     = document.getElementById("job-btn-save"),
		jobBtnClear    = document.getElementById("job-btn-clear"),
		jobBtnFilter   = document.getElementById("job-btn-filter"),
		jobBtnModify   = document.getElementById("job-btn-modify"),
		jobBtnDelete   = document.getElementById("job-btn-delete"),
		jobBtnRefresh  = document.getElementById("job-btn-refresh"),
		tbodyjob       = document.getElementById("tbody-job"); 
	
	var jobNameFunction      = function()
	{
		jobName.className = "form-control";
		jobName.setAttribute("placeholder", "Digite el nombre");
	};
	var jobBtnSaveFunction   = function(){
		if(jobName.value === "")
		{
			jobName.className = "form-control error";
			jobName.setAttribute("placeholder", "Campor requerido");
			return;
		}
		if(isModify){
			$.ajax({
				url:  "Controllers/Planilla.php",
				type: "POST",
				data: "code="  + jobcode.value + 
					  "&name=" + jobName.value + 
					  "&department="  + jobDepartament.value + 
					  "&action=modifyJob"
			}).done(function(message){
				if(message === 'correcto'){
					selectAllJobs();
					Alert.render("Departamento modificado satisfactoriamente");
				}else{
					Alert.render(message);
					return;
				}
			});
		}else{
			$.ajax({
				url:  "Controllers/Planilla.php",
				type: "POST",
				data: "name="         + jobName.value        + 
					  "&department="  + jobDepartament.value + 
					  "&action=insertJob"
			}).done(function(message){
				if(message === 'correcto'){
					selectAllJobs();
					Alert.render("Departamento agregado satisfactoriamente");
				}else{
					Alert.render(message);
					return;
				}
			});
		}
		isModify = false;
		jobBtnClearFunction();
		selectAllJobs();
	};
	var jobBtnClearFunction  = function(){
		jobName.value = "";
		jobcode.value       = "";
		jobName.className = "form-control";
		jobName.setAttribute("placeholder", "Digite el nombre");
		isModify = false;
	};
	var jobBtnFilterFunction = function(){
		if(jobName.value === "error"){
			jobName.className = "form-control error";
			jobName.setAttribute("placeholder", "Campor requerido");
			return;
		}
		$.ajax({
			url:  "Controllers/Planilla.php",
			type: "POST",
			data: "name=" + jobName.value + 
			"&action=SelectFilterJobByName"
		}).done(function(message){
			var x = JSON.parse(message);
			if(x[0] != 'error'){
				if ( tbodyjob.hasChildNodes() )
				{
					while ( tbodyjob.childNodes.length >= 1 )
					{
						tbodyjob.removeChild( tbodyjob.firstChild );
					}
				}
				for (var i = 0; i < x.length; i++) {
					var row = x[i];
					var hilera      = document.createElement("tr"), 
				    	celda1      = document.createElement("td"),
				      	celda2      = document.createElement("td"),
				      	celda3      = document.createElement("td"),
				      	celda4      = document.createElement("td"),
				      	textoCelda1 = document.createTextNode(row[0]),
				      	textoCelda2 = document.createTextNode(row[1]),
				      	textoCelda3 = document.createTextNode(row[2]),
				      	textoCelda4 = document.createTextNode(row[3]);

				      	celda1.className = "hidden";
				      	celda3.className = "hidden";
				      	celda1.appendChild(textoCelda1);
				      	hilera.appendChild(celda1);
				      	celda2.appendChild(textoCelda2);
				      	hilera.appendChild(celda2);
				      	celda3.appendChild(textoCelda3);
				      	hilera.appendChild(celda3);
				      	celda4.appendChild(textoCelda4);
				      	hilera.appendChild(celda4);
				    	tbodyjob.appendChild(hilera);
				};
				for (var i = 0; i < tbodyjob.rows.length; i++) {
					tbodyjob.rows[i].addEventListener("click", tbodyjobFunction);
				};
				
			}else{
				Alert.render(x);
				return;
			}
		});
	};
	var jobBtnModifyFunction = function(){
		for (var i = 0; i < tbodyjob.rows.length; i++) {
			if(tbodyjob.rows[i].className === "warning"){
				isModify = true;
				jobcode.value        = tbodyjob.rows[i].cells[0].innerHTML;
				jobDepartament.value = tbodyjob.rows[i].cells[1].innerHTML;
				jobName.value        = tbodyjob.rows[i].cells[2].innerHTML;
				jobName.focus();
				return;
			}
		};
		Alert.render("Debe seleccionar la fila que desea modificar");
	};
	var jobBtnDeleteFunction = function(){
		for (var i = 0; i < tbodyjob.rows.length; i++) {
			if(tbodyjob.rows[i].className === "warning"){
				jobcode.value = tbodyjob.rows[i].cells[0].innerHTML;
				$.ajax({
					url: "Controllers/Planilla.php",
					type: "POST",
					data: "code=" + jobcode.value + 
					"&action=deleteJob"
				}).done(function(message){
					if(message === "correcto"){
						selectAllJobs();
						Alert.render("Departamento eliminado satisfactoriamente");
						return;
					}else{
						Alert.render(message);
						return;
					}
				});
				return;
			}
		};
		Alert.render("Debe seleccionar la fila que desea eliminar");
	};
	var jobBtnRefreshFunction   = function(){
		selectAllDepartments();
		selectAllJobs();
	};
	var tbodyjobFunction     = function(){
		for (var i = 0; i < tbodyjob.rows.length; i++) {
			if(tbodyjob.rows[i].className == "warning"){
				var cont = i%2;
				if(cont!=0){
					tbodyjob.rows[i].className = "info";
				}else{
					tbodyjob.rows[i].className = "";
				}
			}
		};
		this.className = "warning";
	};
	var selectAllDepartments = function(){
		$.ajax({
			url:  "Controllers/Planilla.php",
			type: "POST",
			data: "action=selectjobdepartment"
		}).done(function(message){
			var x = JSON.parse(message);
			if(x[0] != 'error'){
				if ( jobDepartament.hasChildNodes() )
				{
					while ( jobDepartament.childNodes.length >= 1 )
					{
						jobDepartament.removeChild( jobDepartament.firstChild );
					}
				}
				for (var i = 0; i < x.length; i++) {
					var row = x[i];
					var hilera      = document.createElement("option"), 
				      	textoCelda1 = document.createTextNode(row[1]),
				      	value = row[0];

				      	hilera.setAttribute("value", value);
				      	hilera.appendChild(textoCelda1);
				      	
				    	jobDepartament.appendChild(hilera);
				};

			}else{
				Alert.render(x);
				return;
			}
		});
	};
	var selectAllJobs        = function(){
		$.ajax({
			url:  "Controllers/Planilla.php",
			type: "POST",
			data: "action=selectJob"
		}).done(function(message){
			var x = JSON.parse(message);
			if(x[0] != 'error'){
				if ( tbodyjob.hasChildNodes() )
				{
					while ( tbodyjob.childNodes.length >= 1 )
					{
						tbodyjob.removeChild( tbodyjob.firstChild );
					}
				}
				for (var i = 0; i < x.length; i++) {
					var row = x[i];
					var count = i%2;
					var hilera      = document.createElement("tr"), 
				    	celda1      = document.createElement("td"),
				      	celda2      = document.createElement("td"),
				      	celda3      = document.createElement("td"),
				      	celda4      = document.createElement("td"),
				      	textoCelda1 = document.createTextNode(row[0]),
				      	textoCelda2 = document.createTextNode(row[1]),
				      	textoCelda3 = document.createTextNode(row[2]),
				      	textoCelda4 = document.createTextNode(row[3]);

				      	celda1.className = "hidden";
				      	celda2.className = "hidden";
				      	celda1.appendChild(textoCelda1);
				      	hilera.appendChild(celda1);
				      	celda2.appendChild(textoCelda2);
				      	hilera.appendChild(celda2);
				      	celda3.appendChild(textoCelda3);
				      	hilera.appendChild(celda3);
				      	celda4.appendChild(textoCelda4);
				      	hilera.appendChild(celda4);
				      	if(count!=0){
			      			hilera.setAttribute("class", "info");
				      	}
				    	tbodyjob.appendChild(hilera);
				};
				for (var i = 0; i < tbodyjob.rows.length; i++) {
					tbodyjob.rows[i].addEventListener("click", tbodyjobFunction);
				};
				
			}else{
				Alert.render(x);
				return;
			}
		});
	};

	jobName.addEventListener("click", jobNameFunction);
	jobBtnSave.addEventListener("click", jobBtnSaveFunction);
	jobBtnClear.addEventListener("click", jobBtnClearFunction);
	jobBtnFilter.addEventListener("click", jobBtnFilterFunction);
	jobBtnModify.addEventListener("click", jobBtnModifyFunction);
	jobBtnDelete.addEventListener("click", jobBtnDeleteFunction);
	jobBtnRefresh.addEventListener("click", jobBtnRefreshFunction);
	selectAllDepartments();
	selectAllJobs();
	/*************************** TERMINA PUESTOS **********************************/
	/*********************************** TIPOS DE PAGO ****************************/
	var typeOfPayCode        = "",
		typeOfPayName        = document.getElementById("typeofpay_name"),
		typeOfPayEquivalence = document.getElementById("typeofpay_equivalence"),
		typeOfPaySave        = document.getElementById("typeofpay_btn_save"),
		typeOfPayClear       = document.getElementById("typeofpay_btn_clear"),
		typeOfPayDelete      = document.getElementById("typeofpay_btn_delete"),
		typeOfPayModify      = document.getElementById("typeofpay_btn_modify"),
		typeOfPayRefresh      = document.getElementById("typeofpay_btn_refresh"),
		typeOfPayTBody       = document.getElementById("tbody_typeofpay");

	var saveTypeOfPay = function()
	{
		var typeOfPaycontinuar = true;
		if(typeOfPayName.value === "")
		{
			typeOfPayName.className = "form-control error";
			typeOfPayName.setAttribute("placeholder", "Campor requerido");
			typeOfPaycontinuar = false;
		}
		if(typeOfPayEquivalence.value === "")
		{
			typeOfPayEquivalence.className = "form-control error";
			typeOfPayEquivalence.setAttribute("placeholder", "Campor requerido");
			typeOfPaycontinuar = false;
		}
		if(!typeOfPaycontinuar){
			return;
		}
		if(isModify){
			$.ajax({
				url:  "Controllers/Planilla.php",
				type: "POST",
				data: "code="  + typeOfPayCode + 
					  "&name=" + typeOfPayName.value + 
					  "&equivalence="  + typeOfPayEquivalence.value + 
					  "&action=modifyTypeOfPay"
			}).done(function(message){
				if(message === 'correcto'){
					selectAllTypeOfPay();
					Alert.render("Tipo de pago modificado satisfactoriamente");
				}else{
					Alert.render(message);
					return;
				}
			});
		}else{
			$.ajax({
				url:  "Controllers/Planilla.php",
				type: "POST",
				data: "name="          + typeOfPayName.value        + 
					  "&equivalence="  + typeOfPayEquivalence.value + 
					  "&action=insertTypeOfPay"
			}).done(function(message){
				if(message === 'correcto'){
					selectAllTypeOfPay();
					Alert.render("Tipo de pago agregado satisfactoriamente");
				}else{
					Alert.render(message);
					return;
				}
			});
		}
		isModify = false;
		clearTypeOfPay();
		selectAllTypeOfPay();
	};
	var modifyTypeOfPay = function()
	{
		for (var i = 0; i < typeOfPayTBody.rows.length; i++) {
			if(typeOfPayTBody.rows[i].className === "warning"){
				isModify = true;
				typeOfPayCode              = typeOfPayTBody.rows[i].cells[0].innerHTML;
				typeOfPayName.value        = typeOfPayTBody.rows[i].cells[1].innerHTML;
				typeOfPayEquivalence.value = typeOfPayTBody.rows[i].cells[2].innerHTML;
				typeOfPayName.focus();
				return;
			}
		};
		Alert.render("Debe seleccionar la fila que desea modificar");
	};
	var deleteTypeOfPay = function()
	{
		for (var i = 0; i < typeOfPayTBody.rows.length; i++) {
			if(typeOfPayTBody.rows[i].className === "warning"){
				typeOfPayCode = typeOfPayTBody.rows[i].cells[0].innerHTML;
				$.ajax({
					url: "Controllers/Planilla.php",
					type: "POST",
					data: "code=" + typeOfPayCode + 
					"&action=deleteTypeOfPay"
				}).done(function(message){
					if(message === "correcto"){
						selectAllTypeOfPay();
						Alert.render("Tipo de pago eliminado satisfactoriamente");
						return;
					}else{
						Alert.render(message);
						return;
					}
				});
				return;
			}
		};
		Alert.render("Debe seleccionar la fila que desea eliminar");
	};
	
	var refreshTypeOfPay = function()
	{
		selectAllTypeOfPay();
	};
	var clearTypeOfPay = function()
	{
		typeOfPayCode = "";
		typeOfPayName.value = "";
		typeOfPayEquivalence.value = "";
		isModify = false;
		for (var i = 0; i < typeOfPayTBody.rows.length; i++) {
			if(typeOfPayTBody.rows[i].className == "warning"){
				var cont = i%2;
				if(cont!=0){
					typeOfPayTBody.rows[i].className = "info";
				}else{
					typeOfPayTBody.rows[i].className = "";
				}
			}
		};

	};
	var tbodyTypeOfPayFunction     = function()
	{
		for (var i = 0; i < typeOfPayTBody.rows.length; i++) {
			if(typeOfPayTBody.rows[i].className == "warning"){
				var cont = i%2;
				if(cont!=0){
					typeOfPayTBody.rows[i].className = "info";
				}else{
					typeOfPayTBody.rows[i].className = "";
				}
			}
		};
		this.className = "warning";
	};

	var selectAllTypeOfPay = function()
	{
		$.ajax({
			url:  "Controllers/Planilla.php",
			type: "POST",
			data: "action=SelectTypeOfPay"
		}).done(function(message){
			var x = JSON.parse(message);
			if(x[0] != 'error'){
				if ( typeOfPayTBody.hasChildNodes() )
				{
					while ( typeOfPayTBody.childNodes.length >= 1 )
					{
						typeOfPayTBody.removeChild( typeOfPayTBody.firstChild );
					}
				}
				for (var i = 0; i < x.length; i++) {
					var row = x[i];
					var count = i%2;
					var hilera      = document.createElement("tr"), 
				    	celda1      = document.createElement("td"),
				      	celda2      = document.createElement("td"),
				      	celda3      = document.createElement("td"),
				      	textoCelda1 = document.createTextNode(row[0]),
				      	textoCelda2 = document.createTextNode(row[1]),
				      	textoCelda3 = document.createTextNode(row[2]);

				      	celda1.className = "hidden";
				      	celda1.appendChild(textoCelda1);
				      	hilera.appendChild(celda1);
				      	celda2.appendChild(textoCelda2);
				      	hilera.appendChild(celda2);
				      	celda3.appendChild(textoCelda3);
				      	hilera.appendChild(celda3);
				      	if(count!=0){
				      		hilera.className = "info";
				      	}
				    	typeOfPayTBody.appendChild(hilera);
				};
				for (var i = 0; i < typeOfPayTBody.rows.length; i++) {
					typeOfPayTBody.rows[i].addEventListener("click", tbodyTypeOfPayFunction);
				};
				
			}else{
				Alert.render(x);
				return;
			}
		});
	};

	typeOfPaySave.addEventListener("click", saveTypeOfPay);
	typeOfPayClear.addEventListener("click", clearTypeOfPay);
	typeOfPayDelete.addEventListener("click", deleteTypeOfPay);
	typeOfPayModify.addEventListener("click", modifyTypeOfPay);
	typeOfPayRefresh.addEventListener("click", refreshTypeOfPay);
	selectAllTypeOfPay();	
	/*************************** TERMINA TIPOS DE PAGO ****************************/

	/*********************************** TIPOS DE DEDUCCIONES ****************************/
	var typeOfDeductionCode        = "",
		typeOfDeductionName        = document.getElementById("typeOfDeduction_name"),
		typeOfDeductionSave        = document.getElementById("typeOfDeduction_btn_save"),
		typeOfDeductionClear       = document.getElementById("typeOfDeduction_btn_clear"),
		typeOfDeductionDelete      = document.getElementById("typeOfDeduction_btn_delete"),
		typeOfDeductionModify      = document.getElementById("typeOfDeduction_btn_modify"),
		typeOfDeductionRefresh     = document.getElementById("typeOfDeduction_btn_refresh"),
		typeOfDeductionTBody       = document.getElementById("tbody_typeOfDeduction");

	var saveTypeOfDeduction = function()
	{
		if(typeOfDeductionName.value === "")
		{
			typeOfDeductionName.className = "form-control error";
			typeOfDeductionName.setAttribute("placeholder", "Campor requerido");
			return;
		}
		if(isModify){
			$.ajax({
				url:  "Controllers/Planilla.php",
				type: "POST",
				data: "code="  + typeOfDeductionCode + 
					  "&name=" + typeOfDeductionName.value + 
					  "&action=modifyTypeOfDeduction"
			}).done(function(message){
				if(message === 'correcto'){
					selectAllTypeOfPay();
					Alert.render("Tipo de deducción modificado satisfactoriamente");
				}else{
					Alert.render(message);
					return;
				}
			});
		}else{
			$.ajax({
				url:  "Controllers/Planilla.php",
				type: "POST",
				data: "name=" + typeOfDeductionName.value + 
					  "&action=insertTypeOfDeduction"
			}).done(function(message){
				if(message === 'correcto'){
					selectAllTypeOfPay();
					Alert.render("Tipo de deducción agregado satisfactoriamente");
				}else{
					Alert.render(message);
					return;
				}
			});
		}
		isModify = false;
		clearTypeOfDeduction();
		selectAllTypeOfDeduction();
	};
	var modifyTypeOfDeduction = function()
	{
		for (var i = 0; i < typeOfDeductionTBody.rows.length; i++) {
			if(typeOfDeductionTBody.rows[i].className === "warning"){
				isModify = true;
				typeOfDeductionCode              = typeOfDeductionTBody.rows[i].cells[0].innerHTML;
				typeOfDeductionName.value        = typeOfDeductionTBody.rows[i].cells[1].innerHTML;
				typeOfDeductionName.focus();
				return;
			}
		};
		Alert.render("Debe seleccionar la fila que desea modificar");
	};
	var deleteTypeOfDeduction = function()
	{
		for (var i = 0; i < typeOfDeductionTBody.rows.length; i++) {
			if(typeOfDeductionTBody.rows[i].className === "warning"){
				typeOfDeductionCode = typeOfDeductionTBody.rows[i].cells[0].innerHTML;
				$.ajax({
					url: "Controllers/Planilla.php",
					type: "POST",
					data: "code=" + typeOfDeductionCode + 
					"&action=deleteTypeOfDeduction"
				}).done(function(message){
					if(message === "correcto"){
						selectAllTypeOfDeduction();
						Alert.render("Tipo de deducción eliminado satisfactoriamente");
						return;
					}else{
						Alert.render(message);
						return;
					}
				});
				return;
			}
		};
		Alert.render("Debe seleccionar la fila que desea eliminar");
	};
	
	var refreshTypeOfDeduction = function()
	{
		selectAllTypeOfDeduction();
	};
	var clearTypeOfDeduction = function()
	{
		typeOfDeductionCode = "";
		typeOfDeductionName.value = "";
		isModify = false;
		for (var i = 0; i < typeOfDeductionTBody.rows.length; i++) {
			if(typeOfDeductionTBody.rows[i].className == "warning"){
				var cont = i%2;
				if(cont!=0){
					typeOfDeductionTBody.rows[i].className = "info";
				}else{
					typeOfDeductionTBody.rows[i].className = "";
				}
			}
		};

	};
	var tbodyTypeOfDeductionFunction     = function()
	{
		for (var i = 0; i < typeOfDeductionTBody.rows.length; i++) {
			if(typeOfDeductionTBody.rows[i].className == "warning"){
				var cont = i%2;
				if(cont!=0){
					typeOfDeductionTBody.rows[i].className = "info";
				}else{
					typeOfDeductionTBody.rows[i].className = "";
				}
			}
		};
		this.className = "warning";
	};

	var selectAllTypeOfDeduction = function()
	{
		$.ajax({
			url:  "Controllers/Planilla.php",
			type: "POST",
			data: "action=SelectTypeOfDeduction"
		}).done(function(message){
			var x = JSON.parse(message);
			if(x[0] != 'error'){
				if ( typeOfDeductionTBody.hasChildNodes() )
				{
					while ( typeOfDeductionTBody.childNodes.length >= 1 )
					{
						typeOfDeductionTBody.removeChild( typeOfDeductionTBody.firstChild );
					}
				}
				for (var i = 0; i < x.length; i++) {
					var row = x[i];
					var count = i%2;
					var hilera      = document.createElement("tr"), 
				    	celda1      = document.createElement("td"),
				      	celda2      = document.createElement("td"),
				      	textoCelda1 = document.createTextNode(row[0]),
				      	textoCelda2 = document.createTextNode(row[1]);

				      	celda1.className = "hidden";
				      	celda1.appendChild(textoCelda1);
				      	hilera.appendChild(celda1);
				      	celda2.appendChild(textoCelda2);
				      	hilera.appendChild(celda2);
				      	if(count!=0){
				      		hilera.className = "info";
				      	}
				    	typeOfDeductionTBody.appendChild(hilera);
				};
				for (var i = 0; i < typeOfDeductionTBody.rows.length; i++) {
					typeOfDeductionTBody.rows[i].addEventListener("click", tbodyTypeOfDeductionFunction);
				};
				
			}else{
				Alert.render(x);
				return;
			}
		});
	};
	
	typeOfDeductionSave.addEventListener("click", saveTypeOfDeduction);
	typeOfDeductionClear.addEventListener("click", clearTypeOfDeduction);
	typeOfDeductionDelete.addEventListener("click", deleteTypeOfDeduction);
	typeOfDeductionModify.addEventListener("click", modifyTypeOfDeduction);
	typeOfDeductionRefresh.addEventListener("click", refreshTypeOfDeduction);
	selectAllTypeOfDeduction();	
	/*************************** TERMINA TIPOS DE DEDUCCIONES ****************************/

	/*********************************** TIPOS DE INCAPACIDADES ****************************/
	var typeOfInabilityCode        = "",
		typeOfInabilityName        = document.getElementById("typeOfInability_name"),
		typeOfInabilityPercentage  = document.getElementById("typeOfInability_percentaje"),
		typeOfInabilitySave        = document.getElementById("typeOfInability_btn_save"),
		typeOfInabilityClear       = document.getElementById("typeOfInability_btn_clear"),
		typeOfInabilityDelete      = document.getElementById("typeOfInability_btn_delete"),
		typeOfInabilityModify      = document.getElementById("typeOfInability_btn_modify"),
		typeOfInabilityRefresh     = document.getElementById("typeOfInability_btn_refresh"),
		typeOfInabilityTBody       = document.getElementById("tbody_typeOfInability");

	var saveTypeOfInability = function()
	{	
		var typeOfInabilityContinue = true;
		if(typeOfInabilityName.value === "")
		{
			typeOfInabilityName.className = "form-control error";
			typeOfInabilityName.setAttribute("placeholder", "Campor requerido");
			typeOfInabilityContinue = false;
		}
		if(typeOfInabilityPercentage.value === "")
		{
			typeOfInabilityPercentage.className = "form-control error";
			typeOfInabilityPercentage.setAttribute("placeholder", "Campor requerido");
			typeOfInabilityContinue = false;
		}
		if(!typeOfInabilityContinue){
			return;
		}
		if(isModify){
			$.ajax({
				url:  "Controllers/Planilla.php",
				type: "POST",
				data: "code="  + typeOfInabilityCode + 
					  "&name=" + typeOfInabilityName.value + 
					  "&percentage=" + typeOfInabilityPercentage.value + 
					  "&action=modifyTypeOfInability"
			}).done(function(message){
				if(message === 'correcto'){
					selectAllTypeOfPay();
					Alert.render("Tipo de incapacidad modificado satisfactoriamente");
				}else{
					Alert.render(message);
					return;
				}
			});
		}else{
			$.ajax({
				url:  "Controllers/Planilla.php",
				type: "POST",
				data: "name=" + typeOfInabilityName.value + 
					  "&percentage=" + typeOfInabilityPercentage.value +  
					  "&action=insertTypeOfInability"
			}).done(function(message){
				if(message === 'correcto'){
					selectAllTypeOfPay();
					Alert.render("Tipo de incapacidad agregado satisfactoriamente");
				}else{
					Alert.render(message);
					return;
				}
			});
		}
		isModify = false;
		clearTypeOfInability();
		selectAllTypeOfInability();
	};
	var modifyTypeOfInability = function()
	{
		for (var i = 0; i < typeOfInabilityTBody.rows.length; i++) {
			if(typeOfInabilityTBody.rows[i].className === "warning"){
				isModify = true;
				typeOfInabilityCode              = typeOfInabilityTBody.rows[i].cells[0].innerHTML;
				typeOfInabilityName.value        = typeOfInabilityTBody.rows[i].cells[1].innerHTML;
				typeOfInabilityPercentage.value  = typeOfInabilityTBody.rows[i].cells[2].innerHTML;
				typeOfInabilityName.focus();
				return;
			}
		};
		Alert.render("Debe seleccionar la fila que desea modificar");
	};
	var deleteTypeOfInability = function()
	{
		for (var i = 0; i < typeOfInabilityTBody.rows.length; i++) {
			if(typeOfInabilityTBody.rows[i].className === "warning"){
				typeOfInabilityCode = typeOfInabilityTBody.rows[i].cells[0].innerHTML;
				$.ajax({
					url: "Controllers/Planilla.php",
					type: "POST",
					data: "code=" + typeOfInabilityCode + 
					"&action=deleteTypeOfInability"
				}).done(function(message){
					if(message === "correcto"){
						selectAllTypeOfInability();
						Alert.render("Tipo de incapacidad eliminado satisfactoriamente");
						return;
					}else{
						Alert.render(message);
						return;
					}
				});
				return;
			}
		};
		Alert.render("Debe seleccionar la fila que desea eliminar");
	};
	
	var refreshTypeOfInability = function()
	{
		selectAllTypeOfInability();
	};
	var clearTypeOfInability = function()
	{
		typeOfInabilityCode = "";
		typeOfInabilityName.value = "";
		typeOfInabilityPercentage.value = "";
		isModify = false;
		for (var i = 0; i < typeOfInabilityTBody.rows.length; i++) {
			if(typeOfInabilityTBody.rows[i].className == "warning"){
				var cont = i%2;
				if(cont!=0){
					typeOfInabilityTBody.rows[i].className = "info";
				}else{
					typeOfInabilityTBody.rows[i].className = "";
				}
			}
		};

	};
	var tbodyTypeOfInabilityFunction     = function()
	{
		for (var i = 0; i < typeOfInabilityTBody.rows.length; i++) {
			if(typeOfInabilityTBody.rows[i].className == "warning"){
				var cont = i%2;
				if(cont!=0){
					typeOfInabilityTBody.rows[i].className = "info";
				}else{
					typeOfInabilityTBody.rows[i].className = "";
				}
			}
		};
		this.className = "warning";
	};

	var selectAllTypeOfInability = function()
	{
		$.ajax({
			url:  "Controllers/Planilla.php",
			type: "POST",
			data: "action=SelectTypeOfInability"
		}).done(function(message){
			var x = JSON.parse(message);
			if(x[0] != 'error'){
				if ( typeOfInabilityTBody.hasChildNodes() )
				{
					while ( typeOfInabilityTBody.childNodes.length >= 1 )
					{
						typeOfInabilityTBody.removeChild( typeOfInabilityTBody.firstChild );
					}
				}
				for (var i = 0; i < x.length; i++) {
					var row = x[i];
					var count = i%2;
					var hilera      = document.createElement("tr"), 
				    	celda1      = document.createElement("td"),
				      	celda2      = document.createElement("td"),
				      	celda3      = document.createElement("td"),
				      	textoCelda1 = document.createTextNode(row[0]),
				      	textoCelda2 = document.createTextNode(row[1]),
				      	textoCelda3 = document.createTextNode(row[2]);

				      	celda1.className = "hidden";
				      	celda1.appendChild(textoCelda1);
				      	hilera.appendChild(celda1);
				      	celda2.appendChild(textoCelda2);
				      	hilera.appendChild(celda2);
				      	celda3.appendChild(textoCelda3);
				      	hilera.appendChild(celda3);
				      	if(count!=0){
				      		hilera.className = "info";
				      	}
				    	typeOfInabilityTBody.appendChild(hilera);
				};
				for (var i = 0; i < typeOfInabilityTBody.rows.length; i++) {
					typeOfInabilityTBody.rows[i].addEventListener("click", tbodyTypeOfInabilityFunction);
				};
				
			}else{
				Alert.render(x);
				return;
			}
		});
	};
	
	typeOfInabilitySave.addEventListener("click", saveTypeOfInability);
	typeOfInabilityClear.addEventListener("click", clearTypeOfInability);
	typeOfInabilityDelete.addEventListener("click", deleteTypeOfInability);
	typeOfInabilityModify.addEventListener("click", modifyTypeOfInability);
	typeOfInabilityRefresh.addEventListener("click", refreshTypeOfInability);
	selectAllTypeOfInability();	
	/*************************** TERMINA TIPOS DE DEDUCCIONES ****************************/

	/*********************************** COLABORADORES ************************************/
	var cedulaToChange     = "",
		cedula             = document.getElementById("employee_identification"),
		nombre             = document.getElementById("employee_name"),
		apellidos          = document.getElementById("employee_surnames"),
		fechaIngreso       = document.getElementById("employee_entry_date"),
		puesto             = document.getElementById("employee_job"),
		tipoPago           = document.getElementById("employee_typeofpay"),
		salario            = document.getElementById("employee_salary"),
		tieneLicenciaYes   = document.getElementById("employee_rb_havelicence_yes"),
		tieneLicenciaNo    = document.getElementById("employee_rb_havelicence_no"),
		fecha_exp          = document.getElementById("employee_exp_licence"),
		asegurado          = document.getElementById("employee_rb_insurance"),
		serProfesional     = document.getElementById("employee_rb_professionaservice"),
		colaboratorSave    = document.getElementById("employee_btn_save"),
		colaboratorClear   = document.getElementById("employee_btn_clear"),
		colaboratorFilter  = document.getElementById("employee_btn_filter"),
		colaboratorModify  = document.getElementById("employee_btn_modify"),
		colaboratorDelete  = document.getElementById("employee_btn_delete"),
		colaboratorRefresh = document.getElementById("employee_btn_refresh"),
		colaboratorTBody   = document.getElementById("tbody_employee");

	var tbodyColaboratorFunction     = function()
	{
		for (var i = 0; i < colaboratorTBody.rows.length; i++) {
			if(colaboratorTBody.rows[i].className == "warning"){
				var cont = i%2;
				if(cont!=0){
					colaboratorTBody.rows[i].className = "info";
				}else{
					colaboratorTBody.rows[i].className = "";
				}
			}
		};
		this.className = "warning";
	};

	var clearColabortor = function()
	{
		cedulaToChange            = "";
		cedula.value              = "";
		nombre.value              = "";
		apellidos.value           = "";
		fechaIngreso.value        = "";
		puesto.selectedIndex      = 0;
		tipoPago.selectedIndex    = 0;
		salario.value             = "";
		tieneLicenciaYes.checked  = true;
		tieneLicenciaNo.checked   = false;
		tieneLicenciaYes.disabled = false;
		tieneLicenciaNo.disabled  = false;
		fecha_exp.value           = "";
		fecha_exp.disabled        = false;
		asegurado.checked         = true;
		serProfesional.checked    = false;

	};

	var selectColaborator = function()
	{
		$.ajax({
			url:  "Controllers/Planilla.php",
			type: "POST",
			data: "action=SelectColaborators"
		}).done(function(message){
			var x = JSON.parse(message);
			if ( colaboratorTBody.hasChildNodes() )
			{
				while ( colaboratorTBody.childNodes.length >= 1 )
				{
					colaboratorTBody.removeChild( colaboratorTBody.firstChild );
				}
			}
			if(x[0] != 'error'){
				for (var i = 0; i < x.length; i++) {
					var row = x[i];
					var count = i%2;
					var hilera      = document.createElement("tr"), 
				    	celda1      = document.createElement("td"),
				      	celda2      = document.createElement("td"),
				      	celda3      = document.createElement("td"),
				      	celda4      = document.createElement("td"),
				      	celda5      = document.createElement("td"),
				      	celda6      = document.createElement("td"),
				      	celda7      = document.createElement("td"),
				      	celda8      = document.createElement("td"),
				      	celda9      = document.createElement("td"),
				      	celda10      = document.createElement("td"),
				      	celda11      = document.createElement("td"),
				      	celda12      = document.createElement("td"),
				      	textoCelda1 = document.createTextNode(row[0]),
				      	textoCelda2 = document.createTextNode(row[1]),
				      	textoCelda3 = document.createTextNode(row[2]),
				      	textoCelda4 = document.createTextNode(row[3]),
				      	textoCelda5 = document.createTextNode(row[4]),
				      	textoCelda6 = document.createTextNode(row[5]),
				      	textoCelda7 = document.createTextNode(row[6]),
				      	textoCelda8 = document.createTextNode(row[7]),
				      	textoCelda9 = document.createTextNode(row[8]);
				      	textoCelda10 = "",
				      	textoCelda11 = document.createTextNode(row[10]),
				      	textoCelda12 = "";

				      	if(row[11] === "1"){
				      		textoCelda12 = document.createTextNode("Asegurado");
				      	}else{
				      		textoCelda12 = document.createTextNode("Servicios profesionales");
				      	}			   
				      	
				      	if(row[9] === "1"){
				      		textoCelda10 = document.createTextNode("Si");
				      	}else{
				      		textoCelda10 = document.createTextNode("No");
				      	}

				      	celda5.className = "hidden";
				      	celda7.className = "hidden";

				      	celda1.appendChild(textoCelda1);
				      	hilera.appendChild(celda1);
				      	celda2.appendChild(textoCelda2);
				      	hilera.appendChild(celda2);
				      	celda3.appendChild(textoCelda3);
				      	hilera.appendChild(celda3);
				      	celda4.appendChild(textoCelda4);
				      	hilera.appendChild(celda4);
				      	celda5.appendChild(textoCelda5);
				      	hilera.appendChild(celda5);
				      	celda6.appendChild(textoCelda6);
				      	hilera.appendChild(celda6);
				      	celda7.appendChild(textoCelda7);
				      	hilera.appendChild(celda7);
				      	celda8.appendChild(textoCelda8);
				      	hilera.appendChild(celda8);
				      	celda9.appendChild(textoCelda9);
				      	hilera.appendChild(celda9);
				      	celda10.appendChild(textoCelda10);
				      	hilera.appendChild(celda10);
				      	celda11.appendChild(textoCelda11);
				      	hilera.appendChild(celda11);
				      	celda12.appendChild(textoCelda12);
				      	hilera.appendChild(celda12);
				      	if(count!=0){
				      		hilera.className = "info";
				      	}
				    	colaboratorTBody.appendChild(hilera);
				};
				for (var i = 0; i < colaboratorTBody.rows.length; i++) {
					colaboratorTBody.rows[i].addEventListener("click", tbodyColaboratorFunction);
				};
			}else{
				
				return;
			}
		});
	};

	var selectJobs = function()
	{
		$.ajax({
			url:  "Controllers/Planilla.php",
			type: "POST",
			data: "action=selectJob"
		}).done(function(message){
			var x = JSON.parse(message);
			if(x[0] != 'error'){
				if ( puesto.hasChildNodes() )
				{
					while ( puesto.childNodes.length >= 1 )
					{
						puesto.removeChild( puesto.firstChild );
					}
				}
				for (var i = 0; i < x.length; i++) {
					var row = x[i];
					var count = i%2;
					var hilera      = document.createElement("option"), 
				      	textoCelda1 = document.createTextNode((i+1) + ". " + row[2]);

			      		hilera.setAttribute("value", row[0]);
				      	hilera.appendChild(textoCelda1);
				    	puesto.appendChild(hilera);
				};
			}else{
				
				return;
			}
		});
	};

	var selectTypeOfPay = function()
	{
		$.ajax({
			url:  "Controllers/Planilla.php",
			type: "POST",
			data: "action=SelectTypeOfPay"
		}).done(function(message){
			var x = JSON.parse(message);
			if(x[0] != 'error'){
				if ( tipoPago.hasChildNodes() )
				{
					while ( tipoPago.childNodes.length >= 1 )
					{
						tipoPago.removeChild( tipoPago.firstChild );
					}
				}
				for (var i = 0; i < x.length; i++) {
					var row = x[i];
					var count = i%2;
					var hilera      = document.createElement("option"), 
				      	textoCelda1 = document.createTextNode((i+1) + ". " + row[1]);

			      		hilera.setAttribute("value", row[0]);
				      	hilera.appendChild(textoCelda1);
				    	tipoPago.appendChild(hilera);
				};
			}else{
				
				return;
			}
		});
	};
	var colaboratorSaveFunction = function()
	{
		var colaboratorContinue = true,
			haveLicence = false,
			isInsured = false;

		if(cedula.value === ""){
			cedula.className = "form-control error";
			cedula.setAttribute("placeholder", "Campor requerido");
			colaboratorContinue = false;
		}
		if(nombre.value === ""){
			nombre.className = "form-control error";
			nombre.setAttribute("placeholder", "Campor requerido");
			colaboratorContinue = false;
		}
		if(apellidos.value === ""){
			apellidos.className = "form-control error";
			apellidos.setAttribute("placeholder", "Campor requerido");
			colaboratorContinue = false;
		}
		if(fechaIngreso.value === ""){
			fechaIngreso.className = "form-control error";
			fechaIngreso.setAttribute("placeholder", "Campor requerido");
			colaboratorContinue = false;
		}
		if(puesto.selectedIndex < 0){
			puesto.selectedIndex = 0;
		}
		if(tipoPago.selectedIndex < 0){
			tipoPago.selectedIndex = 0;
		}
		if(salario.value === ""){
			salario.className = "form-control error";
			salario.setAttribute("placeholder", "Campor requerido");
			colaboratorContinue = false;
		}

		if(!colaboratorContinue){
			return;
		}
		if(asegurado.checked){
			isInsured = true;
		}else{
			isInsured = false;
		}
		if(tieneLicenciaYes.checked){
			haveLicence = true;
		}else{
			haveLicence = false;
		}
		if(isModify){
			$.ajax({
				url:  "Controllers/Planilla.php",
				type: "POST",
				data: "cedulaToChange=" + cedulaToChange     + 
					  "&cedula="         + cedula.value       + 
					  "&name="          + nombre.value       +  
					  "&surname="       + apellidos.value    +  
					  "&fechaIngreso="  + fechaIngreso.value +  
					  "&puesto="        + puesto.value       +  
					  "&tipoPago="      + tipoPago.value     +  
					  "&salario="       + salario.value      +  
					  "&tieneLicencia=" + haveLicence        +  
					  "&fechaExp="      + fecha_exp.value    +  
					  "&modalidad="     + isInsured          + 
					  "&action=modifyColaborator"
			}).done(function(message){
				if(message === 'correcto'){
					selectColaborator();
					Alert.render("Colaborador modificado satisfactoriamente");
				}else{
					Alert.render(message);
					return;
				}
			});
		}else{
			$.ajax({
				url:  "Controllers/Planilla.php",
				type: "POST",
				data: "cedula="         + cedula.value       + 
					  "&name="          + nombre.value       +  
					  "&surname="       + apellidos.value    +  
					  "&fechaIngreso="  + fechaIngreso.value +  
					  "&puesto="        + puesto.value       +  
					  "&tipoPago="      + tipoPago.value     +  
					  "&salario="       + salario.value      +  
					  "&tieneLicencia=" + haveLicence        +  
					  "&fechaExp="      + fecha_exp.value    +  
					  "&modalidad="     + isInsured          +  
					  "&action=insertColaborator"
			}).done(function(message){
				if(message === 'correcto'){
					selectColaborator();
					Alert.render("Colaborador agregado satisfactoriamente");
				}else{
					Alert.render(message);
					return;
				}
			});
		}
		isModify = false;
		clearColabortor();
		selectColaborator();
	};
	var colaboratorClearFunction = function()
	{
		clearColabortor();
	};
	var colaboratorFilterFunction = function()
	{
		
	};
	var colaboratorModifyFunction = function()
	{
		var fecha1 = "",
			fecha2 = "",
			aux    = "";
		for (var i = 0; i < colaboratorTBody.rows.length; i++) {
			if(colaboratorTBody.rows[i].className === "warning"){
				isModify = true;
				cedulaToChange     = colaboratorTBody.rows[i].cells[0].innerHTML;
				cedula.value       = colaboratorTBody.rows[i].cells[0].innerHTML;
				nombre.value       = colaboratorTBody.rows[i].cells[1].innerHTML;
				apellidos.value    = colaboratorTBody.rows[i].cells[2].innerHTML;
				aux = colaboratorTBody.rows[i].cells[3].innerHTML;
				for(var h = 0; h<10; h++){
					fecha1 += aux[h];
				}
				fechaIngreso.value = fecha1;
				puesto.value       = colaboratorTBody.rows[i].cells[4].innerHTML;
				tipoPago.value     = colaboratorTBody.rows[i].cells[6].innerHTML;
				salario.value      = colaboratorTBody.rows[i].cells[8].innerHTML;
				if(colaboratorTBody.rows[i].cells[9].innerHTML === "Si"){
					tieneLicenciaYes.checked = true;
					tieneLicenciaYes.disabled = true;
					tieneLicenciaNo.disabled = true;
				}else{
					tieneLicenciaNo.checked = true;
				}
				aux = colaboratorTBody.rows[i].cells[10].innerHTML;
				for(var h = 0; h<10; h++){
					fecha2 += aux[h];
				}
				fecha_exp.value = fecha2;
				if(colaboratorTBody.rows[i].cells[11].innerHTML === "Si"){
					asegurado.checked = true;
				}else{
					serProfesional.checked = true;
				}
				cedula.focus();
				return;
			}
		};
		Alert.render("Debe seleccionar la fila que desea modificar");
	};
	var colaboratorDeleteFunction = function()
	{
		for (var i = 0; i < colaboratorTBody.rows.length; i++) {
			if(colaboratorTBody.rows[i].className === "warning"){
				cedulaToChange = colaboratorTBody.rows[i].cells[0].innerHTML;
				$.ajax({
					url: "Controllers/Planilla.php",
					type: "POST",
					data: "code=" + cedulaToChange + 
					"&action=deleteColaborator"
				}).done(function(message){
					if(message === "correcto"){
						selectColaborator();
						Alert.render("Colaborador eliminado satisfactoriamente");
						return;
					}else{
						Alert.render(message);
						return;
					}
				});
				return;
			}
		};
		Alert.render("Debe seleccionar la fila que desea eliminar");
	};
	var colaboratorRefreshFunction = function()
	{
		selectColaborator();
	};
	var tieneLicenciaYesFunction = function(){
		if(tieneLicenciaYes.checked){
			fecha_exp.disabled = false;
		}
	};
	var tieneLicenciaNoFunction = function(){
		if(tieneLicenciaNo.checked){
			fecha_exp.disabled = true;
		}
	};
	colaboratorSave.addEventListener("click", colaboratorSaveFunction);
	colaboratorClear.addEventListener("click", colaboratorClearFunction);
	colaboratorFilter.addEventListener("click", colaboratorFilterFunction);
	colaboratorModify.addEventListener("click", colaboratorModifyFunction);
	colaboratorDelete.addEventListener("click", colaboratorDeleteFunction);
	colaboratorRefresh.addEventListener("click", colaboratorRefreshFunction);
	tieneLicenciaYes.addEventListener("click", tieneLicenciaYesFunction);
	tieneLicenciaNo.addEventListener("click", tieneLicenciaNoFunction);
	clearColabortor();
	selectColaborator();	
	selectJobs();
	selectTypeOfPay();
	/*********************************** TERMINA COLABORADORES ****************************/
}());