(function(){
	var pagoSalarioIdentificationToModify = "",
		pagoSalarioIdentification         = document.getElementById('payrollEmployee_identification'),
		pagoSalarioName    = document.getElementById('payrollEmployee_name'),
		pagoSalarioSurname = document.getElementById('payrollEmployee_surnames'),
		pagoSalarioDate    = document.getElementById('payrollEmployee_date_invoice'),
		pagoSalarioExtra   = document.getElementById('payrollEmployee_extra_pay'),
		pagoSalarioSalary  = document.getElementById('payrollEmployee_salary'),
		pagoSalarioDeduction = document.getElementById('payrollEmployee_deductions'),
		pagoSalarioInability = document.getElementById('payrollEmployee_inabilities'),
		pagoSalarioSave   = document.getElementById('payrollEmployee_btn_save'),
		pagoSalarioClear  = document.getElementById('payrollEmployee_btn_clear'),
		pagoSalarioFilter = document.getElementById('payrollEmployee_btn_filter'),
		pagoSalarioSearch = document.getElementById('payrollEmployee_btn_search'),
		pagoSalarioModify = document.getElementById('payrollEmployee_btn_modify'),
		pagoSalarioDelete = document.getElementById('payrollEmployee_btn_delete'),
		pagoSalarioTbody  = document.getElementById('tbody_payrollEmployee');

		var pagoSalarioIdentificationFunction = function(e){
			if (e.keyCode == 13) {
		        $.ajax({
					url:  "Controllers/Planilla.php",
					type: "POST",
					data: "code="  + pagoSalarioIdentification.value + 
						  "&action=searchByIdColaborator"
				}).done(function(message){
					var x = JSON.parse(message);
					if(x[0] != 'error'){
						for (var i = 0; i < x.length; i++) {
							var row = x[i];
							pagoSalarioName.value    = row[0];
							pagoSalarioSurname.value = row[1];
							var num = row[2].replace(/\./g,'');
							if(!isNaN(num)){
								num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
								num = num.split('').reverse().join('').replace(/^[\.]/,'');
								pagoSalarioSalary.value = num;
							}
							pagoSalarioDeduction.value  = row[3];
							pagoSalarioInability.value  = row[4];
						}
					}else{
						pagoSalarioIdentification.value = "El usuario: " + pagoSalarioIdentification.value + " no existe";
					}
				});
		    }
			
		};
		var formatNumber = {
		 	separador: ".", // separador para los miles
		 	sepDecimal: ',', // separador para los decimales
		 	formatear:function (num){
			  	num +='';
			  	var splitStr = num.split('.');
			  	var splitLeft = splitStr[0];
			  	var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
			  	var regx = /(\d+)(\d{3})/;
			  	while (regx.test(splitLeft)) {
			  		splitLeft = splitLeft.replace(regx, '₡1' + this.separador + '₡2');
			  	}
			  	return this.simbol + splitLeft  +splitRight;
	 		},
		 	new:function(num, simbol){
		  		this.simbol = simbol ||'';
		  		return this.formatear(num);
		 	}
		}
		pagoSalarioIdentification.addEventListener("keypress", pagoSalarioIdentificationFunction);
}());