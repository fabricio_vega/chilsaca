(
function(){
	var modify = false,
		filter = 0;

	/******************************** ARTICLE VARIABLES **********************************/
	var articleCode        = document.getElementById("article-code"),
		articleOldCode     = document.getElementById("article-old-code"),
		articleDescription = document.getElementById("article-description"),
		articlePrice1      = document.getElementById("article-price1"),
		articlePrice2      = document.getElementById("article-price2"),
		articlePrice3      = document.getElementById("article-price3"),
		articlePrice4      = document.getElementById("article-price4"),
		articleTax         = document.getElementById("article-tax"),
		articleFreetax     = document.getElementById("article-freetax"),
		articleBtnSave     = document.getElementById("article-btn-save"),
		articleBtnClear    = document.getElementById("article-btn-clear"),
		articleBtnSearch   = document.getElementById("article-btn-search"),
		articleBtnFilter   = document.getElementById("article-btn-filter"),
		articleBtnModify   = document.getElementById("article-btn-modify"),
		articleBtnDelete   = document.getElementById("article-btn-delete"),
		articleTableBody   = document.getElementById("article-table-body");

	var articleSaveFunction = function()
	{
		var cont = 0,
			ok = false;
		do{
			if(articleCode.value === "")
			{
				articleCode.className = "form-control error";
				articleCode.setAttribute("placeholder", "Requerido");
				ok = true;
			}
			if(articleDescription.value === "")
			{
				articleDescription.className = "form-control error";
				articleDescription.setAttribute("placeholder", "Requerido");
				ok = true;
			}
			if(articlePrice1.value === "")
			{
				articlePrice1.className = "form-control error";
				articlePrice1.setAttribute("placeholder", "Requerido");
				ok = true;
			}
			if(articlePrice2.value === "")
			{
				articlePrice2.className = "form-control error";
				articlePrice2.setAttribute("placeholder", "Requerido");
				ok = true;
			}
			if(articlePrice3.value === "")
			{
				articlePrice3.className = "form-control error";
				articlePrice3.setAttribute("placeholder", "Requerido");
				ok = true;
			}
			if(articlePrice4.value === "")
			{
				articlePrice4.className = "form-control error";
				articlePrice4.setAttribute("placeholder", "Requerido");
				ok = true;
			}
			cont++;
		}
		while(cont <= 6);
		
		if(ok)
		{
			return;
		}
		
		if(modify)
		{
			$.ajax({
				url:'Controllers/Inventory.php',
				type:'POST',
				data:'code='           + articleCode.value        +
				'&OldCode='            + articleOldCode.value     +  
				'&articleDescription=' + articleDescription.value + 
				"&articlePrice1="      + articlePrice1.value      +
				"&articlePrice2="      + articlePrice2.value      + 
				"&articlePrice3="      + articlePrice3.value      + 
				"&articlePrice4="      + articlePrice4.value      + 
				"&articleFreeTax="     + articleFreetax.checked   +   
				"&action=modify"
			}).done(function(message){
				if(message === 'correcto')
				{
					selectAll();
					Alert.render("Artículo modificado satisfactoriamente");
				}
				else
				{
					Alert.render(message);
				}
			});
			selectAll();
			modify = false;
		}
		else
		{
			$.ajax({
				url:'Controllers/Inventory.php',
				type:'POST',
				data:'code='           + articleCode.value        + 
				'&articleDescription=' + articleDescription.value + 
				"&articlePrice1="      + articlePrice1.value      +
				"&articlePrice2="      + articlePrice2.value      + 
				"&articlePrice3="      + articlePrice3.value      + 
				"&articlePrice4="      + articlePrice4.value      + 
				"&articleFreeTax="     + articleFreetax.checked   +   
				"&action=insert"
				
			}).done(function(message){
				if(message === 'correcto')
				{
					selectAll();
					Alert.render("Artículo agregado satisfactoriamente");
				}
				else
				{
					Alert.render(message);
				}
			});
		}
		articleClearFunction();
	};

	var articleClearFunction = function()
	{
		articleOldCode.value = "";
		articleCode.className = "form-control";
		articleCode.setAttribute("placeholder", "Código del artículo");
		articleCode.value     = "";

		articleDescription.className = "form-control";
		articleDescription.setAttribute("placeholder", "Nombre del artículo");
		articleDescription.value     = "";

		articlePrice1.className = "form-control";
		articlePrice1.setAttribute("placeholder", "Precio 1");
		articlePrice1.value     = 1;

		articlePrice2.className = "form-control";
		articlePrice2.setAttribute("placeholder", "Precio 2");
		articlePrice2.value     = 0;

		articlePrice3.className = "form-control";
		articlePrice3.setAttribute("placeholder", "Precio 3");
		articlePrice3.value     = 0;

		articlePrice4.className = "form-control";
		articlePrice4.setAttribute("placeholder", "Precio 4");
		articlePrice4.value     = 0;

		articleTax.checked     = true;
		articleFreetax.checked = false;

		for (var i = 0; i < articleTableBody.rows.length; i++) {
			articleTableBody.rows[i].className = "";
		};

		modify = false;
	};

	var articleSearchFunction = function()
	{

		alert("search");
	};

	var articleFilterFunction = function()
	{

		alert("filter");
	};

	var articleModifyFunction = function()
	{
		for (var i = 0; i < articleTableBody.rows.length; i++) {
			if(articleTableBody.rows[i].className === "warning")
			{
				modify = true;
				articleOldCode.value     = articleTableBody.rows[i].cells[0].innerHTML;
				articleCode.value        = articleTableBody.rows[i].cells[0].innerHTML;
				articleDescription.value = articleTableBody.rows[i].cells[1].innerHTML;
				articlePrice1.value      = articleTableBody.rows[i].cells[2].innerHTML;
				articlePrice2.value      = articleTableBody.rows[i].cells[3].innerHTML;
				articlePrice3.value      = articleTableBody.rows[i].cells[4].innerHTML;
				articlePrice4.value      = articleTableBody.rows[i].cells[5].innerHTML;
				if(articleTableBody.rows[i].cells[6].innerHTML === "Excento")
				{
					articleFreetax.checked = true;
					articleTax.checked     = false;
				}
				else
				{
					articleFreetax.checked = false;
					articleTax.checked     = true;
				}
				return;
			}
		};
		Alert.render("Debe seleccionar la fila que desea modificar");
	};

	var articleDeleteFunction = function()
	{
		for (var i = 0; i < articleTableBody.rows.length; i++) {
			if(articleTableBody.rows[i].className === "warning")
			{
				articleCode.value = articleTableBody.rows[i].cells[0].innerHTML;

				$.ajax({
					url:'Controllers/Inventory.php',
					type:'POST',
					data:'code=' + articleCode.value +
					"&action=delete"
					
				}).done(function(message){
					if(message === 'correcto')
					{
						selectAll();
						Alert.render("Artículo eliminado satisfactoriamente");
					}
					else
					{
						Alert.render(message);
					}
				});
				selectAll();
				return;
			}
		};
		Alert.render("Debe seleccionar la fila que desea eliminar");
	};

	var changeSelection = function()
	{
		for (var i = 0; i < articleTableBody.rows.length; i++) {
			articleTableBody.rows[i].className = "";
		};
		this.className = "warning";
	};

	var articleCodeFunction = function()
	{
		articleCode.className = "form-control";
		articleCode.setAttribute("placeholder", "Código del artículo");
	};

	var articleDescriptionFunction = function()
	{
		articleDescription.className = "form-control";
		articleDescription.setAttribute("placeholder", "Nombre del artículo");
	};

	var articlePrice1Function = function()
	{
		articlePrice1.className = "form-control";
		articlePrice1.setAttribute("placeholder", "Precio 1");
	};

	var articlePrice2Function = function()
	{
		articlePrice2.className = "form-control";
		articlePrice2.setAttribute("placeholder", "Precio 2");
	};

	var articlePrice3Function = function()
	{
		articlePrice3.className = "form-control";
		articlePrice3.setAttribute("placeholder", "Precio 3");
	};

	var articlePrice4Function = function()
	{
		articlePrice4.className = "form-control";
		articlePrice4.setAttribute("placeholder", "Precio 4");
	};

	var selectAll = function(){
		$.ajax({
			url:'Controllers/Inventory.php',
			type:'POST',
			data:'action=select'
		}).done(function(message){
			var m = JSON.parse(message);
			if(m[0] != 'error')
			{
				if ( articleTableBody.hasChildNodes() )
				{
					while ( articleTableBody.childNodes.length >= 1 )
					{
						articleTableBody.removeChild( articleTableBody.firstChild );
					}
				}
				for (var i = 0; i < m.length; i++) 
				{
					var row = m[i];
					var hilera  = document.createElement("tr"), 
				    	celda1      = document.createElement("td"),
				      	celda2      = document.createElement("td"),
				      	celda3      = document.createElement("td"),
				      	celda4      = document.createElement("td"),
				      	celda5      = document.createElement("td"),
				      	celda6      = document.createElement("td"),
				      	celda7      = document.createElement("td"),
				      	textoCelda1 = document.createTextNode(row[0]),
				      	textoCelda2 = document.createTextNode(row[1]),
				      	textoCelda3 = document.createTextNode(row[2]),
				      	textoCelda4 = document.createTextNode(row[3]),
				      	textoCelda5 = document.createTextNode(row[4]),
			      		textoCelda6 = document.createTextNode(row[5]),
			      		textoCelda7 = "";

			      		if(row[6] === '0')
			      		{
			      			textoCelda7 = document.createTextNode("Excento");
			      		}
			      		else
			      		{
			      			textoCelda7 = document.createTextNode("Impuesto de ventas");
			      		}
				      	celda1.appendChild(textoCelda1);
				      	hilera.appendChild(celda1);
				      	celda2.appendChild(textoCelda2);
				      	hilera.appendChild(celda2);
				      	celda3.appendChild(textoCelda3);
				      	hilera.appendChild(celda3);
				      	celda4.appendChild(textoCelda4);
				      	hilera.appendChild(celda4);
				      	celda5.appendChild(textoCelda5);
				      	hilera.appendChild(celda5);
				      	celda6.appendChild(textoCelda6);
				      	hilera.appendChild(celda6);
				      	celda7.appendChild(textoCelda7);
				      	hilera.appendChild(celda7);
				    	articleTableBody.appendChild(hilera);
				};
				for (var i = 0; i < articleTableBody.rows.length; i++) {
					articleTableBody.rows[i].addEventListener("click", changeSelection);
				};
			}
		});
	};

	articleBtnSave.addEventListener("click", articleSaveFunction);
	articleBtnClear.addEventListener("click", articleClearFunction);
	articleBtnSearch.addEventListener("click", articleSearchFunction);
	articleBtnFilter.addEventListener("click", articleFilterFunction);
	articleBtnModify.addEventListener("click", articleModifyFunction);
	articleBtnDelete.addEventListener("click", articleDeleteFunction);
	articleCode.addEventListener("click", articleCodeFunction);
	articleDescription.addEventListener("click", articleDescriptionFunction);
	articlePrice1.addEventListener("click", articlePrice1Function);
	articlePrice2.addEventListener("click", articlePrice2Function);
	articlePrice3.addEventListener("click", articlePrice3Function);
	articlePrice4.addEventListener("click", articlePrice4Function);
	selectAll();
	/*********************************** END ARTICLE ***********************************/

	/*********************************** NEW INVOICE ***********************************/
	var inventoryInputInvoice         = document.getElementById("inventory-input-invoice"),
		inventoryInputCodeProvider    = document.getElementById("inventory-input-code-provider"),
		inventoryInputDate            = document.getElementById("inventory-input-date"),
		inventoryInputRbInvoiceCash   = document.getElementById("inventory-input-rb-invoice-cash"),
		inventoryInputRbInvoiceCredit = document.getElementById("inventory-input-rb-invoice-credit"),
		fullProviderName              = document.getElementById("full-provider-name"),
		inventoryBtnSearchArticle     = document.getElementById("inventory-btn-search-article"),
		inventoryInputArticleCode     = document.getElementById("inventory-input-article-code"),
		inventoryInputArticleName     = document.getElementById("inventory-input-article-name"),
		inventoryInputArticlePrice    = document.getElementById("inventory-input-article-price"),
		inventoryInputArticleDiscount = document.getElementById("inventory-input-article-discount"),
		inventoryInputArticleQuantity = document.getElementById("inventory-input-article-quantity"),
		inventoryInputArticleAdd      = document.getElementById("inventory-input-article-add"),
		inventoryInvoiceSubtotal      = document.getElementById("inventory-invoice-subtotal"),
		inventoryInvoiceDiscount      = document.getElementById("inventory-invoice-discount"),
		inventoryInvoiceTax           = document.getElementById("inventory-invoice-tax"),
		inventoryInvoiceService       = document.getElementById("inventory-invoice-service"),
		inventoryInvoiceTotal         = document.getElementById("inventory-invoice-total"),
		btnSaveInventoryInvoice       = document.getElementById("btn-save-inventory-invoice"),
		inventoryArticleTableBody     = document.getElementById("inventory-article-table-body");

	inventoryInputRbInvoiceCash.checked = true;

	var inventoryInputInvoiceFunction         = function()
	{
		inventoryInputInvoice.className = "form-control";
		inventoryInputInvoice.setAttribute("placeholder", "Número de factura");
	};

	var inventoryInputCodeProviderFunction    = function()
	{
		inventoryInputCodeProvider.className = "form-control";
		inventoryInputCodeProvider.setAttribute("placeholder", "Código del proveedor");
	};

	var inventoryInputDateFunction            = function()
	{

		inventoryInputDate.className = "form-control";
	};

	var inventoryInputArticleCodeFunction     = function()
	{
		inventoryInputArticleCode.className = "form-control";
		inventoryInputArticleCode.setAttribute("placeholder", "Código artículo");
	};

	var inventoryInputArticlePriceFunction    = function()
	{
		inventoryInputArticlePrice.className = "form-control";
		inventoryInputArticlePrice.setAttribute("placeholder", "Cantidad");
	};

	var inventoryInputArticleQuantityFunction = function()
	{
		inventoryInputArticleQuantity.className = "form-control";
		inventoryInputArticleQuantity.setAttribute("placeholder", "Precio");
	};

	var inventoryInvoiceServiceFunction       = function()
	{	
		var subtotal = 0,
			discount = 0,
			tax      = 0,
			total    = 0,
			service  = 0;

		for (var i = 0; i < inventoryArticleTableBody.rows.length; i++) 
		{
			subtotal += parseFloat(inventoryArticleTableBody.rows[i].cells[2].innerHTML) * parseFloat(inventoryArticleTableBody.rows[i].cells[3].innerHTML);
			discount += parseFloat(inventoryArticleTableBody.rows[i].cells[4].innerHTML);
		};

		tax = (subtotal - discount) * 0.13;
		total = subtotal + tax;

		inventoryInvoiceSubtotal.value = subtotal;
		inventoryInvoiceDiscount.value = discount;
		inventoryInvoiceTax.value      = tax;
		if(inventoryInvoiceService.value > 0){
			service = parseFloat(inventoryInvoiceService.value);
		}
		inventoryInvoiceTotal.value    = total + service;
	};

	var btnSaveInventoryInvoiceFunction       = function()
	{
		var ok   = false;
		
		if(inventoryInputInvoice.value === "")
		{
			inventoryInputInvoice.className = "form-control error";
			inventoryInputInvoice.setAttribute("placeholder", "Campo requerido");
			ok = true;
		}
		if(inventoryInputCodeProvider.value === "")
		{
			inventoryInputCodeProvider.className = "form-control error";
			inventoryInputCodeProvider.setAttribute("placeholder", "Campo requerido");
			ok = true;
		}
		if(inventoryInputDate.value === "")
		{
			inventoryInputDate.className = "form-control error";
			ok = true;
		}
		
		
		if(ok)
		{
			return;
		}

		if(inventoryInvoiceTotal.value <= 0)
		{
			Alert.render("Debe agregar al menos un artículo");
			return;
		}
	};

	var deleteArticle                         = function()
	{
		this.parentNode.removeChild(this); //Eliminar el elemento seleccionado
		GetTotalInvoice();
	};

	var GetTotalInvoice                       = function()
	{
		var subtotal = 0,
			discount = 0,
			tax      = 0,
			total    = 0;

		for (var i = 0; i < inventoryArticleTableBody.rows.length; i++) 
		{
			subtotal += parseFloat(inventoryArticleTableBody.rows[i].cells[2].innerHTML) * parseFloat(inventoryArticleTableBody.rows[i].cells[3].innerHTML);
			discount += parseFloat(inventoryArticleTableBody.rows[i].cells[4].innerHTML);
		};

		tax = (subtotal - discount) * 0.13;
		total = subtotal + tax;

		inventoryInvoiceSubtotal.value = subtotal;
		inventoryInvoiceDiscount.value = discount;
		inventoryInvoiceTax.value      = tax;
		inventoryInvoiceTotal.value    = total;
	};

	var inventoryInputArticleAddFunction = function()
	{
		var parentValue           = document.createElement("tr"), //Crea un elemento tr
			CodechildrenValue     = document.createElement("td"), //Crea un elemento td para el código del artículo
			ArticlechildrenValue  = document.createElement("td"), //Crea un elemento td para el nombre del artículo
			QuantitychildrenValue = document.createElement("td"), //Crea un elemento td para la cantidad del artículo
			PricechildrenValue    = document.createElement("td"), //Crea un elemento td para el precio del artículo
			DiscountchildrenValue = document.createElement("td"), //Crea un elemento td para el descuento del artículo
			Codecontent           = document.createTextNode(inventoryInputArticleCode.value), //Valor en el input de Código del panel Nuevo artículo
			Articlecontent        = document.createTextNode(inventoryInputArticleName.value), //Valor en el input de Artículo del panel Nuevo artículo
			Pricecontent          = document.createTextNode(inventoryInputArticlePrice.value), //Valor en el input de Precio del panel Nuevo artículo
			Discountcontent       = document.createTextNode(inventoryInputArticleDiscount.value), //Valor en el input de Descuento del panel Nuevo artículo
			Quantitycontent       = document.createTextNode(inventoryInputArticleQuantity.value), //Valor en el input de Cantidad del panel Nuevo artículo
			ok   = false;
		
		if(inventoryInputArticleCode.value === "")
		{
			inventoryInputArticleCode.className = "form-control error";
			inventoryInputArticleCode.setAttribute("placeholder", "Campo requerido");
			ok = true;
		}
		if(inventoryInputArticleQuantity.value === "")
		{
			inventoryInputArticleQuantity.className = "form-control error";
			inventoryInputArticleQuantity.setAttribute("placeholder", "Campo requerido");
			ok = true;
		}
		if(inventoryInputArticlePrice.value === "")
		{
			inventoryInputArticlePrice.className = "form-control error";
			inventoryInputArticlePrice.setAttribute("placeholder", "Campo requerido");
			ok = true;
		}
		if(inventoryInputArticleQuantity.value <= 0)
		{
			inventoryInputArticleQuantity.className = "form-control error";
			inventoryInputArticleQuantity.setAttribute("placeholder", "Debe ser mayor a 0");
			ok = true;
		}
		if(inventoryInputArticlePrice.value <= 0)
		{
			inventoryInputArticlePrice.className = "form-control error";
			inventoryInputArticlePrice.setAttribute("placeholder", "Debe ser mayor a 0");
			ok = true;
		}
		if(inventoryInputArticleDiscount.value === '')
		{
			inventoryInputArticleDiscount.value = 0;
			Discountcontent = document.createTextNode(inventoryInputArticleDiscount.value);
		}

		if(ok)
		{
			return;
		}

		parentValue.className = "article-selected"; //se agrega el atributo necesario
		CodechildrenValue.appendChild(Codecontent); //Código del artículo
		parentValue.appendChild(CodechildrenValue); //Se agregar los td dentro de los tr
		
		ArticlechildrenValue.appendChild(Articlecontent); //Nombre del artículo
		parentValue.appendChild(ArticlechildrenValue); //Se agregar los td dentro de los tr
		
		QuantitychildrenValue.appendChild(Quantitycontent); //Cantidad
		parentValue.appendChild(QuantitychildrenValue); //Se agregar los td dentro de los tr
		
		PricechildrenValue.appendChild(Pricecontent); // Precio
		parentValue.appendChild(PricechildrenValue); //Se agregar los td dentro de los tr
		
		DiscountchildrenValue.appendChild(Discountcontent); //Descuento
		parentValue.appendChild(DiscountchildrenValue); //Se agregar los td dentro de los tr
		
		inventoryArticleTableBody.appendChild(parentValue); //Se agrega el nuevo tr al tbody
		
		//Limpiar los valores
		inventoryInputArticleCode.value     = "";
		inventoryInputArticlePrice.value    = "";
		inventoryInputArticleName.value     = "";
		inventoryInputArticleQuantity.value = "";
		inventoryInputArticleDiscount.value = "";

		//Vuelve a agregar el evento clic a cada item de la lista en el panel Lista de artículos añadados
		for (var i = 0; i < inventoryArticleTableBody.rows.length; i++) 
		{
			inventoryArticleTableBody.rows[i].addEventListener("click", deleteArticle);
		};

		GetTotalInvoice();
		inventoryInputArticleCode.focus(); //Le dá foco al input Código del panel Nuevo Artículo
	};

	var inventoryBtnSearchArticleFunction = function()
	{

		filter = 1;
	};

	inventoryInputInvoice.addEventListener("click", inventoryInputInvoiceFunction);
	inventoryInputCodeProvider.addEventListener("click", inventoryInputCodeProviderFunction);
	inventoryInputDate.addEventListener("click", inventoryInputDateFunction);
	inventoryInputArticleCode.addEventListener("click", inventoryInputArticleCodeFunction);
	inventoryInputArticlePrice.addEventListener("click", inventoryInputArticlePriceFunction);
	inventoryInputArticleQuantity.addEventListener("click", inventoryInputArticleQuantityFunction);
	inventoryInvoiceService.addEventListener("blur", inventoryInvoiceServiceFunction);
	btnSaveInventoryInvoice.addEventListener("click", btnSaveInventoryInvoiceFunction);
	inventoryInputArticleAdd.addEventListener("click", inventoryInputArticleAddFunction);
	inventoryBtnSearchArticle.addEventListener("click", inventoryBtnSearchArticleFunction);
	/********************************** END NEW INVOICE ********************************/

	/***************************** ADD IN MANUAL ARTICLES ******************************/
	var numberAddManualArticle      = document.getElementById("code-add-manual-article"),
		dateAddManualArticle        = document.getElementById("date-add-manual-article"),
		descriptionAddManualArticle = document.getElementById("description-add-manual-article"),
		articleCodeAddManualArticle = document.getElementById("article-code-add-manual-article"),
		quantityAddManualArticle    = document.getElementById("quantity-add-manual-article"),
		noteAddManualArticle        = document.getElementById("note-add-manual-article"),
		btnSaveAddManualArticle     = document.getElementById("save-btn-add-manual-article"),
		btnClearAddManualArticle    = document.getElementById("clear-btn-add-manual-article"),
		btnSearchAddManualArticle   = document.getElementById("search-btn-add-manual-article"),
		btnFilterAddManualArticle   = document.getElementById("filter-btn-add-manual-article"),
		btnModifyAddManualArticle   = document.getElementById("modify-btn-add-manual-article"),
		btnDeleteAddManualArticle   = document.getElementById("delete-btn-add-manual-article"),
		tbodyPaneAddManualArticle   = document.getElementById("tbody-add-manual-article");

	var dateAddManualArticleFunction        = function()
	{
		dateAddManualArticle.className = "form-control";
		dateAddManualArticle.setAttribute("placeholder", "Fecha");
	};

	var articleCodeAddManualArticleFunction = function()
	{
		articleCodeAddManualArticle.className = "form-control";
		articleCodeAddManualArticle.setAttribute("placeholder", "Código del artículo");
	};

	var quantityAddManualArticleFunction    = function()
	{
		quantityAddManualArticle.className = "form-control";
		quantityAddManualArticle.setAttribute("placeholder", "Cantidad a ingresar");
	};

	var btnSaveAddManualArticleFunction     = function()
	{
		var cont = 0,
			ok = false;
		do
		{
			if(dateAddManualArticle.value === "")
			{
				dateAddManualArticle.className = "form-control error";
				dateAddManualArticle.setAttribute("placeholder", "Campo requerido");
				ok = true;
			}
			if(articleCodeAddManualArticle.value === "")
			{
				articleCodeAddManualArticle.className = "form-control error";
				articleCodeAddManualArticle.setAttribute("placeholder", "Campo requerido");
				ok = true;
			}
			if(quantityAddManualArticle.value === "")
			{
				quantityAddManualArticle.className = "form-control error";
				quantityAddManualArticle.setAttribute("placeholder", "Campo requerido");
				ok = true;
			}
			cont++;
		}
		while(cont <= 3);

		if(ok)
		{
			return;
		}
		if(modify)
		{
			$.ajax({
				url: 'Controllers/Inventory.php',
				type: 'POST',
				data: 'Id='     + numberAddManualArticle.value      + 
				'&date='        + dateAddManualArticle.value        + 
				'&articleCode=' + articleCodeAddManualArticle.value +
				'&quantity='    + quantityAddManualArticle.value    +
				'&note='        + noteAddManualArticle.value        +
				'&action=modifyAddManual'
			}).done(function(message){
				if(message === 'correcto')
				{
					Alert.render("Ingreso manual modificado satisfactoriamente");
					selectAllAddManualArticles();
				}
				else
				{
					Alert.render(message);
					return;
				}
			});
			modify = false;
		}
		else
		{
			$.ajax({
				url: 'Controllers/Inventory.php',
				type: 'POST',
				data: 'date='      + dateAddManualArticle.value        + 
				'&articleCode='    + articleCodeAddManualArticle.value +
				'&quantity='       + quantityAddManualArticle.value    +
				'&note='           + noteAddManualArticle.value        +
				'&action=insertAddManual'
			}).done(function(message){
				if(message === 'correcto')
				{
					Alert.render("Ingreso manual agregado satisfactoriamente");
					selectAllAddManualArticles();
				}
				else
				{
					Alert.render(message);
					return;
				}
			});
		}
		btnClearAddManualArticleFunction();
	};

	var btnClearAddManualArticleFunction    = function()
	{
		numberAddManualArticle.value   = "";
		dateAddManualArticle.value     = "";
		dateAddManualArticle.className = "form-control";
		dateAddManualArticle.setAttribute("placeholder", "Fecha");
		articleCodeAddManualArticle.value     = "";
		articleCodeAddManualArticle.className = "form-control";
		articleCodeAddManualArticle.setAttribute("placeholder", "Código del artículo");
		quantityAddManualArticle.value     = 1;
		quantityAddManualArticle.className = "form-control";
		quantityAddManualArticle.setAttribute("placeholder", "Cantidad a ingresar");
		descriptionAddManualArticle.value = "";
		noteAddManualArticle.value = "";
		for (var i = 0; i < tbodyPaneAddManualArticle.rows.length; i++) 
		{
			tbodyPaneAddManualArticle.rows[i].className = "";
		};
		modify = false;
	};

	var btnSearchAddManualArticleFunction = function()
	{

		filter = 2;
	};

	var btnFilterAddManualArticleFunction   = function()
	{

		alert("funciona");
	};

	var btnModifyAddManualArticleFunction   = function()
	{
		for (var i = 0; i < tbodyPaneAddManualArticle.rows.length; i++) 
		{
			if(tbodyPaneAddManualArticle.rows[i].className === 'warning')
			{
				modify = true;
				numberAddManualArticle.value      = tbodyPaneAddManualArticle.rows[i].cells[0].innerHTML;
				dateAddManualArticle.value        = tbodyPaneAddManualArticle.rows[i].cells[1].innerHTML; 
				articleCodeAddManualArticle.value = tbodyPaneAddManualArticle.rows[i].cells[2].innerHTML; 
				descriptionAddManualArticle.value = tbodyPaneAddManualArticle.rows[i].cells[3].innerHTML; 
				quantityAddManualArticle.value    = tbodyPaneAddManualArticle.rows[i].cells[4].innerHTML; 
				noteAddManualArticle.value        = tbodyPaneAddManualArticle.rows[i].cells[5].innerHTML;   
				return;
			}
		};
		Alert.render("Debe seleccionar la fila que desea modiicar");
	};

	var btnDeleteAddManualArticleFunction   = function()
	{	
		for (var i = 0; i < tbodyPaneAddManualArticle.rows.length; i++) 
		{
			if(tbodyPaneAddManualArticle.rows[i].className === 'warning')
			{
				numberAddManualArticle.value = tbodyPaneAddManualArticle.rows[i].cells[0].innerHTML;
				$.ajax({
					url: 'Controllers/Inventory.php',
					type: 'POST',
					data: 'Id=' + numberAddManualArticle.value +
					'&action=deleteAddManual'
				}).done(function(message){
					if(message === 'correcto'){
						Alert.render("Ingreso manual eliminado satisfactoriamente");
						selectAllAddManualArticles();
						return;
					}else{
						Alert.render(message);
						return;
					}
				});
			}
		};
		Alert.render("Debe seleccionar la fila que desea eliminar");
	};

	var tbodyPaneAddManualArticleFunction   = function()
	{
		for (var i = 0; i < tbodyPaneAddManualArticle.rows.length; i++) 
		{
			tbodyPaneAddManualArticle.rows[i].className = "";
		};
		this.className = "warning";
	};

	var selectAllAddManualArticles          = function()
	{
		$.ajax({
			url:'Controllers/Inventory.php',
			type:'POST',
			data:'action=selectAllAddManual'
		}).done(function(message){
			var m = JSON.parse(message);
			if(m[0] != 'error')
			{
				if ( tbodyPaneAddManualArticle.hasChildNodes() )
				{
					while ( tbodyPaneAddManualArticle.childNodes.length >= 1 )
					{
						tbodyPaneAddManualArticle.removeChild( tbodyPaneAddManualArticle.firstChild );
					}
				}
				for (var i = 0; i < m.length; i++) 
				{
					var row = m[i];
					var hilera  = document.createElement("tr"), 
				    	celda1      = document.createElement("td"),
				      	celda2      = document.createElement("td"),
				      	celda3      = document.createElement("td"),
				      	celda4      = document.createElement("td"),
				      	celda5      = document.createElement("td"),
				      	celda6      = document.createElement("td"),
				      	textoCelda1 = document.createTextNode(row[0]),
				      	textoCelda2,
				      	date,
				      	spl,
				      	textoCelda3 = document.createTextNode(row[2]),
				      	textoCelda4 = document.createTextNode(row[3]),
				      	textoCelda5 = document.createTextNode(row[4]),
			      		textoCelda6 = document.createTextNode(row[5]);

			      		date = row[1];
			      		spl = date.split(' ');
			      		textoCelda2 = document.createTextNode(spl[0]);
			      		celda3.className = 'hidden';
			      		celda1.appendChild(textoCelda1);
				      	hilera.appendChild(celda1);
				      	celda2.appendChild(textoCelda2);
				      	hilera.appendChild(celda2);
				      	celda3.appendChild(textoCelda3);
				      	hilera.appendChild(celda3);
				      	celda4.appendChild(textoCelda4);
				      	hilera.appendChild(celda4);
				      	celda5.appendChild(textoCelda5);
				      	hilera.appendChild(celda5);
				      	celda6.appendChild(textoCelda6);
				      	hilera.appendChild(celda6);
				    	tbodyPaneAddManualArticle.appendChild(hilera);
				};
				for (var i = 0; i < tbodyPaneAddManualArticle.rows.length; i++) {
					tbodyPaneAddManualArticle.rows[i].addEventListener("click", tbodyPaneAddManualArticleFunction);
				};
			}
		});
	};

	dateAddManualArticle.addEventListener("click", dateAddManualArticleFunction);
	articleCodeAddManualArticle.addEventListener("click", articleCodeAddManualArticleFunction);
	quantityAddManualArticle.addEventListener("click", quantityAddManualArticleFunction);
	btnSaveAddManualArticle.addEventListener("click", btnSaveAddManualArticleFunction);
	btnClearAddManualArticle.addEventListener("click", btnClearAddManualArticleFunction);
	btnSearchAddManualArticle.addEventListener("click", btnSearchAddManualArticleFunction);
	btnFilterAddManualArticle.addEventListener("click", btnFilterAddManualArticleFunction);
	btnModifyAddManualArticle.addEventListener("click", btnModifyAddManualArticleFunction);
	btnDeleteAddManualArticle.addEventListener("click", btnDeleteAddManualArticleFunction);
	selectAllAddManualArticles();
	/*********************************** END ADD IN MANUAL *********************************/

	/********************************** ARTICLE PRICES ************************************/
	 	var articlePricesTable        = document.getElementById("article-prices-table"),
	 		btnSelectionArticlePrices = document.getElementById("btn-selection-article-prices");

 		var btnSelectionArticlePricesFunction = function(){
 			for (var i = 0; i < articlePricesTable.rows.length; i++) 
 			{
 				if(articlePricesTable.rows[i].className === "warning")
 				{
 					inventoryInputArticlePrice.value = articlePricesTable.rows[i].cells[3].innerHTML;
 					$("#articlePrices").modal("hide");
 					return;
 				}
 			};
 		};

 		var articlePricesTableFunction = function()
 		{
 			for (var i = 0; i < articlePricesTable.rows.length; i++) 
 			{
 				articlePricesTable.rows[i].className = "";
 			};
 			this.className = "warning";
 		};

 		btnSelectionArticlePrices.addEventListener("click", btnSelectionArticlePricesFunction);
 	/********************************** END ARTICLE PRICES ************************************/

	/*********************************** SEARCH ARTICLE **********************************/
	var searchArticleName         = document.getElementById("search-article-name"),
		btnSearchArticle          = document.getElementById("btn-search-article"),
		btnSelectionSearchArticle = document.getElementById("btn-selection-search-article"),
		searchArticleTable        = document.getElementById("search-article-table"),
		searchArticleModal        = document.getElementById("searchArticle");

	var searchArticleNameFunction = function(){
		searchArticleName.className = "form-control";
		searchArticleName.setAttribute("placeholder","Digite una palabra clave");
	};

	var btnSearchArticleFunction = function(){
		if(searchArticleName.value === '')
		{
			searchArticleName.className = "form-control error";
			searchArticleName.setAttribute("placeholder","Campo requerido");
			return;
		}
		$.ajax({
			url:'Controllers/Inventory.php',
			type:'POST',
			data:'key=' + searchArticleName.value +
			'&action=selectArticleFilter'
		}).done(function(message){
			var m = JSON.parse(message);
			if(m[0] != 'error')
			{
				if ( searchArticleTable.hasChildNodes() )
				{
					while ( searchArticleTable.childNodes.length >= 1 )
					{
						searchArticleTable.removeChild( searchArticleTable.firstChild );
					}
				}
				for (var i = 0; i < m.length; i++) 
				{
					var row = m[i];
					var hilera      = document.createElement("tr"), 
				    	celda1      = document.createElement("td"),
				      	celda2      = document.createElement("td"),
				      	celda3      = document.createElement("td"),
				      	celda4      = document.createElement("td"),
				      	celda5      = document.createElement("td"),
				      	celda6      = document.createElement("td"),
				      	celda7      = document.createElement("td"),
				      	textoCelda1 = document.createTextNode(row[0]),
				      	textoCelda2 = document.createTextNode(row[1]),
				      	textoCelda3 = document.createTextNode(row[2]),
				      	textoCelda4 = document.createTextNode(row[3]),
				      	textoCelda5 = document.createTextNode(row[4]),
				      	textoCelda6 = document.createTextNode(row[5]),
				      	textoCelda7 = document.createTextNode(row[6]);

				      	celda4.className = "hidden";
				      	celda5.className = "hidden";
				      	celda6.className = "hidden";
				      	celda7.className = "hidden";

				      	celda1.appendChild(textoCelda1);
				      	hilera.appendChild(celda1);
				      	celda2.appendChild(textoCelda2);
				      	hilera.appendChild(celda2);
				      	celda3.appendChild(textoCelda3);
				      	hilera.appendChild(celda3);
				      	celda4.appendChild(textoCelda4);
				      	hilera.appendChild(celda4);
				      	celda5.appendChild(textoCelda5);
				      	hilera.appendChild(celda5);
				      	celda6.appendChild(textoCelda6);
				      	hilera.appendChild(celda6);
				      	celda7.appendChild(textoCelda7);
				      	hilera.appendChild(celda7);
				    	searchArticleTable.appendChild(hilera);
				};
				for (var i = 0; i < searchArticleTable.rows.length; i++) {
					searchArticleTable.rows[i].addEventListener("click", searchArticleTableFunction);
				};
			}
		});
	};

	var searchArticleTableFunction = function(){
		for (var i = 0; i < searchArticleTable.rows.length; i++) 
		{
			searchArticleTable.rows[i].className = "";
		};
		this.className = "warning";
	};

	var btnSelectionSearchArticleFunction = function(){
		for (var i = 0; i < searchArticleTable.rows.length; i++) {
			if(searchArticleTable.rows[i].className === 'warning')
			{
				switch(filter)
				{
					case 1:
						inventoryInputArticleCode.value  = searchArticleTable.rows[i].cells[0].innerHTML;
						inventoryInputArticleName.value  = searchArticleTable.rows[i].cells[1].innerHTML;
						inventoryInputArticlePrice.value = searchArticleTable.rows[i].cells[2].innerHTML;
					break;
					
					case 2:
						articleCodeAddManualArticle.value = searchArticleTable.rows[i].cells[0].innerHTML;
						descriptionAddManualArticle.value = searchArticleTable.rows[i].cells[1].innerHTML;
					break;

					case 3:
						articleCodeAddManualArticle.value = searchArticleTable.rows[i].cells[0].innerHTML;
						descriptionAddManualArticle.value = searchArticleTable.rows[i].cells[1].innerHTML;
					break;
				}
				if ( articlePricesTable.hasChildNodes() )
				{
					while ( articlePricesTable.childNodes.length >= 1 )
					{
						articlePricesTable.removeChild( articlePricesTable.firstChild );
					}
				}
				var hilera1      = document.createElement("tr"),
					hilera2      = document.createElement("tr"), 
					hilera3      = document.createElement("tr"), 
					hilera4      = document.createElement("tr"),
					precio1      = document.createElement("td"),
					precio2      = document.createElement("td"),
					precio3      = document.createElement("td"),
					precio4      = document.createElement("td"),  
			    	celda1       = document.createElement("td"),
			      	celda2       = document.createElement("td"),
			      	celda3       = document.createElement("td"),
			      	celda4       = document.createElement("td"),
			      	celda5       = document.createElement("td"),
			      	celda6       = document.createElement("td"),
			      	celda7       = document.createElement("td"),
			      	celda8       = document.createElement("td"),
			      	celda9       = document.createElement("td"),
			      	celda10      = document.createElement("td"),
			      	celda11      = document.createElement("td"),
			      	celda12      = document.createElement("td"),
			      	preciotextoCelda1  = document.createTextNode("Precio 1"),
			      	preciotextoCelda2  = document.createTextNode("Precio 2"),
			      	preciotextoCelda3  = document.createTextNode("Precio 3"),
			      	preciotextoCelda4  = document.createTextNode("Precio 4"),
			      	textoCelda1  = document.createTextNode(searchArticleTable.rows[i].cells[0].innerHTML),
			      	textoCelda2  = document.createTextNode(searchArticleTable.rows[i].cells[1].innerHTML),
			      	textoCelda3  = document.createTextNode(searchArticleTable.rows[i].cells[2].innerHTML),
			      	textoCelda4  = document.createTextNode(searchArticleTable.rows[i].cells[0].innerHTML),
			      	textoCelda5  = document.createTextNode(searchArticleTable.rows[i].cells[1].innerHTML),
			      	textoCelda6  = document.createTextNode(searchArticleTable.rows[i].cells[3].innerHTML),
			      	textoCelda7  = document.createTextNode(searchArticleTable.rows[i].cells[0].innerHTML),
			      	textoCelda8  = document.createTextNode(searchArticleTable.rows[i].cells[1].innerHTML),
			      	textoCelda9  = document.createTextNode(searchArticleTable.rows[i].cells[4].innerHTML),
			      	textoCelda10 = document.createTextNode(searchArticleTable.rows[i].cells[0].innerHTML),
			      	textoCelda11 = document.createTextNode(searchArticleTable.rows[i].cells[1].innerHTML),
			      	textoCelda12 = document.createTextNode(searchArticleTable.rows[i].cells[5].innerHTML);
		      	
		      	precio1.appendChild(preciotextoCelda1);
		      	hilera1.appendChild(precio1);
		      	celda1.appendChild(textoCelda1);
		      	hilera1.appendChild(celda1);
		      	celda2.appendChild(textoCelda2);
		      	hilera1.appendChild(celda2);
		      	celda3.appendChild(textoCelda3);
		      	hilera1.appendChild(celda3);
		      	articlePricesTable.appendChild(hilera1);

		      	precio2.appendChild(preciotextoCelda2);
		      	hilera2.appendChild(precio2);
		      	celda4.appendChild(textoCelda4);
		      	hilera2.appendChild(celda4);
		      	celda5.appendChild(textoCelda5);
		      	hilera2.appendChild(celda5);
		      	celda6.appendChild(textoCelda6);
		      	hilera2.appendChild(celda6);
		      	articlePricesTable.appendChild(hilera2);

		      	precio3.appendChild(preciotextoCelda3);
		      	hilera3.appendChild(precio3);
		      	celda7.appendChild(textoCelda7);
		      	hilera3.appendChild(celda7);
		      	celda8.appendChild(textoCelda8);
		      	hilera3.appendChild(celda8);
		      	celda9.appendChild(textoCelda9);
		      	hilera3.appendChild(celda9);
		      	articlePricesTable.appendChild(hilera3);

		      	precio4.appendChild(preciotextoCelda4);
		      	hilera4.appendChild(precio4);
		      	celda10.appendChild(textoCelda10);
		      	hilera4.appendChild(celda10);
		      	celda11.appendChild(textoCelda11);
		      	hilera4.appendChild(celda11);
		      	celda12.appendChild(textoCelda12);
		      	hilera4.appendChild(celda12);
				articlePricesTable.appendChild(hilera4);

				for (var i = 0; i < articlePricesTable.rows.length; i++) 
				{
					articlePricesTable.rows[i].addEventListener("click", articlePricesTableFunction);
				};
				$("#searchArticle").modal('hide');
				filter = 0;
			}
		};
	};

	searchArticleName.addEventListener("click", searchArticleNameFunction);
	btnSearchArticle.addEventListener("click", btnSearchArticleFunction);	
	btnSelectionSearchArticle.addEventListener("click", btnSelectionSearchArticleFunction);
	/********************************* END SEARCH INVOICE **********************************/

	/*********************************** SEARCH INVOICE **********************************/
	var searchProviderName         = document.getElementById("search-provider-name"),
		btnSearchProvider          = document.getElementById("btn-search-provider"),
		btnSelectionSearchProvider = document.getElementById("btn-selection-search-provider"),
		searchProviderTable        = document.getElementById("search-provider-table"),
		searchProviderModal        = document.getElementById("searchProvider");

	var searchProviderNameFunction = function()
	{
		searchProviderName.className = "form-control";
		searchProviderName.setAttribute("placeholder","Digite una palabra clave");
	};

	var btnSearchProviderFunction = function()
	{
		if(searchProviderName.value === '')
		{
			searchProviderName.className = "form-control error";
			searchProviderName.setAttribute("placeholder","Campo requerido");
			return;
		}
		$.ajax({
			url:'Controllers/Inventory.php',
			type:'POST',
			data:'key=' + searchProviderName.value +
			'&action=selectProviderFilter'
		}).done(function(message){
			var m = JSON.parse(message);
			if(m[0] != 'error')
			{
				if ( searchProviderTable.hasChildNodes() )
				{
					while ( searchProviderTable.childNodes.length >= 1 )
					{
						searchProviderTable.removeChild( searchProviderTable.firstChild );
					}
				}
				for (var i = 0; i < m.length; i++) 
				{
					var row = m[i];
					var hilera      = document.createElement("tr"), 
				    	celda1      = document.createElement("td"),
				      	celda2      = document.createElement("td"),
				      	textoCelda1 = document.createTextNode(row[0]),
				      	textoCelda2 = document.createTextNode(row[1]);

				      	celda1.appendChild(textoCelda1);
				      	hilera.appendChild(celda1);
				      	celda2.appendChild(textoCelda2);
				      	hilera.appendChild(celda2);
				    	searchProviderTable.appendChild(hilera);
				};
				for (var i = 0; i < searchProviderTable.rows.length; i++) {
					searchProviderTable.rows[i].addEventListener("click", searchProviderTableFunction);
				};
			}
		});
	};

	var searchProviderTableFunction = function()
	{
		for (var i = 0; i < searchProviderTable.rows.length; i++) 
		{
			searchProviderTable.rows[i].className = "";
		};
		this.className = "warning";
	};

	var btnSelectionSearchProviderFunction = function()
	{
		for (var i = 0; i < searchProviderTable.rows.length; i++) 
		{
			if(searchProviderTable.rows[i].className === 'warning')
			{
				inventoryInputCodeProvider.value = searchProviderTable.rows[i].cells[0].innerHTML;
				fullProviderName.innerHTML       = "Nombre del proveedor: <strong style='text-decoration:underline'>" + searchProviderTable.rows[i].cells[1].innerHTML + "</strong>";
				$("#searchProvider").modal('hide');
				filter = 0;
			}
		};
	};

	searchProviderName.addEventListener("click", searchProviderNameFunction);
	btnSearchProvider.addEventListener("click", btnSearchProviderFunction);	
	btnSelectionSearchProvider.addEventListener("click", btnSelectionSearchProviderFunction);
	/********************************* END SEARCH INVOICE **********************************/
}());