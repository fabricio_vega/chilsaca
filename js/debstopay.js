(function(){
	var isModify  = false,
	 	continuar = false,
	 	mensaje   = "",
	 	action    = 0;

 	/*******************************************  GOOD TRUE INVOICES  ********************************/
 	var tbodyReceivableInvoicesList = document.getElementById("tbody-goodtrue-invoices");
 	var selectAllGoodTrueInvoices = function()
 	{

 	};
 	selectAllGoodTrueInvoices();
 	/*******************************************  END GOOD TRUE INVOICES  ****************************/

	/*****************************************   PROVIDER   *******************************************/
	var	providerIdentification    = document.getElementById("provider-identification"),
		providerIdentificationOld = document.getElementById("provider-identification-old"),
		providertName             = document.getElementById("provider-name"),
		providerPhone1            = document.getElementById("provider-phone1"),
		providerEmail             = document.getElementById("provider-email"),
		providerUbication         = document.getElementById("provider-ubication"),
		providerCreditAmount      = document.getElementById("provider-credit-amount"),
		providerCreditDays        = document.getElementById("provider-credit-days"),
		providerCreditThanksDays  = document.getElementById("provider-credit-thanksdays"),
		providerBtnSave           = document.getElementById("provider-btn-save"),
		providerBtnClear          = document.getElementById("provider-btn-clear"),
		providerBtnFilter         = document.getElementById("provider-btn-filter"),
		providerBtnModify         = document.getElementById("provider-btn-modify"),
		providerBtnDelete         = document.getElementById("provider-btn-delete"),
		tbodyProviderList         = document.getElementById("tbody-provider");

	var providerSaveFunction = function()
	{
		var cont = 0;
		var continuar = false;
		do{	
			if(providerIdentification.value === "")
			{
				providerIdentification.setAttribute("placeholder", "Requerido");
				providerIdentification.className = "form-control error";
				continuar = true;
			}
			if(providertName.value === "")
			{
				providertName.setAttribute("placeholder", "Requerido");
				providertName.className = "form-control error";
				continuar = true;
			}
			if(providerPhone1.value === "")
			{
				providerPhone1.setAttribute("placeholder", "Requerido");
				providerPhone1.className = "form-control error";
				continuar = true;
			}
			if(providerEmail.value === "")
			{
				providerEmail.setAttribute("placeholder", "Requerido");
				providerEmail.className = "form-control error";
				continuar = true;
			}
			if(providerCreditAmount.value === "")
			{
				providerCreditAmount.setAttribute("placeholder", "Requerido");
				providerCreditAmount.className = "form-control error";
				continuar = true;
			}
			if(providerCreditDays.value === "")
			{
				providerCreditDays.setAttribute("placeholder", "Requerido");
				providerCreditDays.className = "form-control error";
				continuar = true;
			}
			if(providerCreditThanksDays.value === "")
			{
				providerCreditThanksDays.setAttribute("placeholder", "Requerido");
				providerCreditThanksDays.className = "form-control error";
				continuar = true;
			}
			cont++;
		}while(cont <= 7);
		
		if(continuar)
		{
			return;
		}
		if(isModify)
		{
			$.ajax({
				url:'Controllers/Provider.php',
				type:'POST',
				data:'identification=' + providerIdentification.value + 
				'&identificationOld=' + providerIdentificationOld.value + 
				'&name_suplier='+ providertName.value + 
				"&phone=" + providerPhone1.value +
				"&email=" + providerEmail.value + 
				"&ubication=" + providerUbication.value+ 
				"&credit_amount=" + providerCreditAmount.value+ 
				"&credit_days=" + providerCreditDays.value + 
				"&tanks_days=" + providerCreditThanksDays.value + 
				"&action=modify"
			}).done(function(message){
				if(message === 'correcto')
				{
					SelectAllProvider();
					Alert.render("Proveedor modificado satisfactoriamente");
				}
				else
				{
					Alert.render(message);
				}
			});
			isModify = false; //IMPORTANT
		}
		else
		{
			$.ajax({
				url:'Controllers/Provider.php',
				type:'POST',
				data:'identification=' + providerIdentification.value + 
				'&name_suplier='+ providertName.value + 
				"&phone=" + providerPhone1.value +
				"&email=" + providerEmail.value + 
				"&ubication=" + providerUbication.value+ 
				"&credit_amount=" + providerCreditAmount.value+ 
				"&credit_days=" + providerCreditDays.value + 
				"&tanks_days=" + providerCreditThanksDays.value + 
				"&action=insert"
			}).done(function(message){
				if(message === 'correcto')
				{
					SelectAllProvider();
					Alert.render("Proveedor agregado satisfactoriamente");
				}
				else
				{
					Alert.render(message);
				}
			});
		}
		providerClearFunction();
	};

	var providerClearFunction = function()
	{
		isModify = false;
		providerIdentification.value = "";
		providerIdentificationOld.value = "";
		providertName.value = "";
		providerPhone1.value = "";
		providerEmail.value = "";
		providerUbication.value = "";
		providerCreditAmount.value = 0;
		providerCreditDays.value = 0;
		providerCreditThanksDays.value = 0;

		for (var i = 0; i < tbodyProviderList.rows.length; i++) {
			tbodyProviderList.rows[i].className = "";
		};

		IdentificationChangeErrorInputFunction();
		NameChangeErrorInputFunction();
		PhoneChangeErrorInputFunction();
		EmailChangeErrorInputFunction();
	};

	var providerFilterFunction = function()
	{
		alert("Filtrado");
	};

	var providerModifyFunction = function()
	{
		for (var i = 0; i < tbodyProviderList.rows.length; i++) 
		{
			if(tbodyProviderList.rows[i].className === "warning")
			{
				isModify = true; //IMPORTANT
				providerIdentificationOld.value = tbodyProviderList.rows[i].cells[0].innerHTML;
				providerIdentification.value    = tbodyProviderList.rows[i].cells[0].innerHTML;
				providertName.value             = tbodyProviderList.rows[i].cells[1].innerHTML;
				providerPhone1.value            = tbodyProviderList.rows[i].cells[2].innerHTML;
				providerEmail.value             = tbodyProviderList.rows[i].cells[3].innerHTML;
				providerUbication.value         = tbodyProviderList.rows[i].cells[4].innerHTML;
				providerCreditAmount.value      = tbodyProviderList.rows[i].cells[5].innerHTML;
				providerCreditDays.value        = tbodyProviderList.rows[i].cells[6].innerHTML;
				providerCreditThanksDays.value  = tbodyProviderList.rows[i].cells[7].innerHTML;
				return;
			}
			
		};
		Alert.render("Debe seleccionar la fila que desea modificar");
	};

	var deleteProvider = function()
	{
		$.ajax({
			url:'Controllers/Provider.php',
			type:'POST',
			data:'identification=' + providerIdentification.value + 
			"&action=delete"
		}).done(function(message)
		{
			if(message === 'correcto')
			{
				SelectAllProvider();
				Alert.render("Proveedor eliminado satisfactoriamente");
			}
			else
			{
				Alert.render(message);
			}
		});
	};

	var providerDeleteFunction = function()
	{
		
		for (var i = 0; i < tbodyProviderList.rows.length; i++) 
		{
			if(tbodyProviderList.rows[i].className === "warning")
			{
				providerIdentification.value = tbodyProviderList.rows[i].cells[0].innerHTML;
				deleteProvider();
				return;
			}
		};
		
		Alert.render("Debe seleccionar la fila que desea eliminar");
	};

	var changeSelection= function()
	{
		for (var i = 0; i < tbodyProviderList.rows.length; i++) 
		{
			tbodyProviderList.rows[i].className = "";
		};
		this.className = "warning";
	};
	
	var IdentificationChangeErrorInputFunction = function()
	{
		providerIdentification.setAttribute("placeholder", "Digite la cédula");
		providerIdentification.className = "form-control";
	};

	var NameChangeErrorInputFunction = function()
	{
		providertName.setAttribute("placeholder", "Digite la razón social");
		providertName.className = "form-control";
	};

	var PhoneChangeErrorInputFunction = function()
	{
		providerPhone1.setAttribute("placeholder", "Digite el teléfono");
		providerPhone1.className = "form-control";
	};

	var EmailChangeErrorInputFunction = function()
	{
		providerEmail.setAttribute("placeholder", "Digite el correo");
		providerEmail.className = "form-control";
	};

	var SelectAllProvider = function(){
		$.ajax({
			url:'Controllers/Provider.php',
			type:'POST',
			data:'action=selectProvider'
		}).done(function(message){
			var m = JSON.parse(message);

			if(m[0] != 'error')
			{
				if ( tbodyProviderList.hasChildNodes() )
				{
					while ( tbodyProviderList.childNodes.length >= 1 )
					{
						tbodyProviderList.removeChild( tbodyProviderList.firstChild );
					}
				}
				for (var i = 0; i < m.length; i++) 
				{
					var row = m[i];
					var hilera      = document.createElement("tr"), 
				    	celda1      = document.createElement("td"),
				      	celda2      = document.createElement("td"),
				      	celda3      = document.createElement("td"),
				      	celda4      = document.createElement("td"),
				      	celda5      = document.createElement("td"),
				      	celda6      = document.createElement("td"),
				      	celda7      = document.createElement("td"),
				      	celda8      = document.createElement("td"),
				      	textoCelda1 = document.createTextNode(row[0]),
				      	textoCelda2 = document.createTextNode(row[1]),
				      	textoCelda3 = document.createTextNode(row[2]),
				      	textoCelda4 = document.createTextNode(row[3]),
				      	textoCelda5 = document.createTextNode(row[4]),
			      		textoCelda6 = document.createTextNode(row[5]),
			      		textoCelda7 = document.createTextNode(row[6]),
		      			textoCelda8 = document.createTextNode(row[7]);

				      	celda1.appendChild(textoCelda1);
				      	hilera.appendChild(celda1);
				      	celda2.appendChild(textoCelda2);
				      	hilera.appendChild(celda2);
				      	celda3.appendChild(textoCelda3);
				      	hilera.appendChild(celda3);
				      	celda4.appendChild(textoCelda4);
				      	hilera.appendChild(celda4);
				      	celda5.appendChild(textoCelda5);
				      	hilera.appendChild(celda5);
				      	celda6.appendChild(textoCelda6);
				      	hilera.appendChild(celda6);
				      	celda7.appendChild(textoCelda7);
				      	hilera.appendChild(celda7);
			      		celda8.appendChild(textoCelda8);
				      	hilera.appendChild(celda8);
				    	tbodyProviderList.appendChild(hilera);
				};
				for (var i = 0; i < tbodyProviderList.rows.length; i++) {
					tbodyProviderList.rows[i].addEventListener("click", changeSelection);
				};
			}
		});
	};

	providerBtnSave.addEventListener("click", providerSaveFunction);
	providerBtnClear.addEventListener("click", providerClearFunction);
	providerBtnFilter.addEventListener("click", providerFilterFunction);
	providerBtnModify.addEventListener("click", providerModifyFunction);
	providerBtnDelete.addEventListener("click", providerDeleteFunction);
	
	providerIdentification.addEventListener("click", IdentificationChangeErrorInputFunction);
	providertName.addEventListener("click", NameChangeErrorInputFunction);
	providerPhone1.addEventListener("click", PhoneChangeErrorInputFunction);
	providerEmail.addEventListener("click", EmailChangeErrorInputFunction);
	
	SelectAllProvider();
	/***************************************   END PROVIDER   ***************************************/
	
	/********************************************** ABONOS ********************************************/
	var	paymentNumberInput    = document.getElementById("payment-number-input"),
		paymentNumberInputOld = document.getElementById("payment-numberOld-input"),
		paymentInvoiceInput   = document.getElementById("payment-invoice-input"),
		paymentDateInput      = document.getElementById("payment-date-input"),
		paymentProviderInput  = document.getElementById("payment-provider-input"),
		paymentAmountInput    = document.getElementById("payment-amount-input"),
		paymentNoteInput      = document.getElementById("payment-note-input"),
		paymentBtnSave        = document.getElementById("payment-btn-save"),
		paymentBtnClear       = document.getElementById("payment-btn-clear"),
		paymentBtnSearch      = document.getElementById("payment-btn-search");
		paymentBtnFilter      = document.getElementById("payment-btn-filter"),
		paymentBtnModify      = document.getElementById("payment-btn-modify"),
		paymentBtnDelete      = document.getElementById("payment-btn-delete"),
		tbodyPaymentList      = document.getElementById("tbody-payment");

	var paymentSaveFunction = function()
	{
		var cont = 0,
			ok = false;
		do{
			if(paymentNumberInput.value == "")
			{
				paymentNumberInput.setAttribute("placeholder", "Campo requerido");
				paymentNumberInput.className = "form-control error";
				ok = true;
			}
			if(paymentInvoiceInput.value == "")
			{
				paymentInvoiceInput.setAttribute("placeholder", "Campo requerido");
				paymentInvoiceInput.className = "form-control error";
				ok = true;
			}
			if(paymentDateInput.value == "")
			{
				paymentDateInput.setAttribute("placeholder", "Campo requerido");
				paymentDateInput.className = "form-control error";
				ok = true;
			}
			if(paymentAmountInput.value == "")
			{
				paymentAmountInput.setAttribute("placeholder", "Campo requerido");
				paymentAmountInput.className = "form-control error";
				ok = true;
			}
			cont++;
		}while(cont <= 4);
		
		if(ok)
		{
			return;
		}

		if(isModify)
		{
			$.ajax({
				url:'Controllers/Provider.php',
				type:'POST',
				data:'paymentNumber=' + paymentNumberInput.value    + 
				'&paymentNumberOld='  + paymentNumberInputOld.value + 
				'&paymentInvoice='    + paymentInvoiceInput.value   + 
				"&paymentDate="       + paymentDateInput.value      +
				"&paymentAmount="     + paymentAmountInput.value    + 
				"&paymentNote="       + paymentNoteInput.value      + 
				"&action=modifyPayment"
				
			}).done(function(message)
			{
				
				if(message === 'correcto')
				{
					Alert.render("Abono modificado satisfactoriamente");
					SelectAllPayments();
				}
				else
				{
					Alert.render(message);
				}
			});
			isModify = false; //IMPORTANT
		}
		else
		{
			$.ajax({
				url:'Controllers/Provider.php',
				type:'POST',
				data:'paymentNumber=' + paymentNumberInput.value  + 
				'&paymentInvoice='    + paymentInvoiceInput.value + 
				"&paymentDate="       + paymentDateInput.value    +
				"&paymentAmount="     + paymentAmountInput.value  + 
				"&paymentNote="       + paymentNoteInput.value    + 
				"&action=insertPayment"
			}).done(function(message){
				if(message === 'correcto')
				{
					Alert.render("Abono agregado satisfactoriamente");
					SelectAllPayments();
				}
				else
				{
					Alert.render(message);
				}
			});
		}
	};

	var paymentClearFunction = function()
	{
		paymentNumberInput.value = "";
		paymentInvoiceInput.value = "";
		paymentDateInput.value = "";
		paymentAmountInput.value = "";
		paymentNoteInput.value = "";
		paymentProviderInput.value = "";
		for (var i = 0; i < tbodyPaymentList.rows.length; i++) 
		{
			tbodyPaymentList.rows[i].className = "";
		};

		NumberChangeErrorInputFunction();
		InvoiceChangeErrorInputFunction();
		DateChangeErrorInputFunction();
		AmountChangeErrorInputFunction();
		isModify = false; //IMPORTANT
		action = 0;
	};

	var paymentSearchFunction = function()
	{

		action = 1;
	};

	var paymentFilterFunction = function()
	{

	};

	var paymentModifyFunction = function()
	{

		for (var i = 0; i < tbodyPaymentList.rows.length; i++) 
		{
			if(tbodyPaymentList.rows[i].className === "warning")
			{
				isModify = true; //IMPORTANT
				paymentNumberInput.value    = tbodyPaymentList.rows[i].cells[0].innerHTML;
				paymentNumberInputOld.value = tbodyPaymentList.rows[i].cells[0].innerHTML;
				paymentInvoice.value        = tbodyPaymentList.rows[i].cells[1].innerHTML;
				paymentDateInput.value      = tbodyPaymentList.rows[i].cells[3].innerHTML;
				paymentAmountInput.value    = tbodyPaymentList.rows[i].cells[4].innerHTML;
				paymentNoteInput.value      = tbodyPaymentList.rows[i].cells[6].innerHTML;
				paymentInvoice.disabled;
				return;
			}
		};
		Alert.render("Debe seleccionar la fila que desea modificar");
	};

	var deletePayment = function()
	{
		$.ajax({
			url:'Controllers/Provider.php',
			type:'POST',
			data:'number=' + paymentNumberInput.value + 
			"&action=deletePayment"
		}).done(function(message)
		{
			if(message === 'correcto')
			{
				Alert.render("Registro eliminado satisfactoriamente");
				SelectAllPayments();
			}
			else
			{
				Alert.render(message);
			}
		});
	};

	var paymentDeleteFunction = function()
	{
		for (var i = 0; i < tbodyPaymentList.rows.length; i++) 
		{
			if(tbodyPaymentList.rows[i].className === "warning")
			{
				paymentNumberInput.value    = tbodyPaymentList.rows[i].cells[0].innerHTML;
				deletePayment();
				return;
			}
		};
		Alert.render("Debe seleccionar la fila que desea eliminar");
	};

	var NumberChangeErrorInputFunction = function()
	{
		paymentNumberInput.setAttribute("placeholder", "Número de abono");
		paymentNumberInput.className = "form-control";
	};

	var InvoiceChangeErrorInputFunction = function()
	{
		paymentInvoiceInput.setAttribute("placeholder", "Número de factura");
		paymentInvoiceInput.className = "form-control";
	};

	var DateChangeErrorInputFunction = function()
	{
		
		paymentDateInput.className = "form-control";
	};

	var AmountChangeErrorInputFunction = function()
	{
		paymentAmountInput.setAttribute("placeholder", "Monto del abono");
		paymentAmountInput.className = "form-control";
	};

	var changeSelectionPayment= function()
	{
		for (var i = 0; i < tbodyPaymentList.rows.length; i++) {
			tbodyPaymentList.rows[i].className = "";
		};
		this.className = "warning";
	};

	var SelectAllPayments = function()
	{
		$.ajax({
			url:'Controllers/Provider.php',
			type:'POST',
			data:'action=selectAllPayments'
		}).done(function(message){
			var m = JSON.parse(message);
			if(m[0] != 'error')
			{
				if ( tbodyPaymentList.hasChildNodes() )
				{
					while ( tbodyPaymentList.childNodes.length >= 1 )
					{
						tbodyPaymentList.removeChild( tbodyPaymentList.firstChild );
					}
				}
				for (var i = 0; i < m.length; i++) 
				{
					var row = m[i];
					var hilera      = document.createElement("tr"), 
				    	celda1      = document.createElement("td"),
				      	celda2      = document.createElement("td"),
				      	celda3      = document.createElement("td"),
				      	celda4      = document.createElement("td"),
				      	celda5      = document.createElement("td"),
				      	celda6      = document.createElement("td"),
				      	celda7      = document.createElement("td"),
				      	textoCelda1 = document.createTextNode(row[0]),
				      	textoCelda2 = document.createTextNode(row[1]),
				      	textoCelda3 = document.createTextNode(row[2]),
				      	textoCelda4 = document.createTextNode(row[3]),
				      	textoCelda5 = document.createTextNode(row[4]),
			      		textoCelda6 = document.createTextNode(row[5]),
			      		textoCelda7 = document.createTextNode(row[6]);

			      		celda3.className = "hidden";
				      	celda1.appendChild(textoCelda1);
				      	hilera.appendChild(celda1);
				      	celda2.appendChild(textoCelda2);
				      	hilera.appendChild(celda2);
				      	celda3.appendChild(textoCelda3);
				      	hilera.appendChild(celda3);
				      	celda4.appendChild(textoCelda4);
				      	hilera.appendChild(celda4);
				      	celda5.appendChild(textoCelda5);
				      	hilera.appendChild(celda5);
				      	celda6.appendChild(textoCelda6);
				      	hilera.appendChild(celda6);
				      	celda7.appendChild(textoCelda7);
				      	hilera.appendChild(celda7);
				    	tbodyPaymentList.appendChild(hilera);
				};
				for (var i = 0; i < tbodyPaymentList.rows.length; i++) {
					tbodyPaymentList.rows[i].addEventListener("click", changeSelectionPayment);
				};
			}
		});
	};

	paymentBtnSave.addEventListener("click", paymentSaveFunction);
	paymentBtnClear.addEventListener("click", paymentClearFunction);
	paymentBtnSearch.addEventListener("click", paymentSearchFunction);
	paymentBtnFilter.addEventListener("click", paymentFilterFunction);
	paymentBtnModify.addEventListener("click", paymentModifyFunction);
	paymentBtnDelete.addEventListener("click", paymentDeleteFunction);
	paymentNumberInput.addEventListener("click", NumberChangeErrorInputFunction);
	paymentInvoiceInput.addEventListener("click", InvoiceChangeErrorInputFunction);
	paymentDateInput.addEventListener("click", DateChangeErrorInputFunction);
	paymentAmountInput.addEventListener("click", AmountChangeErrorInputFunction);
	SelectAllPayments();
	/********************************************* END ABONOS  *********************************************/

	/***********************************************  NC   *************************************************/
	var ncInputNumber          = document.getElementById("nc-input-number"),	
		ncInputOldNumber       = document.getElementById("nc-input-old-number"),
		ncInputInvoiceProvider = document.getElementById("nc-input-invoice-provider"),	
		ncInputInvoiceNumber   = document.getElementById("nc-input-invoice-number"),	
		ncInputDate            = document.getElementById("nc-input-date"),	
		ncInputAmount          = document.getElementById("nc-input-amount"),	
		ncInputNote            = document.getElementById("nc-input-note"),	
		ncBtnSave              = document.getElementById("nc-btn-save"),	
		ncBtnClear             = document.getElementById("nc-btn-clear"),	
		ncBtnSearch            = document.getElementById("nc-btn-search"),	
		ncBtnFilter            = document.getElementById("nc-btn-filter"),	
		ncBtnModify            = document.getElementById("nc-btn-modify"),	
		ncBtnDelete            = document.getElementById("nc-btn-delete"),
		ncTableBody            = document.getElementById("nc-table-body");

	var ncInputNumberFunction        = function()
	{
		ncInputNumber.className = "form-control";
		ncInputNumber.setAttribute("placeholder", "Número de nota");
	};

	var ncInputInvoiceNumberFunction = function()
	{

		ncInputInvoiceNumber.className = "form-control";
	};

	var ncInputDateFunction          = function()
	{
		ncInputDate.className = "form-control";
		ncInputDate.setAttribute("placeholder", "Número de nota");
	};

	var ncInputAmountFunction        = function()
	{
		ncInputAmount.className = "form-control";
		ncInputAmount.setAttribute("placeholder", "Monto de nota");
	};

	var ncBtnSaveFunction            = function()
	{
		var cont = 0,
			ok = false;
		do
		{
			if(ncInputNumber.value === "")
			{
				ncInputNumber.className = "form-control error";
				ncInputNumber.setAttribute("placeholder", "Campo requerido");
				ok = true;
			}
			if(ncInputInvoiceNumber.value === "")
			{
				ncInputInvoiceNumber.className = "form-control error";
				ncInputInvoiceNumber.setAttribute("placeholder", "Campo requerido");
				ok = true;
			}
			if(ncInputDate.value === "")
			{
				ncInputDate.className = "form-control error";
				ncInputDate.setAttribute("placeholder", "Campo requerido");
				ok = true;
			}
			if(ncInputAmount.value === "")
			{
				ncInputAmount.className = "form-control error";
				ncInputNumber.setAttribute("placeholder", "Campo requerido");
				ok = true;
			}
			cont++;
		}
		while(cont <= 4);
		if(ok)
		{
			return;
		}

		if(modify)
		{
			$.ajax({
				url:'Controllers/Provider.php',
				type:'POST',
				data: 'oldNumber=' + ncInputOldNumber.value     +
				'&ncNumber='       + ncInputNumber.value        + 
				'&date='           + ncInputDate.value          + 
				'&invoice='        + ncInputInvoiceNumber.value + 
				"&amount="         + ncInputAmount.value        +
				"&note="           + ncInputNote.value          +  
				"&action=modifyNC"
				
			}).done(function(message)
			{
				if(message === 'correcto')
				{
					Alert.render("Nota de crédito modificado satisfactoriamente");
					SelectAllNC();
				}
				else
				{
					Alert.render(message);
				}
			});
			isModify = false; //IMPORTANT
		}
		else
		{
			$.ajax({
				url:'Controllers/Provider.php',
				type:'POST',
				data:'ncNumber=' + ncInputNumber.value        + 
				'&date='         + ncInputDate.value          + 
				'&invoice='      + ncInputInvoiceNumber.value + 
				"&amount="       + ncInputAmount.value        +
				"&note="         + ncInputNote.value          +  
				"&action=insertNC"
				
			}).done(function(message)
			{
				
				if(message === 'correcto')
				{
					Alert.render("Nota de crédito agregada satisfactoriamente");
					isModify = false; //IMPORTANT
					SelectAllNC();
				}
				else
				{
					Alert.render(message);
				}
			});
		}
	};

	var ncBtnClearFunction           = function()
	{
		ncInputNumber.value            = "";
		ncInputInvoiceNumber.value     = "";
		ncInputDate.value              = "";
		ncInputAmount.value            = 1;
		ncInputNumberFunction();
		ncInputInvoiceNumberFunction();
		ncInputDateFunction();
		ncInputAmountFunction();
		for ( var i = 0; i < ncTableBody.rows.length; i++ )
		{
			ncTableBody.rows[i].className = "";
		};
		isModify = false; //IMPORTANT
		action = 0;
	};

	var ncSearchFunction             = function()
	{

		action = 2;
	};

	var ncBtnFilterFunction          = function()
	{

		alert("Filtrando");
	};

	var ncBtnModifyFunction          = function()
	{
		for ( var i = 0; i < ncTableBody.rows.length; i++ )
		{
			if(ncTableBody.rows[i].className === "warning")
			{
				isModify = true; //IMPORTANT;
				ncInputOldNumber.value       = ncTableBody.rows[i].cells[0].innerHTML;
				ncInputNumber.value          = ncTableBody.rows[i].cells[0].innerHTML;         
				ncInputDate.value            = ncTableBody.rows[i].cells[1].innerHTML;
		       	ncInputInvoiceProvider.value = ncTableBody.rows[i].cells[2].innerHTML;
				ncInputInvoiceNumber.value   = ncTableBody.rows[i].cells[3].innerHTML;
				ncInputAmount.value          = ncTableBody.rows[i].cells[4].innerHTML; 
				ncInputNote.value            = ncTableBody.rows[i].cells[5].innerHTML;
				return;
			}
		};
		Alert.render("Debe seleccionar el registro que desea modificar");
	};

	var ncBtnDeleteFunction          = function()
	{
		for ( var i = 0; i < ncTableBody.rows.length; i++ )
		{
			if(ncTableBody.rows[i].className === "warning")
			{
				ncInputNumber.value = ncTableBody.rows[i].cells[0].innerHTML;
				$.ajax({
					url:'Controllers/Provider.php',
					type:'POST',
					data:'ncNumber=' + ncInputNumber.value + 
					"&action=deleteNC"
				}).done(function(message)
				{
					if(message === 'correcto')
					{
						Alert.render("Nota de crédito eliminada satisfactoriamente");
						SelectAllNC();
					}
					else
					{
						Alert.render(message);
					}
				});
				return;
			}
		};
		Alert.render("Debe seleccionar el registro que desea eliminar");
	};

	var ncTableBodyFunction          = function()
	{
		for (var i = 0; i < ncTableBody.rows.length; i++) {
			ncTableBody.rows[i].className = "";
		};
		this.className = "warning";
	};

	var SelectAllNC                  = function()
	{
		$.ajax({
			url:'Controllers/Provider.php',
			type:'POST',
			data:'action=selectAllNC'
		}).done(function(message){
			var m = JSON.parse(message);
			if(m[0] != 'error')
			{
				if ( ncTableBody.hasChildNodes() )
				{
					while ( ncTableBody.childNodes.length >= 1 )
					{
						ncTableBody.removeChild( ncTableBody.firstChild );
					}
				}
				for (var i = 0; i < m.length; i++) 
				{
					var row = m[i];
					var hilera  = document.createElement("tr"), 
				    	celda1      = document.createElement("td"),
				      	celda2      = document.createElement("td"),
				      	celda3      = document.createElement("td"),
				      	celda4      = document.createElement("td"),
				      	celda5      = document.createElement("td"),
				      	celda6      = document.createElement("td"),
				      	textoCelda1 = document.createTextNode(row[0]),
				      	textoCelda2 = document.createTextNode(row[1]),
				      	textoCelda3 = document.createTextNode(row[2]),
				      	textoCelda4 = document.createTextNode(row[3]),
				      	textoCelda5 = document.createTextNode(row[4]),
			      		textoCelda6 = document.createTextNode(row[5]);

				      	celda1.appendChild(textoCelda1);
				      	hilera.appendChild(celda1);
				      	celda2.appendChild(textoCelda2);
				      	hilera.appendChild(celda2);
				      	celda3.appendChild(textoCelda3);
				      	hilera.appendChild(celda3);
				      	celda4.appendChild(textoCelda4);
				      	hilera.appendChild(celda4);
				      	celda5.appendChild(textoCelda5);
				      	hilera.appendChild(celda5);
				      	celda6.appendChild(textoCelda6);
				      	hilera.appendChild(celda6);
				    	ncTableBody.appendChild(hilera);
				};
				for (var i = 0; i < ncTableBody.rows.length; i++) {
					ncTableBody.rows[i].addEventListener("click", ncTableBodyFunction);
				};
			}
		});
	};

	ncInputNumber.addEventListener("click",ncInputNumberFunction);
	ncInputInvoiceNumber.addEventListener("click", ncInputInvoiceNumberFunction);
	ncInputDate.addEventListener("click", ncInputDateFunction);
	ncInputAmount.addEventListener("click", ncInputAmountFunction);
	ncBtnSave.addEventListener("click", ncBtnSaveFunction);
	ncBtnClear.addEventListener("click", ncBtnClearFunction);
	ncBtnSearch.addEventListener("click", ncSearchFunction);
	ncBtnFilter.addEventListener("click", ncBtnFilterFunction);
	ncBtnModify.addEventListener("click", ncBtnModifyFunction);
	ncBtnDelete.addEventListener("click", ncBtnDeleteFunction);
	ncTableBody.addEventListener("click", ncTableBodyFunction);
	SelectAllNC();
	/***********************************************  END NC  ***********************************************/

	/************************************************  ND   ************************************************/
	var ndInputNumber          = document.getElementById("nd-input-number"),	
		ndInputInvoiceProvider = document.getElementById("nd-input-invoice-provider"),	
		ndInputInvoiceNumber   = document.getElementById("nd-input-invoice-number"),	
		ndInputDate            = document.getElementById("nd-input-date"),	
		ndInputAmount          = document.getElementById("nd-input-amount"),	
		ndInputNote            = document.getElementById("nd-input-note"),	
		ndBtnSave              = document.getElementById("nd-btn-save"),	
		ndBtnClear             = document.getElementById("nd-btn-clear"),	
		ndBtnSearch            = document.getElementById("nd-btn-search"),	
		ndBtnFilter            = document.getElementById("nd-btn-filter"),	
		ndBtnModify            = document.getElementById("nd-btn-modify"),	
		ndBtnDelete            = document.getElementById("nd-btn-delete"),
		ndTableBody            = document.getElementById("nd-table-body");

	var ndInputNumberFunction        = function()
	{
		ndInputNumber.className = "form-control";
		ndInputNumber.setAttribute("placeholder", "Número de nota");
	};

	var ndInputInvoiceNumberFunction = function()
	{

		ndInputInvoiceNumber.className = "form-control";
	};

	var ndInputDateFunction          = function()
	{
		ndInputDate.className = "form-control";
		ndInputDate.setAttribute("placeholder", "Número de nota");
	};

	var ndInputAmountFunction        = function()
	{
		ndInputAmount.className = "form-control";
		ndInputAmount.setAttribute("placeholder", "Monto de nota");
	};

	var ndBtnSaveFunction            = function()
	{
		var cont = 0,
			ok = false;
		do
		{
			if(ndInputNumber.value === "")
			{
				ndInputNumber.className = "form-control error";
				ndInputNumber.setAttribute("placeholder", "Campo requerido");
				ok = true;
			}
			if(ndInputInvoiceNumber.value === "")
			{
				ndInputInvoiceNumber.className = "form-control error";
				ndInputInvoiceNumber.setAttribute("placeholder", "Campo requerido");
				ok = true;
			}
			if(ndInputDate.value === "")
			{
				ndInputDate.className = "form-control error";
				ndInputDate.setAttribute("placeholder", "Campo requerido");
				ok = true;
			}
			if(ndInputAmount.value === "")
			{
				ndInputAmount.className = "form-control error";
				ndInputNumber.setAttribute("placeholder", "Campo requerido");
				ok = true;		
			}
			cont++;
		}
		while(cont <= 4);
		if(ok)
		{
			return;
		}

		if(modify)
		{
			$.ajax({
				url:'Controllers/Provider.php',
				type:'POST',
				data: 'oldNumber=' + ndInputOldNumber.value     +
				'&ndNumber='       + ndInputNumber.value        + 
				'&date='           + ndInputDate.value          + 
				'&invoice='        + ndInputInvoiceNumber.value + 
				"&amount="         + ndInputAmount.value        +
				"&note="           + ndInputNote.value          +  
				"&action=modifyND"
				
			}).done(function(message)
			{
				
				if(message === 'correcto')
				{
					Alert.render("Nota de débito modificado satisfactoriamente");
					SelectAllND();
				}
				else
				{
					Alert.render(message);
				}
			});
			isModify = false; //IMPORTANT;
		}
		else
		{
			$.ajax({
				url:'Controllers/Provider.php',
				type:'POST',
				data:'ndNumber=' + ndInputNumber.value        + 
				'&date='         + ndInputDate.value          + 
				'&invoice='      + ndInputInvoiceNumber.value + 
				"&amount="       + ndInputAmount.value        +
				"&note="         + ndInputNote.value          +  
				"&action=insertND"
				
			}).done(function(message)
			{
				
				if(message === 'correcto')
				{
					Alert.render("Nota de débito agregada satisfactoriamente");
					isModify = false; //IMPORTANT
					SelectAllND();
				}
				else
				{
					Alert.render(message);
				}
			});
		}
	};

	var ndBtnClearFunction           = function()
	{
		ndInputNumber.value            = "";
		ndInputInvoiceNumber.value     = "";
		ndInputDate.value              = "";
		ndInputAmount.value            = 1;
		ndInputNumberFunction();
		ndInputInvoiceNumberFunction();
		ndInputDateFunction();
		ndInputAmountFunction();
		for ( var i = 0; i < ndTableBody.rows.length; i++ )
		{
			ndTableBody.rows[i].className = "";
		};
		isModify = false; //IMPORTANT;
		action = 0;
	};

	var ndSearchFunction = function()
	{

		action = 3;
	};

	var ndBtnFilterFunction          = function()
	{
		alert("Filtrando");
	};

	var ndBtnModifyFunction          = function()
	{
		for ( var i = 0; i < ndTableBody.rows.length; i++ )
		{
			if(ndTableBody.rows[i].className === "warning")
			{
				isModify = true; //IMPORTANT;
				ndInputOldNumber.value       = ndTableBody.rows[i].cells[0].innerHTML;
				ndInputNumber.value          = ndTableBody.rows[i].cells[0].innerHTML;         
				ndInputDate.value            = ndTableBody.rows[i].cells[1].innerHTML;
		       	ndInputInvoiceProvider.value = ndTableBody.rows[i].cells[2].innerHTML;
				ndInputInvoiceNumber.value   = ndTableBody.rows[i].cells[3].innerHTML;
				ndInputAmount.value          = ndTableBody.rows[i].cells[4].innerHTML; 
				ndInputNote.value            = ndTableBody.rows[i].cells[5].innerHTML;
				return;
			}
		};
		Alert.render("Debe seleccionar el registro que desea modificar");
	};

	var ndBtnDeleteFunction          = function()
	{
		for ( var i = 0; i < ndTableBody.rows.length; i++ )
		{
			if(ndTableBody.rows[i].className === "warning")
			{
				ndInputNumber.value = ndTableBody.rows[i].cells[0].innerHTML;
				$.ajax({
					url:'Controllers/Provider.php',
					type:'POST',
					data:'ndNumber=' + ndInputNumber.value + 
					"&action=deleteND"
				}).done(function(message)
				{
					
					if(message === 'correcto')
					{
						Alert.render("Nota de débito eliminada satisfactoriamente");
						SelectAllND();
					}
					else
					{
						Alert.render(message);
					}
				});
				return;
			}
		};
		Alert.render("Debe seleccionar el registro que desea eliminar");
	};

	var ndTableBodyFunction          = function()
	{
		for (var i = 0; i < ndTableBody.rows.length; i++) {
			ndTableBody.rows[i].className = "";
		};
		this.className = "warning";
	};

	var SelectAllND                  = function()
	{
		$.ajax({
			url:'Controllers/Provider.php',
			type:'POST',
			data:'action=selectAllND'
		}).done(function(message){
			var m = JSON.parse(message);
			if(m[0] != 'error')
			{
				if ( ndTableBody.hasChildNodes() )
				{
					while ( ndTableBody.childNodes.length >= 1 )
					{
						ndTableBody.removeChild( ndTableBody.firstChild );
					}
				}
				for (var i = 0; i < m.length; i++) 
				{
					var row = m[i];
					var hilera  = document.createElement("tr"), 
				    	celda1      = document.createElement("td"),
				      	celda2      = document.createElement("td"),
				      	celda3      = document.createElement("td"),
				      	celda4      = document.createElement("td"),
				      	celda5      = document.createElement("td"),
				      	celda6      = document.createElement("td"),
				      	textoCelda1 = document.createTextNode(row[0]),
				      	textoCelda2 = document.createTextNode(row[1]),
				      	textoCelda3 = document.createTextNode(row[2]),
				      	textoCelda4 = document.createTextNode(row[3]),
				      	textoCelda5 = document.createTextNode(row[4]),
			      		textoCelda6 = document.createTextNode(row[5]);

				      	celda1.appendChild(textoCelda1);
				      	hilera.appendChild(celda1);
				      	celda2.appendChild(textoCelda2);
				      	hilera.appendChild(celda2);
				      	celda3.appendChild(textoCelda3);
				      	hilera.appendChild(celda3);
				      	celda4.appendChild(textoCelda4);
				      	hilera.appendChild(celda4);
				      	celda5.appendChild(textoCelda5);
				      	hilera.appendChild(celda5);
				      	celda6.appendChild(textoCelda6);
				      	hilera.appendChild(celda6);
				    	ndTableBody.appendChild(hilera);
				};
				for (var i = 0; i < ndTableBody.rows.length; i++) {
					ndTableBody.rows[i].addEventListener("click", ndTableBodyFunction);
				};
			}
		});
	};

	ndInputNumber.addEventListener("click",ndInputNumberFunction);
	ndInputInvoiceNumber.addEventListener("click", ndInputInvoiceNumberFunction);
	ndInputDate.addEventListener("click", ndInputDateFunction);
	ndInputAmount.addEventListener("click", ndInputAmountFunction);
	ndBtnSave.addEventListener("click", ndBtnSaveFunction);
	ndBtnClear.addEventListener("click", ndBtnClearFunction);
	ndBtnSearch.addEventListener("click", ndSearchFunction);
	ndBtnFilter.addEventListener("click", ndBtnFilterFunction);
	ndBtnModify.addEventListener("click", ndBtnModifyFunction);
	ndBtnDelete.addEventListener("click", ndBtnDeleteFunction);
	ndTableBody.addEventListener("click", ndTableBodyFunction);
	SelectAllND();
	/*************************************************  END ND  ********************************************/

	/*******************************************  SEARCH  *********************************************/
	var	providerInputName   = document.getElementById("search-provider-name"),
		btnFilterProvider   = document.getElementById("btn-search-provider"),
		btnSelectedProvider = document.getElementById("btn-selection-search-provider"),
		tableSearchProvider = document.getElementById("search-provider-table"),
		searchForm          = document.getElementById("searchInvoice");

	var FilterProviderFunction = function()
	{
		if(providerInputName.value === "")
		{
			providerInputName.className = "form-control error";
			providerInputName.setAttribute("placeholder", "Campo requerido");
			return;
		}

		$.ajax({
			url:'Controllers/Provider.php',
			type:'POST',
			data:'provider=' + providerInputName.value + 
			"&action=invoiceProvider"
		}).done(function(message){
			var result = JSON.parse(message);
			if ( result[0] != 'error' )
			{
				if ( tableSearchProvider.hasChildNodes() )
				{
					while ( tableSearchProvider.childNodes.length >= 1 )
					{
						tableSearchProvider.removeChild( tableSearchProvider.firstChild );
					}
				}
				for (var i = 0; i < result.length; i++) 
				{
					var row = result[i];
					var hilera  = document.createElement("tr"), 
			    	celda1      = document.createElement("td"),
			      	celda2      = document.createElement("td"),
			      	celda3      = document.createElement("td"),
			      	textoCelda1 = document.createTextNode(row[0]),
			      	textoCelda2 = document.createTextNode(row[1]),
			      	textoCelda3 = document.createTextNode(row[2]);

			      	celda1.appendChild(textoCelda1);
			      	hilera.appendChild(celda1);
			      	celda2.appendChild(textoCelda2);
			      	hilera.appendChild(celda2);
			      	celda3.appendChild(textoCelda3);
			      	hilera.appendChild(celda3);
			    	tableSearchProvider.appendChild(hilera);
				};
				
				for (var i = 0; i < tableSearchProvider.rows.length; i++) {
					tableSearchProvider.rows[i].addEventListener("click", changeSearchSelection);
				};
			}
		});
	};

	var changeSearchSelection = function()
	{
		for (var i = 0; i < tableSearchProvider.rows.length; i++) {
			tableSearchProvider.rows[i].className = '';
		};
		this.className = "warning";
	};

	var AddProviderFunction = function()
	{
		for (var i = 0; i < tableSearchProvider.rows.length; i++) {
			if(tableSearchProvider.rows[i].className === 'warning')
			{
				switch(action)
				{
					case 1:
						paymentInvoiceInput.value  = tableSearchProvider.rows[i].cells[0].innerHTML;
						paymentProviderInput.value = tableSearchProvider.rows[i].cells[1].innerHTML;
					break;

					case 2:
						ndInputInvoiceProvider = tableSearchProvider.rows[i].cells[0].innerHTML;
						ndInputInvoiceNumber = tableSearchProvider.rows[i].cells[1].innerHTML;
					break;

					case 3:
						ncInputInvoiceProvider = tableSearchProvider.rows[i].cells[0].innerHTML;
						ncInputInvoiceNumber = tableSearchProvider.rows[i].cells[1].innerHTML;
					break;
				}
				providerNameChangeErrorFunction();
				providerInputName.value = "";
				$("#searchInvoice").modal("hide");

				action = 0;
				return;
			}
		};
		Alert.render("Debe seleccionar el registro deseado");
	};

	var providerNameChangeErrorFunction = function(){
		providerInputName.className = "form-control";
		providerInputName.setAttribute("placeholder", "Digite una palabra clave");
	};

	btnFilterProvider.addEventListener("click", FilterProviderFunction);
	btnSelectedProvider.addEventListener("click", AddProviderFunction);
	providerInputName.addEventListener("click", providerNameChangeErrorFunction);

	for (var i = 0; i < tableSearchProvider.rows.length; i++) {
		tableSearchProvider.rows[i].addEventListener("click", changeSearchSelection);
	};
	/***************************************** END SEARCH  ********************************************/
}());