(function(){
	var isModify = false;
	
	var tbodyReceivableInvoicesList = document.getElementById("tbody-goodtrue-invoices"); //Lista de facturas vencidas
	
	/*************************** CLIENT ****************************************/
	var	clientIdentification = document.getElementById("client-identification"),
		clientIdentificationOld = document.getElementById("client-identification-old"),
		clientName = document.getElementById("client-name"),
		clienteSurnames = document.getElementById("client-surnames"),
		clientPhone1 = document.getElementById("client-phone1"),
		clientPhone2 = document.getElementById("client-phone2"),
		clientEmail = document.getElementById("client-email"),
		clientUbication = document.getElementById("client-ubication"),
		clientCreditAmount = document.getElementById("client-credit-amount"),
		clientCreditDays = document.getElementById("client-credit-days"),
		clientCreditThanksDays = document.getElementById("client-credit-thanksdays"),
		clientBtnSave = document.getElementById("client-btn-save"),
		clientBtnClear = document.getElementById("client-btn-clear"),
		clientBtnFilter = document.getElementById("client-btn-filter"),
		clientBtnModify = document.getElementById("client-btn-modify"),
		clientBtnDelete = document.getElementById("client-btn-delete")
		tbodyClientList = document.getElementById("tbody-client");


	//Client functions
	var clientSaveFunction = function(){
		if(clientIdentification.value === ""){
			clientIdentification.setAttribute("placeholder", "Requerido");
			clientIdentification.className = "form-control error";
			return;
		}
		else if(clientName.value === ""){
			clientName.setAttribute("placeholder", "Requerido");
			clientName.className = "form-control error";
			return;
		}
		else if(clienteSurnames.value === ""){
			clienteSurnames.setAttribute("placeholder", "Requerido");
			clienteSurnames.className = "form-control error";
			return;
		}
		else if(clientPhone1.value === ""){
			clientPhone1.setAttribute("placeholder", "Requerido");
			clientPhone1.className = "form-control error";
			return;
		}
		else if(clientEmail.value === ""){
			clientEmail.setAttribute("placeholder", "Requerido");
			clientEmail.className = "form-control error";
			return;
		}
		else if(clientCreditAmount.value === ""){
			clientCreditAmount.setAttribute("placeholder", "Requerido");
			clientCreditAmount.className = "form-control error";
			return;
		}
		else if(clientCreditDays.value === ""){
			clientCreditDays.setAttribute("placeholder", "Requerido");
			clientCreditDays.className = "form-control error";
			return;
		}
		else if(clientCreditThanksDays.value === ""){
			clientCreditThanksDays.setAttribute("placeholder", "Requerido");
			clientCreditThanksDays.className = "form-control error";
			return;
		}
		if(!isModify){
			location.href = "http://localhost/software/DataAccess/accountsreceivableSave.php" + 
				"?client_identification=" + clientIdentification.value +
				"&client_name="           + clientName.value + 
				"&client_surnames="       + clienteSurnames.value + 
				"&client_phone1="         + clientPhone1.value + 
				"&client_phone2="         + clientPhone2.value + 
				"&client_email="          + clientEmail.value + 
				"&client_ubication="      + clientUbication.value +
				"&client_credit_amount="  + clientCreditAmount.value + 
				"&client_credit_days="    + clientCreditDays.value + 
				"&client_credit_thanksdays=" + clientCreditThanksDays.value;
		}else{
			location.href = "http://localhost/software/DataAccess/accountsreceivableModify.php" + 
				"?client_identification=" + clientIdentification.value +
				"&client_name="           + clientName.value + 
				"&client_surnames="       + clienteSurnames.value + 
				"&client_phone1="         + clientPhone1.value + 
				"&client_phone2="         + clientPhone2.value + 
				"&client_email="          + clientEmail.value + 
				"&client_ubication="      + clientUbication.value +
				"&client_credit_amount="  + clientCreditAmount.value + 
				"&client_credit_days="    + clientCreditDays.value + 
				"&client_credit_thanksdays=" + clientCreditThanksDays.value +
				"&client_identification_old=" + clientIdentificationOld.value;
		}
	};

	var clientClearFunction = function(){
		isModify = false;
		clientIdentification.value = "";
		clientIdentificationOld.value = "";
		clientName.value = "";
		clienteSurnames.value = "";
		clientPhone1.value = "";
		clientPhone2.value = "";
		clientEmail.value = "";
		clientUbication.value = "";
		clientCreditAmount.value = 1;
		clientCreditDays.value = 1;
		clientCreditThanksDays.value = 0;
		for (var i = 0; i < tbodyClientList.rows.length; i++) {
			tbodyClientList.rows[i].className = "";
		};
	};

	var clientFilterFunction = function(){
		alert("Filtrado");
	};

	var clientModifyFunction = function(){
		isModify = true;

		for (var i = 0; i < tbodyClientList.rows.length; i++) {
			if(tbodyClientList.rows[i].className === "warning"){
				clientIdentificationOld.value = tbodyClientList.rows[i].cells[0].innerHTML;
				clientIdentification.value = tbodyClientList.rows[i].cells[0].innerHTML;
				clientName.value = tbodyClientList.rows[i].cells[1].innerHTML;
				clienteSurnames.value = tbodyClientList.rows[i].cells[2].innerHTML;
				clientPhone1.value = tbodyClientList.rows[i].cells[3].innerHTML;
				clientPhone2.value = tbodyClientList.rows[i].cells[4].innerHTML;
				clientEmail.value = tbodyClientList.rows[i].cells[5].innerHTML;
				clientUbication.value = tbodyClientList.rows[i].cells[6].innerHTML;
				clientCreditAmount.value = tbodyClientList.rows[i].cells[7].innerHTML;
				clientCreditDays.value = tbodyClientList.rows[i].cells[8].innerHTML;
				clientCreditThanksDays.value = tbodyClientList.rows[i].cells[9].innerHTML;
				return;
			}
		};
		alert("Debe seleccionar la fila que desea modificar");
	};

	var clientDeleteFunction = function(){
		for (var i = 0; i < tbodyClientList.rows.length; i++) {
			if(tbodyClientList.rows[i].className === "warning"){
				clientIdentification.value = tbodyClientList.rows[i].cells[0].innerHTML;
				location.href = "http://localhost/software/DataAccess/accountsreceivableDelete.php" + 
				"?client_identification=" + clientIdentification.value;
				return;
			}
		};
		alert("Debe seleccionar la fila que desea eliminar");
	};

	var changeSelection= function(){
		for (var i = 0; i < tbodyClientList.rows.length; i++) {
			tbodyClientList.rows[i].className = "";
		};
		this.className = "warning";
	};

	//Client events
	clientBtnSave.addEventListener("click", clientSaveFunction);
	clientBtnClear.addEventListener("click", clientClearFunction);
	clientBtnFilter.addEventListener("click", clientFilterFunction);
	clientBtnModify.addEventListener("click", clientModifyFunction);
	clientBtnDelete.addEventListener("click", clientDeleteFunction);

	for (var i = 0; i < tbodyClientList.rows.length; i++) {
		tbodyClientList.rows[i].addEventListener("click", changeSelection);
	};
	
	/*************************** FINISH CLIENT*********************************/

	/******************************** ABONOS ********************************/
	/******************************** ABONOS ********************************/

	/******************************** NC ***********************************/
	/************************* FINISH NC ***********************************/

	/********************************* ND **********************************/
	/************************* FINISH NC ***********************************/
}());