(function(){

	//VARIABLES

	//New invoice
	var invoice = document.getElementById("invoice_number"), //Input con el número de factura
		client = document.getElementById("client-code"), //Input con el código del cliente
		fullClientName = document.getElementById("full-client-name"),
		dtpInvoiceDate = document.getElementById("invoice-date"),
		btnsearchInvoice = document.getElementById("btn-search-invoice"), //
		btnsearchClient = document.getElementById("btn-search-client"),
		rbInvoiceCash = document.getElementById("rb-invoice-cash"),
		rbInvoiceCredit = document.getElementById("rb-invoice-credit"),
		rbSearchInvoiceCash = document.getElementById("rb-search-cash"),
		rbSearchInvoiceCredit = document.getElementById("rb-search-credit"),
		rbSearchClientCash = document.getElementById("rb-search-client-cash"),
		rbSearchClientCredit = document.getElementById("rb-search-client-credit"),
		dtpSearchFrom = document.getElementById("dtp-search-date-from"),
		dtpSearchTo = document.getElementById("dtp-search-date-to"),
		iSearchClienteName = document.getElementById("search-client-name"),
		btnFilterInvoice = document.getElementById("btn-filter-invoice"),
		btnFilterClient = document.getElementById("btn-filter-clients"),
		searchListInovice = document.getElementById("search-invoice-table"), //Lista de facturas de acuerdo al filtro en la base de datos
		searchListClient = document.getElementById("search-client-table"), //Lista de clientes de acuerdo al filtro en la base de datos
		btnSelectionSearchInvoice = document.getElementById("btn-selection-search-invoice"),
		btnSelectionSearchClient = document.getElementById("btn-selection-search-client");
		

	//New article
	var code = document.getElementById("articleCode"), //Input código del panel Nuevo artículo
		name = document.getElementById("articleName"), //Input nombre del panel Nuevo artículo
		price = document.getElementById("articlePrice"), //Input precio del panel Nuevo artículo
		discount = document.getElementById("articleDiscount"), //Input descuento del panel Nuevo artículo
		quantity = document.getElementById("articleQuantity"), //Input cantidad a agregar del panel Nuevo artículo
		btnAddArticle = document.getElementById("btn-add-article"), //Botón de agregar un nuevo artículo del panel Nuevo artículo
		btnFilterArticles = document.getElementById("btn-filter-articles"),
		searchListArticle = document.getElementById("search-article-table"),
		btnSelectionArticle = document.getElementById("btn-selection-article");

	//Articles list panel
	var list = document.getElementById("table-body"); //Etiqueta <tbody> del table

	//Totalized panel
	var subtotalInvoice = document.getElementById("invoice-subtotal"), //Input subtotal del panel Desglose de facturación
		discountInvoice = document.getElementById("invoice-discount"), //Input descuento del panel Desglose de facturación
		taxInvoice = document.getElementById("invoice-tax"), //Input impuesto de ventas del panel Desglose de facturación
		serviceInvoice = document.getElementById("invoice-service"), //Input servicio del panel Desglose de facturación
		cont = 0, //Variable contadora para almacenar el último servicio agregado para poder eliminar en caso de cambiar el monto y agregar el nuevo
		totalInvoice = document.getElementById("invoice-total"), //Input total del panel Desglose de facturación
		btngoToPay = document.getElementById("btn-go-to-pay"),
		totalToPay = document.getElementById("total-to-pay"),
		payWith = document.getElementById("pay-with"),
		btnapplyPay = document.getElementById("apply-pay"),
		payTurned = document.getElementById("pay-turned");

	//FUNCIONES
	//Función que agrega un nuevo item al panel de Lista de articulos añadidos
	var addingArticle = function(){
		var parentValue = document.createElement("tr"), //Crea un elemento tr
			CodechildrenValue = document.createElement("td"), //Crea un elemento td para el código del artículo
			ArticlechildrenValue = document.createElement("td"), //Crea un elemento td para el nombre del artículo
			QuantitychildrenValue = document.createElement("td"), //Crea un elemento td para la cantidad del artículo
			PricechildrenValue = document.createElement("td"), //Crea un elemento td para el precio del artículo
			DiscountchildrenValue = document.createElement("td"), //Crea un elemento td para el descuento del artículo
			Codecontent = document.createTextNode(code.value), //Valor en el input de Código del panel Nuevo artículo
			Articlecontent = document.createTextNode(name.value), //Valor en el input de Artículo del panel Nuevo artículo
			Pricecontent = document.createTextNode(price.value), //Valor en el input de Precio del panel Nuevo artículo
			Discountcontent = document.createTextNode(discount.value), //Valor en el input de Descuento del panel Nuevo artículo
			Quantitycontent = document.createTextNode(quantity.value); //Valor en el input de Cantidad del panel Nuevo artículo
		
		if(code.value === "")
		{
			code.setAttribute("placeholder", "Requerido");
			code.className = "form-control error";
			return false;
		}
		else if(name.value === "")
		{
			name.setAttribute("placeholder", "Requerido");
			name.className = "form-control error";
			return false;
		}
		else if(price.value <= 0)
		{
			price.value = "";
			price.setAttribute("placeholder", "Requerido");
			price.className = "form-control error";
			return false;
		}
		else if(quantity.value <= 0)
		{
			quantity.value = "";
			quantity.setAttribute("placeholder", "Requerido");
			quantity.className = "form-control error";
			return false;
		}
		else if(discount.value === "")
		{
			discount.value = 0;
			Discountcontent = document.createTextNode(discount.value); 
		}

		parentValue.className = "article-selected"; //se agrega el atributo necesario
		CodechildrenValue.appendChild(Codecontent); //Código del artículo
		parentValue.appendChild(CodechildrenValue); //Se agregar los td dentro de los tr
		
		ArticlechildrenValue.appendChild(Articlecontent); //Nombre del artículo
		parentValue.appendChild(ArticlechildrenValue); //Se agregar los td dentro de los tr
		
		QuantitychildrenValue.appendChild(Quantitycontent); //Cantidad
		parentValue.appendChild(QuantitychildrenValue); //Se agregar los td dentro de los tr
		
		PricechildrenValue.appendChild(Pricecontent); // Precio
		parentValue.appendChild(PricechildrenValue); //Se agregar los td dentro de los tr
		
		DiscountchildrenValue.appendChild(Discountcontent); //Descuento
		parentValue.appendChild(DiscountchildrenValue); //Se agregar los td dentro de los tr
		
		list.appendChild(parentValue); //Se agrega el nuevo tr al tbody
		
		//Agrega el resultado en el panel de desglose de facturación
		subtotalInvoice.value = parseFloat(subtotalInvoice.value) + (parseFloat(price.value) * parseFloat(quantity.value));
		discountInvoice.value = parseFloat(discountInvoice.value) + parseFloat(discount.value);
		taxInvoice.value = (parseFloat(subtotalInvoice.value) - parseFloat(discountInvoice.value)) * 0.13;
		totalInvoice.value = parseFloat(subtotalInvoice.value) - parseFloat(discountInvoice.value) + parseFloat(taxInvoice.value);
		
		//Limpiar los valores
		code.value = "";
		price.value = "";
		name.value = "";
		discount.value = "";
		quantity.value = "";

		//Vuelve a agregar el evento clic a cada item de la lista en el panel Lista de artículos añadados
		for (var i = 0; i < list.rows.length; i++) {
			list.rows[i].addEventListener("click", deleteArticle);
		};

		code.focus(); //Le dá foco al input Código del panel Nuevo Artículo
	};

	//Función que eliminar un item en el panel de Lista de artículos añadidos
	var deleteArticle = function(){

		var oldPrice = parseFloat(this.cells[3].innerHTML), //Obtiene el campo del tr que almacena el dato en la posicion de Precio
			oldQuantity = parseFloat(this.cells[2].innerHTML), //Obtiene el campo del tr que almacena el dato en la posicion de Cantidad
			oldDiscount = parseFloat(this.cells[4].innerHTML), //Obtiene el campo del tr que almacena el dato en la posicion de Descuento
			oldSubtotal = 0,
			oldTax      = 0,
			oldTotal    = 0;

		this.parentNode.removeChild(this); //Eliminar el elemento seleccionado
		
		oldSubtotal = oldPrice * oldQuantity; //Se calcula el subtotal a eliminar
		oldTax      = (oldSubtotal - oldDiscount) * 0.13; //Se calcula los impuesto a eliminar
		oldTotal    = (oldSubtotal - oldDiscount) + oldTax; //Se calcula el total a eliminar
		
		subtotalInvoice.value = parseFloat(subtotalInvoice.value) - oldSubtotal; //Se rebaja el subtotal actual menor el subtotal a eliminar
		discountInvoice.value = parseFloat(discountInvoice.value) - oldDiscount; //Se rebaja el descuento actual menos el descuento a eliminar
		taxInvoice.value      = parseFloat(taxInvoice.value) - oldTax; //Se rebaja el impuesto actual menos el descuento a eliminar
		totalInvoice.value    = parseFloat(totalInvoice.value) - oldTotal; //Se rebaja el total actual menos el descuento a eliminar
		
		if(parseFloat(serviceInvoice.value) > 0){
			totalInvoice.value = parseFloat(totalInvoice.value) - parseFloat(serviceInvoice.value);
			serviceInvoice.value = "";
		}
	};

	//Función al quitar el foco, añade el valor del servicio al input de Total de la factura
	var addService = function(){
		if(serviceInvoice.value === "")
		{
			serviceInvoice.value = 0;
			serviceInvoice.focus();
			return false;
		}
		else if(serviceInvoice.value < 0)
		{
			serviceInvoice.setAttribute("placeholder", "Debe de digitar un valor positivo");
			serviceInvoice.className = "form-control error";
			return false;
		}
		totalInvoice.value = parseFloat(totalInvoice.value) - parseFloat(cont) + parseFloat(serviceInvoice.value);
		cont = serviceInvoice.value;
	};

	//Función que reestablece el input de Código
	var reestablishCodeInput = function(){
		code.setAttribute("placeholder", "Código");
		code.className = "form-control";
	};

	//Función que reestablece el input del campo Cantidad
	var reestablishQuantityInput = function(){
		quantity.setAttribute("placeholder", "Cantidad");
		quantity.className = "form-control";
	};

	var addingSearchedInvoice = function(){

		for (var i = 0; i < searchListInovice.rows.length; i++) {
			if(searchListInovice.rows[i].getAttribute("class") === "warning"){

				invoice.value = searchListInovice.rows[i].cells[0].innerHTML; //Setea el valor del número de factura al input correspondiente
				client.value = searchListInovice.rows[i].cells[1].innerHTML; //Sete el valor del nombre del cliente al campo correspondiente
				dtpInvoiceDate.value = searchListInovice.rows[i].cells[2].innerHTML; //Sete el valor de fecha al input de fecha
				totalInvoice.value = parseFloat(searchListInovice.rows[i].cells[3].innerHTML); //Setea el total de la factura al campo correspondiente

				//FALTAN MAS DATOS

				invoice.disabled = true;
				client.disabled = true;

				if(rbSearchInvoiceCash.checked){
					rbInvoiceCash.checked = true;
					rbInvoiceCredit.checked = false;
				}else{
					rbInvoiceCredit.checked = true;
					rbInvoiceCash.checked = false;
				}
				
				$('#searchInvoice').modal('hide'); //cierra el modal
			}
		};
	};

	var addingSearchedClient = function(){
		for (var i = 0; i < searchListClient.rows.length; i++) {
			if(searchListClient.rows[i].getAttribute("class") === "warning"){

				client.value = searchListClient.rows[i].cells[0].innerHTML; //Sete el valor del nombre del cliente al campo correspondiente
				fullClientName.innerHTML = "Cédula: " + 
				searchListClient.rows[i].cells[1].innerHTML + 
				" - Nombre: " +
				searchListClient.rows[i].cells[2].innerHTML;

				//FALTAN MAS DATOS
				if(rbSearchClientCash.checked){ //Si radio button de Contado esta chequeado
					rbInvoiceCash.checked = true; //Chequee el radio button de contado de la factura general
					rbInvoiceCredit.checked = false; //Deschequee el radio button de credito de la factura general
				}else{
					rbInvoiceCredit.checked = true;
					rbInvoiceCash.checked = false;
				}
				
				$('#searchClient').modal('hide'); //cierra el modal
			}
		};
	};

	var addingSearchedArticle = function(){
		for (var i = 0; i < searchListArticle.rows.length; i++) {
			if(searchListArticle.rows[i].getAttribute("class") === "warning"){

				code.value = searchListArticle.rows[i].cells[0].innerHTML;
				name.value = searchListArticle.rows[i].cells[1].innerHTML;
				price.value = searchListArticle.rows[i].cells[2].innerHTML;
				$('#searchArticle').modal('hide'); //cierra el modal
			}
		};
	};

	var addFinalAmountToInputPay = function(){
		totalToPay.value = totalInvoice.value;
	};

	var applyPay = function(){
		if(payWith.value === "")
		{
			payWith.setAttribute("placeholder", "Requerido");
			payWith.className = "form-control error";
			return false;
		}

		var result = 0;

		result = parseFloat(totalToPay.value) - parseFloat(payWith.value);
		if(result > 0){
			alert("Aún debe cancelar: " + result);
			return;	
		}
		payTurned.value = result;

		$('#payWindow').modal('hide'); //cierra el modal
		$('#payTurned').modal('show'); //cierra el modal
	};

	var changeSelectionListInvoice = function(){
		for (var i = 0; i < searchListInovice.rows.length; i++) {
			searchListInovice.rows[i].className = "";
		};
		this.className = "warning";
	};

	var changeSelectionListClient = function(){
		for (var i = 0; i < searchListClient.rows.length; i++) {
			searchListClient.rows[i].className = "";
		};
		this.className = "warning";
	};

	var changeSelectionListArticle = function(){
		for (var i = 0; i < searchListArticle.rows.length; i++) {
			searchListArticle.rows[i].className = "";
		};
		this.className = "warning";
	};

	var setEventsInvoicesSearch = function(){
		for (var i = 0; i < searchListInovice.rows.length; i++) {
			searchListInovice.rows[i].addEventListener("click", changeSelectionListInvoice); //Evento al hacer clic al item de la posición i
		};
	};

	var setEventsClientsSearch = function(){
		for (var i = 0; i < searchListClient.rows.length; i++) {
			searchListClient.rows[i].addEventListener("click", changeSelectionListClient); //Evento al hacer clic al item de la posición i
		};
	};

	var setEventsArticlesSearch = function(){
		for (var i = 0; i < searchListArticle.rows.length; i++) {
			searchListArticle.rows[i].addEventListener("click", changeSelectionListArticle); //Evento al hacer clic al item de la posición i
		};
	};

	//EVENTOS
	btnAddArticle.addEventListener("click", addingArticle); //Evento al hacer clic en botón de agregar artículo
	serviceInvoice.addEventListener("blur", addService); //Evento al dejar el focus en el input de Servicio en el panel de Desgloce de Facturación
	code.addEventListener("click", reestablishCodeInput); //Evento al hacer clic en input Código del artículo
	quantity.addEventListener("click", reestablishQuantityInput); //Evento al hacer clic en input Cantidad a agregar
	btnSelectionSearchInvoice.addEventListener("click", addingSearchedInvoice);
	btnSelectionSearchClient.addEventListener("click", addingSearchedClient);
	btnSelectionArticle.addEventListener("click", addingSearchedArticle);
	btngoToPay.addEventListener("click", addFinalAmountToInputPay);
	btnapplyPay.addEventListener("click", applyPay);
	btnFilterInvoice.addEventListener("click", setEventsInvoicesSearch);
	btnFilterClient.addEventListener("click", setEventsClientsSearch);
	btnFilterArticles.addEventListener("click", setEventsArticlesSearch);


	//Recorre todos los item del panel Lista de artículos añadidos
	for (var i = 0; i < list.rows.length; i++) {
		list.rows[i].addEventListener("click", deleteArticle); //Evento al hacer clic al item de la posición i
	};
		
}());