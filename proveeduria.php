<?php
	include 'header.php';
	include 'top-bar.php';
	include 'message_render.php';
	$FirstPane = 'Nuevo registro';
	$SecondPane = 'Listado de ';
?>
	<div class="modal fade options" id="searchInvoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  	<div class="modal-dialog" role="document">
	    	<div class="modal-content">
		      	<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		        		<span aria-hidden="true">&times;</span>
		        	</button>
		        	<h4 class="modal-title" id="myModalLabel">Búsqueda de factura</h4>
		      	</div>
		      	<div class="modal-body">
		        	<div class="panel panel-default panel-responsive">
					  	<div class="panel-heading">
							<!-- HASTA -->
					   		<div class="input-group input-group-md">
							  	<span class="input-group-addon" id="sizing-addon1">Proveedor</span>
							  	<input type="text" id="search-provider-name" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite una palabra clave">
							</div>
							<br />
							<!-- FINISH HASTA INVOICE -->

							<!-- FILTER -->
					   		<div class="input-group input-group-md">
							  	<button type="button" id="btn-search-provider" class="btn btn-primary">
							  		<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
									FILTRAR 
								</button>
							</div>
							<!-- FINISH FILTER INVOICE -->
					  	</div>
					  	<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
					  	<div id="source_code_content" class="tab-content">	
							<div id="tbl_container_demo_grid1" class="table-responsive">
								<table id="tbl_demo_grid1" class="table table-bordered table-hover">
									<!-- TABLE HEAD -->
									<thead>
										<tr id="tbl_demo_grid1_tr_0">
											<th class="th-common">
												Número factura
											</th>
											<th class="th-common">
												Cliente
											</th>
											<th class="th-common">
												Monto
											</th>
										</tr>
									</thead>
									<!-- FINISH TABLE HEAD -->

									<!-- TABLE BODY -->
									<tbody id="search-provider-table" style="cursor:pointer;">
										
									</tbody>
									<!-- FINISH TABLE BODY -->
								</table>
							</div>
						</div>
						<!-- FINISH TABLE RESPONSIVE -->
					</div>
		      	</div>
		      	<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">
		        		<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
		        		Cerrar
		        	</button>
		        	<button type="button" id="btn-selection-search-provider" class="btn btn-success">
		        		<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
		        		Seleccionar
		        	</button>
		      	</div>
	    	</div>
	  	</div>
	</div>

	<div class="container-fluid back-to-session bottom-footer">
		<!-- WEBSITE TITTLE -->
		<div class="row principal-tittle">
			<ol class="breadcrumb">
			  	<li>
			  		<a href="modules.php">
			  			<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
			  			Módulos
			  		</a>
			  	</li>
			  	<li class="active">Proveeduría</li>
			</ol>
			
			<h1 class="text-center first_tittle">
				Módulo de proveeduría
			</h1>
		</div>
		<!-- FINISH WEBSITE TITTLE -->
		
		<!-- PRINCIPAL PANEL -->
		<div class="container-fluid added-bar">
			<div class="row add-articles">
				<!-- MENU BAR PANEL -->
				<nav class="navbar navbar-default">
				  	<div class="container-fluid">
				    	<!-- Brand and toggle get grouped for better mobile display -->
				    	<div class="navbar-header">
					      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					        	<span class="sr-only">Menú</span>
					        	<span class="icon-bar"></span>
					        	<span class="icon-bar"></span>
					        	<span class="icon-bar"></span>
					      	</button>
				    	</div>
				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      	<ul class="nav nav-tabs" role="tablist">
				      		<li role="presentation" class="dropdown active"> 
				      			<a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop1-contents" aria-expanded="false">
				      				Facturas <span class="caret"></span>
				      			</a> 
			      				<ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents"> 
			      					<li class="active">
			      						<a href="#invoices" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1">
			      							Facturas vencidas
			      						</a>
			      					</li> 
			      					<li><a href="#abonos" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2">Abonos</a></li> 
			      					<li><a href="#nc" role="tab" id="dropdown3-tab" data-toggle="tab" aria-controls="dropdown3">Notas de crédito</a></li> 
			      					<li><a href="#nd" role="tab" id="dropdown4-tab" data-toggle="tab" aria-controls="dropdown4">Notas de débito</a></li> 
			      				</ul> 
			      			</li>
			      			<li role="presentation" class=""><a href="#providers" aria-controls="providers" role="tab" data-toggle="tab">Proveedores</a></li>
						</ul>
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>
				<!-- FINISH MENU BAR PANEL -->
	
				<!-- Tab panes -->
			  	<div class="tab-content">
					<!-- INVOICE TO RECEIVABLE -->
					<div role="tabpanel" class="tab-pane active" id="invoices">
						<!-- DATAGRIDVIEW PANEL -->
						<h2 class="text-center menu-item">Facturas vencidas</h2>
						<div class="panel panel-warning">
							<div class="panel-heading">
						    	<h3 class="panel-title"><strong><?php echo $SecondPane . "facturas vencidas"; ?></strong></h3>
						  	</div>

						  	<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
						  	<div id="source_code_content" class="tab-content">	
								<div id="tbl_container_demo_grid1" class="table-responsive">
									<table id="list" class="table table-bordered table-hover">
										<!-- TABLE HEAD -->
										<thead>
											<tr id="tbl_demo_grid1_tr_0">
												<th class="th-common">
													Número de factura
												</th>
												<th class="th-common">
													Proveedor
												</th>
												<th class="th-common">
													Fecha de facturación
												</th>
												<th class="th-common">
													Días vencidos
												</th>
												<th class="th-common">
													Monto de la factura
												</th>
												<th class="th-common">
													Saldo actual
												</th>
											</tr>
										</thead>
										<!-- FINISH TABLE HEAD -->

										<!-- TABLE BODY -->
										<tbody id="" style="cursor:pointer;">
											<?php
												
											?>
										</tbody>
										<!-- FINISH TABLE BODY -->
									</table>
								</div>
							</div>
							<!-- FINISH TABLE RESPONSIVE -->
						</div>
						<!-- FINISH DATAGRIDVIEW PANEL -->
					</div>
					<!-- FINISH INVOICE TO RECEIVABLE -->

			  		<!-- PANEL DE PROVEEDORES -->
				    <div role="tabpanel" class="tab-pane" id="providers">
						<h2 class="text-center menu-item">Proveedores</h2>
						
						<div class="panel-group" id="accordionProviders" role="tablist" aria-multiselectable="true">
							<!-- REGISTER AND MODIFY REGISTERS -->
						  	<div class="panel panel-info">
						    	<div class="panel-heading" role="tab" id="headingOne">
						      		<h4 class="panel-title">
								        <a role="button" data-toggle="collapse" data-parent="#accordionProviders" href="#providerInputs" aria-expanded="true" aria-controls="providerInputs">
								          	<?php echo $FirstPane; ?>
								        </a>
						      		</h4>
						    	</div>
						    	<div id="providerInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						      		<div class="panel-body">
						        		<input type="text" id="provider-identification-old" value="" name="provider-identification-old" class="form-control" style="display:none">
								    	<div class="col-xs-12 col-md-4 col-lg-4">
								    		<div class="input-group">
									      		<div class="input-group-addon">Cédula juridica</div>
									      		<input type="text" id="provider-identification" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite la cédula">
									    	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-4 col-lg-4">
								    		<div class="input-group">
									      		<div class="input-group-addon">Razón social</div>
									      		<input type="text" id="provider-name" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite el nombre">
									    	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-4 col-lg-4">
								    		<div class="input-group">
									      		<div class="input-group-addon">Teléfono</div>
									      		<input type="tel" id="provider-phone1" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite el teléfono">
									    	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-4 col-lg-4">
								    		<div class="input-group">
									      		<div class="input-group-addon">Correo electrónico</div>
									      		<input type="email" id="provider-email" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite un correo electrónico">
									    	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-4 col-lg-4">
								    		<div class="input-group">
									      		<div class="input-group-addon">Ubicación</div>
									      		<input type="text" id="provider-ubication" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite la ubicación">
									    	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-4 col-lg-4">
								    		<div class="input-group">
									      		<div class="input-group-addon">Monto crédito</div>
									      		<input type="number" id="provider-credit-amount" value="0" min="1" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite el monto de crédito">
									    	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-4 col-lg-4">
								    		<div class="input-group">
									      		<div class="input-group-addon">Días de crédito</div>
									      		<input type="number" id="provider-credit-days" value="0" min="1" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite los días del crédito">
									    	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-4 col-lg-4">
								    		<div class="input-group">
									      		<div class="input-group-addon">Días de gracia</div>
									      		<input type="number" id="provider-credit-thanksdays" value="0" min="0" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite los días de gracia">
									    	</div>
									      	<br />
								    	</div>

								    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="provider-btn-save" class="btn btn-info btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
												  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
													Guardar
												</button>
								  			</div>
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="provider-btn-clear" class="btn btn-dafault btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
												  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
													Limpiar
												</button>
								  			</div>
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="provider-btn-filter" class="btn btn-warning btn-lg btn-responsive" aria-label="Left Align" data-toggle="modal">
												  	<span class="glyphicon glyphicon-filte" aria-hidden="true"></span>
													Filtrar
												</button>
								  			</div>
								  		</div>
						        	</div>
						    	</div>
						  	</div>
							<!-- FINISH REGISTER AND MODIFY REGISTERS -->
							
							<!-- DATAGRIDVIEW PANEL -->
						  	<div class="panel panel-warning">
						    	<div class="panel-heading" role="tab" id="headingTwo">
						      		<h4 class="panel-title">
								        <a role="button" data-toggle="collapse" data-parent="#accordionProviders" href="#providerDataGridView" aria-expanded="false" aria-controls="providerDataGridView">
								          	<?php echo $SecondPane . "proveedores"; ?> 
								        </a>
						      		</h4>
						    	</div>
						    	<div id="providerDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
						      		<div class="panel-body">
						        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
									  	<div id="source_code_content" class="tab-content">	
											<div id="tbl_container_demo_grid1" class="table-responsive">
												<table id="list" class="table table-bordered table-hover">
													<!-- TABLE HEAD -->
													<thead>
														<tr id="tbl_demo_grid1_tr_0">
															<th class="th-common">
																Cédula juridica
															</th>
															<th class="th-common">
																Razón social
															</th>
															<th class="th-common">
																Teléfono 
															</th>
															<th class="th-common">
																Correo electrónico
															</th>
															<th class="th-common">
																Ubicación
															</th>
															<th class="th-common">
																Monto de crédito
															</th>
															<th class="th-common">
																Días de crédito
															</th>
															<th class="th-common">
																Días de gracia
															</th>
														</tr>
													</thead>
													<!-- FINISH TABLE HEAD -->

													<!-- TABLE BODY -->
													<tbody id="tbody-provider" style="cursor:pointer;">
														
													</tbody>
													<!-- FINISH TABLE BODY -->
												</table>
											</div>
										</div>
										<!-- FINISH TABLE RESPONSIVE -->
										<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="provider-btn-modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal">
												  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
													Modificar
												</button>
								  			</div>
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="provider-btn-delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal">
												  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
													Eliminar
												</button>
								  			</div>
								  		</div>
						        	</div>
						    	</div>
						  	</div>
						  	<!-- FINISH DATAGRIDVIEW PANEL -->
						</div>
				    </div>
					<!-- FINALIZA PANEL DE PROVEEDORES -->

					<!-- PANEL DE ABONOS -->
				    <div role="tabpanel" class="tab-pane" id="abonos">
				    	<h2 class="text-center menu-item">Abonos</h2>
						
						<div class="panel-group" id="paymentAcordion" role="tablist" aria-multiselectable="true">
						  	<!-- REGISTER AND MODIFY REGISTERS -->
						  	<div class="panel panel-info">
						    	<div class="panel-heading" role="tab" id="headingOne">
						      		<h4 class="panel-title">
								        <a role="button" data-toggle="collapse" data-parent="#paymentAcordion" href="#paymentInputs" aria-expanded="true" aria-controls="paymentInputs">
								          	<?php echo $FirstPane; ?>
								        </a>
						      		</h4>
						    	</div>
							    <div id="paymentInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
							      	<div class="panel-body">
							      		<input type="text" id="payment-numberOld-input" value="" class="form-control" aria-label="" style="display:none;">
								     	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Número de abono</div>
								    			<input type="text" id="payment-number-input" value="" class="form-control" aria-label="" placeholder="Número de abono">
									      	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Proveedor</div>
								    			<input type="text" id="payment-provider-input" value="" class="form-control" aria-label="" placeholder="Nombre del proveedor" disabled>
									      	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Número de factura</div>
								    			<input type="text" id="payment-invoice-input" value="" class="form-control" aria-label="" placeholder="Número de factura">
									      	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Fecha</div>
								    			<input type="date" id="payment-date-input" value="" class="form-control" aria-label="" placeholder="Fecha">
									      	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Monto del abono</div>
								    			<input type="number" id="payment-amount-input" value="1" min="1" class="form-control" aria-label="" placeholder="Monto del abono">
									      	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Nota</div>
									      		<textarea id="payment-note-input" value="" class="form-control" rows="3" placeholder="Nota"></textarea>
									      	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="payment-btn-save" class="btn btn-info btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
												  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
													Guardar
												</button>
								  			</div>
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="payment-btn-clear" class="btn btn-dafault btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
												  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
													Limpiar
												</button>
								  			</div>
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="payment-btn-search" class="btn btn-primary btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
												  	<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
													Buscar factura
												</button>
								  			</div>
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="payment-btn-filter" class="btn btn-warning btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
												  	<span class="glyphicon glyphicon-filte" aria-hidden="true"></span>
													Filtrar
												</button>
								  			</div>
								  		</div>
							     	</div>
							    </div>
						  	</div>
						  	<!-- FINISH REGISTER AND MODIFY REGISTERS -->

						  	<!-- DATAGRIDVIEW PANEL -->
						  	<div class="panel panel-warning">
					    		<div class="panel-heading" role="tab" id="headingTwo">
					      			<h4 class="panel-title">
						        		<a role="button" data-toggle="collapse" data-parent="#paymentAcordion" href="#paymentDataGridView" aria-expanded="true" aria-controls="paymentDataGridView">
					          				<?php echo $SecondPane . "abonos"; ?> 
					        			</a>
					      			</h4>
						    	</div>
						    	<div id="paymentDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
						      		<div class="panel-body">
						        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
									  	<div id="source_code_content" class="tab-content">	
											<div id="tbl_container_demo_grid1" class="table-responsive">
												<table id="list" class="table table-bordered table-hover">
													<!-- TABLE HEAD -->
													<thead>
														<tr id="tbl_demo_grid1_tr_0">
															<th class="th-common">
																Número de nota
															</th>
															<th class="th-common">
																Factura
															</th>
															<th class="th-common">
																Proveedor
															</th>
															<th class="th-common" style="display:none">
																Total factura
															</th>
															<th class="th-common">
																Fecha
															</th>
															<th class="th-common">
																Monto de nota
															</th>
															<th class="th-common">
																Saldo de factura
															</th>
															<th class="th-common">
																Nota
															</th>
														</tr>
													</thead>
													<!-- FINISH TABLE HEAD -->

													<!-- TABLE BODY -->
													<tbody id="tbody-payment" style="cursor:pointer;">
														
													</tbody>
													<!-- FINISH TABLE BODY -->
												</table>
											</div>
										</div>
										<!-- FINISH TABLE RESPONSIVE -->
										<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="payment-btn-modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
												  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
													Modificar
												</button>
								  			</div>
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="payment-btn-delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#">
												  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
													Eliminar
												</button>
								  			</div>
								  		</div>
						        	</div>
						    	</div>
						  	</div>
						  	<!-- FINISH TABLE RESPONSIVE -->
						</div>
				    </div>
					<!-- FINALIZA PANEL DE ABONOS -->

					<!-- PANEL DE NOTAS DE CREDITO -->
				    <div role="tabpanel" class="tab-pane" id="nc">
				    	<h2 class="text-center menu-item">Notas de crédito</h2>
						
						<div class="panel-group" id="creditnotes" role="tablist" aria-multiselectable="true">
							<!-- REGISTER AND MODIFY REGISTERS -->
						  	<div class="panel panel-info">
						    	<div class="panel-heading" role="tab" id="headingOne">
						      		<h4 class="panel-title">
								        <a role="button" data-toggle="collapse" data-parent="#creditnotes" href="#creditnotesInputs" aria-expanded="true" aria-controls="creditnotesInputs">
								          	<?php echo $FirstPane; ?>
								        </a>
						      		</h4>
						    	</div>
							    <div id="creditnotesInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
							      	<div class="panel-body">
							      		<input type="text" id="nc-input-old-number" value="" class="form-control hidden" aria-label="" >
								     	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Número de nota</div>
								    			<input type="text" id="nc-input-number" value="" class="form-control" aria-label="" placeholder="Número de nota">
									      	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Fecha</div>
								    			<input type="date" id="nc-input-date" value="" class="form-control" aria-label="" placeholder="Fecha">
									      	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Proveedor</div>
								    			<input type="text" id="nc-input-invoice-provider" value="" class="form-control" aria-label="" placeholder="Proveedor" disabled>
									      	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Número de factura</div>
								    			<input type="text" id="nc-input-invoice-number" value="" class="form-control" aria-label="" placeholder="Número de factura">
									      	</div>
									      	<p style="color:gray; font-size:10px;">Si conoce el número de factura digitelo en el campo, y sino presione el botón Buscar factura</p>
									      	
								    	</div>
								    	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Monto</div>
								    			<input type="number" id="nc-input-amount" value="1" min="1" class="form-control" aria-label="" placeholder="Monto">
									      	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Nota</div>
								    			<textarea id="nc-input-note" value="" class="form-control" rows="3" placeholder="Nota"></textarea>
									      	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="nc-btn-save" class="btn btn-info btn-lg" aria-label="Left Align" data-toggle="modal" data-target="">
												  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
													Guardar
												</button>
								  			</div>
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="nc-btn-clear" class="btn btn-dafault btn-lg" aria-label="Left Align" data-toggle="modal" data-target="">
												  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
													Limpiar
												</button>
								  			</div>
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="nc-btn-search" class="btn btn-primary btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
												  	<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
													Buscar factura
												</button>
								  			</div>
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="nc-btn-filter" class="btn btn-warning btn-lg" aria-label="Left Align" data-toggle="modal" data-target="">
												  	<span class="glyphicon glyphicon-filte" aria-hidden="true"></span>
													Filtrar
												</button>
								  			</div>
								  		</div>
							     	</div>
							    </div>
						  	</div>
						  	<!-- FINISH REGISTER AND MODIFY REGISTERS -->

						  	<!-- DATAGRIDVIEW PANEL -->
						  	<div class="panel panel-warning">
					    		<div class="panel-heading" role="tab" id="headingTwo">
					      			<h4 class="panel-title">
						        		<a role="button" data-toggle="collapse" data-parent="#creditnotes" href="#creditnotesDataGridView" aria-expanded="false" aria-controls="creditnotesDataGridView">
					          				<?php echo $SecondPane . "notas de crédito"; ?> 
					        			</a>
					      			</h4>
						    	</div>
						    	<div id="creditnotesDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
						      		<div class="panel-body">
						        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
									  	<div id="source_code_content" class="tab-content">	
											<div id="tbl_container_demo_grid1" class="table-responsive">
												<table id="list" class="table table-bordered table-hover">
													<!-- TABLE HEAD -->
													<thead>
														<tr id="tbl_demo_grid1_tr_0">
															<th class="th-common">
																Número de nota
															</th>
															<th class="th-common">
																Fecha
															</th>
															<th class="th-common">
																Proveedor
															</th>
															<th class="th-common">
																Número de Factura
															</th>
															<th class="th-common">
																Monto de nota
															</th>
															<th class="th-common">
																Nota
															</th>
														</tr>
													</thead>
													<!-- FINISH TABLE HEAD -->

													<!-- TABLE BODY -->
													<tbody id="nc-table-body" style="cursor:pointer;">
													</tbody>
													<!-- FINISH TABLE BODY -->
												</table>
											</div>
										</div>
										<!-- FINISH TABLE RESPONSIVE -->
										<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="nc-btn-modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="">
												  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
													Modificar
												</button>
								  			</div>
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="nc-btn-delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="">
												  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
													Eliminar
												</button>
								  			</div>
								  		</div>
						        	</div>
						    	</div>
						  	</div>
						  	<!-- FINISH TABLE RESPONSIVE -->
					  	</div>
				    </div>	
					<!-- FINALIZA PANEL DE NOTAS DE CREDITO -->					

					<!-- PANEL DE NOTAS DE DEBITO -->
				    <div role="tabpanel" class="tab-pane" id="nd">
				    	<h2 class="text-center menu-item">Notas de débito</h2>
						
						<div class="panel-group" id="debitnotes" role="tablist" aria-multiselectable="true">
							<!-- REGISTER AND MODIFY REGISTERS -->
						  	<div class="panel panel-info">
						    	<div class="panel-heading" role="tab" id="headingOne">
						      		<h4 class="panel-title">
								        <a role="button" data-toggle="collapse" data-parent="#debitnotes" href="#debitnotesInputs" aria-expanded="true" aria-controls="debitnotesInputs">
								          	<?php echo $FirstPane; ?>
								        </a>
						      		</h4>
						    	</div>
							    <div id="debitnotesInputs" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
							      	<div class="panel-body">
							      		<input type="text" id="nd-input-old-number" value="" class="form-control hidden" aria-label="" >
								    	<div class="col-xs-12 col-md-6 col-lg-6">
											<div class="input-group">
									      		<div class="input-group-addon">Número de nota</div>
								    			<input type="text" id="nd-input-number" value="" class="form-control" aria-label="" placeholder="Número de nota">
									      	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Fecha</div>
								    			<input type="date" id="nd-input-date" value="" class="form-control" aria-label="" placeholder="Fecha">
									      	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Proveedor</div>
								    			<input type="text" id="nd-input-invoice-provider" value="" class="form-control" aria-label="" placeholder="Proveedor" disabled>
									      	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Número de factura</div>
								    			<input type="text" id="nd-input-invoice-number" value="" class="form-control" aria-label="" placeholder="Número de factura">
									      	</div>
									      	<p style="color:gray; font-size:10px;">Si conoce el número de factura digitelo en el campo, y sino presione el botón Buscar factura</p>
									      	
								    	</div>
								    	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Monto</div>
								    			<input type="number" id="nd-input-amount" value="1" min="1" class="form-control" aria-label="" placeholder="Monto">
									      	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-6 col-lg-6">
								    		<div class="input-group">
									      		<div class="input-group-addon">Nota</div>
								    			<textarea id="nd-input-note" value="" class="form-control" rows="3" placeholder="Nota"></textarea>
									      	</div>
									      	<br />
								    	</div>
								    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="nd-btn-save" class="btn btn-info btn-lg" aria-label="Left Align" data-toggle="modal" data-target="">
												  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
													Guardar
												</button>
								  			</div>
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="nd-btn-clear" class="btn btn-dafault btn-lg" aria-label="Left Align" data-toggle="modal" data-target="">
												  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
													Limpiar
												</button>
								  			</div>
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="nd-btn-search" class="btn btn-primary btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
												  	<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
													Buscar factura
												</button>
								  			</div>
								  			<div class="col-xs-12 col-md-6 col-lg-6">
								  				<button type="button" id="nd-btn-filter" class="btn btn-warning btn-lg" aria-label="Left Align" data-toggle="modal" data-target="">
												  	<span class="glyphicon glyphicon-filte" aria-hidden="true"></span>
													Filtrar
												</button>
								  			</div>
								  		</div>
								  	</div>
							    </div>
						  	</div>
						  	<!-- FINISH REGISTER AND MODIFY REGISTERS -->

						  	<!-- DATAGRIDVIEW PANEL -->
						  	<div class="panel panel-warning">
					    		<div class="panel-heading" role="tab" id="headingTwo">
					      			<h4 class="panel-title">
						        		<a role="button" data-toggle="collapse" data-parent="#debitnotes" href="#debitnotesDataGridView" aria-expanded="false" aria-controls="debitnotesDataGridView">
					          				<?php echo $SecondPane . "notas de débito"; ?>
					        			</a>
					      			</h4>
						    	</div>
						    	<div id="debitnotesDataGridView" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
						      		<div class="panel-body">
						        		<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
							  	<div id="source_code_content" class="tab-content">	
									<div id="tbl_container_demo_grid1" class="table-responsive">
										<table id="list" class="table table-bordered table-hover">
											<!-- TABLE HEAD -->
											<thead>
												<tr id="tbl_demo_grid1_tr_0">
													<th class="th-common">
														Número de nota
													</th>
													<th class="th-common">
														Fecha
													</th>
													<th class="th-common">
														Proveedor
													</th>
													<th class="th-common">
														Número de Factura
													</th>
													<th class="th-common">
														Monto de nota
													</th>
													<th class="th-common">
														Nota
													</th>
												</tr>
											</thead>
											<!-- FINISH TABLE HEAD -->

											<!-- TABLE BODY -->
											<tbody id="nd-table-body" style="cursor:pointer;">
											</tbody>
											<!-- FINISH TABLE BODY -->
										</table>
									</div>
								</div>
								<!-- FINISH TABLE RESPONSIVE -->
								<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
						  			<div class="col-xs-12 col-md-6 col-lg-6">
						  				<button type="button" id="nd-btn-modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="">
										  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
											Modificar
										</button>
						  			</div>
						  			<div class="col-xs-12 col-md-6 col-lg-6">
						  				<button type="button" id="nd-btn-delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="">
										  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
											Eliminar
										</button>
						  			</div>
						  		</div>
						        	</div>
						    	</div>
						  	</div>
						  	<!-- FINISH TABLE RESPONSIVE -->
					  	</div>
				    </div>
				    <!-- FINALIZA PANEL DE NOTAS DE DEBITO -->
			  	</div>
			</div>
		</div>
		<!-- FINISH PRINCIPAL -->
	</div>
<?php
	include "footer-copyright.php";
	include "bootstrap_script.php";
?>

<?php

	include 'footer.php';
?>