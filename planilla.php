<?php
	include 'header.php';
	include 'top-bar.php';
	include 'message_render.php';
	$FirstPane  = 'Nuevo registro';
	$SecondPane = 'Listado de ';
?>
	<div class="container-fluid back-to-session bottom-footer">
		<div class="row principal-tittle">
			<ol class="breadcrumb">
			  	<li>
			  		<a href="modules.php">
			  			<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
			  			Módulos
			  		</a>
			  	</li>
			  	<li class="active">Planilla</li>
			</ol>
			<h1 class="text-center first_tittle">
				Módulo de planilla
			</h1>
		</div>
		<div class="container-fluid added-bar">
			<div class="row add-articles">
				<!-- MENU BAR PANEL -->
				<nav class="navbar navbar-default">
				  	<div class="container-fluid">
				    	<!-- Brand and toggle get grouped for better mobile display -->
				    	<div class="navbar-header">
					      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					        	<span class="sr-only">Menú</span>
					        	<span class="icon-bar"></span>
					        	<span class="icon-bar"></span>
					        	<span class="icon-bar"></span>
					      	</button>
				    	</div>
				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      	<ul class="nav nav-tabs" role="tablist">
				      		<li role="presentation" class="dropdown"> 
				      			<a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop1-contents" aria-expanded="false">
				      				Mantenimiento <span class="caret"></span>
				      			</a> 
			      				<ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents"> 
			      					<li><a href="#departmentAccordionPanel" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1">Departamentos</a></li> 
			      					<li><a href="#jobsPanel" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2">Puestos</a></li> 
			      					<li><a href="#typeOfPayPanel" role="tab" id="dropdown3-tab" data-toggle="tab" aria-controls="dropdown3">Tipos de pago</a></li> 
			      					<li><a href="#typeOfDeductionsPanel" role="tab" id="dropdown4-tab" data-toggle="tab" aria-controls="dropdown4">Tipos de deducciones</a></li>
			      					<li><a href="#typeOfInabilityPanel" role="tab" id="dropdown5-tab" data-toggle="tab" aria-controls="dropdown5">Tipos de incapacidad</a></li>
			      					<li><a href="#colaboratorPanel" role="tab" id="dropdown6-tab" data-toggle="tab" aria-controls="dropdown6">Colaboradores</a></li> 
			      				</ul> 
			      			</li>
			      			<li role="presentation" class="dropdown active"> 
				      			<a href="#" id="myTabDrop2" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop2-contents" aria-expanded="false">
				      				Gestiones <span class="caret"></span>
				      			</a> 
			      				<ul class="dropdown-menu active" aria-labelledby="myTabDrop2" id="myTabDrop2-contents">
			      					<li class="active"><a href="#salarypayPanel" role="tab" id="dropdown7-tab" data-toggle="tab" aria-controls="dropdown7">Generar pago salario</a></li>  
			      					<li><a href="#salaryincreasePanel" role="tab" id="dropdown8-tab" data-toggle="tab" aria-controls="dropdown8">Aumento salarial</a></li> 
			      					<li><a href="#vacationsPanel" role="tab" id="dropdown9-tab" data-toggle="tab" aria-controls="dropdown9">Aplicar vacaciones</a></li> 
			      					<li><a href="#deductionsPanel" role="tab" id="dropdown10-tab" data-toggle="tab" aria-controls="dropdown10">Generar deduciones</a></li> 
			      					<li><a href="#inabilityPanel" role="tab" id="dropdown11-tab" data-toggle="tab" aria-controls="dropdown11">Generar incapacidad</a></li>
			      					<li><a href="#liquidationsPanel" role="tab" id="dropdown12-tab" data-toggle="tab" aria-controls="dropdown12">Liquidaciones</a></li>
			      					<li><a href="#bonusPanel" role="tab" id="dropdown13-tab" data-toggle="tab" aria-controls="dropdown13">Generar aguinaldo</a></li> 
			      				</ul> 
			      			</li>
						</ul>
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>
				<!-- FINISH MENU BAR PANEL -->

				<!-- TAB PANES -->
			  	<div class="tab-content">
			  		<?php
			  			include 'Planilla_GUI/departamentos.php';       // Departamentos
			  			include 'Planilla_GUI/puestos.php';             // Puestos
			  			include 'Planilla_GUI/tipos_pago.php';          // Tipos de pago
			  			include 'Planilla_GUI/tipos_deducciones.php';   // Tipos de deducciones
			  			include 'Planilla_GUI/tipos_incapacidades.php'; // Tipos de incapacidades
			  			include 'Planilla_GUI/colaboradores.php';       // Colaboradores
			  			include 'Planilla_GUI/pago_salario.php';        // Pagos de salario
			  			include 'Planilla_GUI/aumento_salario.php';     // Aumento salarial
			  			include 'Planilla_GUI/vacaciones.php';          // Generar vacaciones
				   		include 'Planilla_GUI/deducciones.php';         // Generar deduciones
			  			include 'Planilla_GUI/incapacidades.php';       // Generar incapacidad
			  			include 'Planilla_GUI/Liquidaciones.php';       // Generar liquidaciones
			  			include 'Planilla_GUI/aguinaldos.php';          // Generar aguinaldo
					?>
			  	</div>
			  	<!-- END TAB PANES -->
			</div>
		</div>
	</div>
<?php
	include "footer-copyright.php";
	include "bootstrap_script.php";
?>
<script src="js/Planilla/planilla.js"></script>
<script src="js/Planilla/pago_salario.js"></script>
<?php
	include 'footer.php';
?>