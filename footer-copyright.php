<div class="container-fluid footer">
	<ul class="footer-menu">
		<li><a href="despacho.php">Despacho</a></li>
		<li><a href="proveeduria.php">Proveeduría</a></li>
		<li><a href="accounting.php">Contabilidad</a></li>
		<li><a href="bancos.php">Bancos</a></li>
		<li><a href="tesoreria.php">Tesorería</a></li>
		<li><a href="combustible.php">Combustible</a></li>
		<li><a href="planilla.php">Planilla</a></li>
		<li><a href="users.php">Usuarios</a></li>
		<li><a href="parameters.php">Parámetros</a></li>
	</ul>
	<ul>
		<li>
			Innovation Technology
		</li>
		<li>
			Todos los derechos reservados
		</li>
		<li>
			<a href="mailto:soporte@innotecweb.com">soporte@innotecweb.com</a>
		</li>
	</ul>
</div>