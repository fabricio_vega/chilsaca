<div role="tabpanel" class="tab-pane active" id="consume">
	<!-- DATAGRIDVIEW PANEL -->
	<h2 class="text-center" style="color: white">Consumo excesivo</h2>
	<div class="panel panel-warning">
		<div class="panel-heading">
	    	<h3 class="panel-title">
	    		<strong>
	    			<?php echo $SecondPane . ' facturas  con consumo excesivo';?> 
	    		</strong>
	    	</h3>
	  	</div>

	  	<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
	  	<div id="source_code_content" class="tab-content">	
			<div id="tbl_container_demo_grid1" class="table-responsive">
				<table id="list" class="table table-bordered table-hover">
					<!-- TABLE HEAD -->
					<thead>
						<tr id="tbl_demo_grid1_tr_0">
							<th class="th-common">
								Número de factura
							</th>
							<th class="th-common">
								Colaborador
							</th>
							<th class="th-common">
								Bus placa
							</th>
							<th class="th-common">
								En carrera
							</th>
							<th class="th-common">
								Fecha de facturación
							</th>
							<th class="th-common">
								Monto de factura
							</th>
							<th class="th-common">
								Kilómetros recorridos
							</th>
							<th class="th-common">
								Kilómetros por litro
							</th>
						</tr>
					</thead>
					<!-- FINISH TABLE HEAD -->

					<!-- TABLE BODY -->
					<tbody id="tbody-goodtrue-invoices" style="cursor:pointer;">
					</tbody>
					<!-- FINISH TABLE BODY -->
				</table>
			</div>
		</div>
		<!-- FINISH TABLE RESPONSIVE -->
	</div>
	<!-- FINISH DATAGRIDVIEW PANEL -->
</div>