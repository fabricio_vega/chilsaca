<div role="tabpanel" class="tab-pane" id="new_invoice">
	<h2 class="text-center" style="color: white">Nueva factura</h2>
	<!-- REGISTER AND MODIFY REGISTERS -->
	<div class="panel panel-info">
    	<div class="panel-heading">
    		<h3 class="panel-title">
	    		<?php echo $FirstPane;?>
	    		
	    	</h3>
	  	</div>

	  	<div class="panel-body">
	  		<div class="col-xs-12 col-md-4 col-lg-4">
	    		<div class="input-group">
		      		<div class="input-group-addon">Carrera</div>
		      		<select class="form-control" name="typeofinability" id="inability_typeofinability">
  					</select>
		    	</div>
		      	<br />
	    	</div>
	    	<div class="col-xs-12 col-md-4 col-lg-4">
	    		<div class="input-group">
		      		<div class="input-group-addon">Colaborador</div>
		      		<input type="text" id="client-name" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Digite el nombre" disabled>
		    	</div>
		      	<br />
	    	</div>
	    	<div class="col-xs-12 col-md-4 col-lg-4">
	    		<div class="input-group">
		      		<div class="input-group-addon">Kilometros recorridos</div>
		      		<input type="number" id="client-name" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Cantidad de kilometros recorridos" disabled>
		    	</div>
		      	<br />
	    	</div>
	    	<div class="col-xs-12 col-md-4 col-lg-4">
	    		<div class="input-group">
		      		<div class="input-group-addon">Fecha de factura</div>
		      		<input type="date" id="client-phone2" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Fecha de factura">
		    	</div>
		      	<br />
	    	</div>
	    	<div class="col-xs-12 col-md-4 col-lg-4">
	    		<div class="input-group">
		      		<div class="input-group-addon">Litros ingresados</div>
		      		<input type="number" id="client-email" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Cantidad de litros">
		    	</div>
		      	<br />
	    	</div>
	    	<div class="col-xs-12 col-md-4 col-lg-4">
	    		<div class="input-group">
		      		<div class="input-group-addon">Monto de factura</div>
		      		<input type="number" id="client-email" value="" class="form-control" aria-describedby="sizing-addon1" placeholder="Monto de factura">
		    	</div>
		      	<br />
	    	</div>
	    	<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
	  			<div class="col-xs-12 col-md-6 col-lg-6">
	  				<button type="button" id="client-btn-save" class="btn btn-info btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
					  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
						Guardar
					</button>
	  			</div>
	  			<div class="col-xs-12 col-md-6 col-lg-6">
	  				<button type="button" id="client-btn-clear" class="btn btn-dafault btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
					  	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						Limpiar
					</button>
	  			</div>
	  			<div class="col-xs-12 col-md-6 col-lg-6">
	  				<button type="button" id="client-btn-filter" class="btn btn-warning btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
					  	<span class="glyphicon glyphicon-filte" aria-hidden="true"></span>
						Filtrar
					</button>
	  			</div>
	  		</div>
	  	</div>
	</div>
	<!-- FINISH REGISTER AND MODIFY REGISTERS -->

  	<!-- DATAGRIDVIEW PANEL -->
	<div class="panel panel-warning">
		<div class="panel-heading">
	    	<h3 class="panel-title"><strong><?php echo $SecondPane . 'facturas'; ?></strong></h3>
	  	</div>

	  	<!-- IMPORTANT: IT ALLOWS THE TABLE TO BE RESPONSIVE -->
	  	<div id="source_code_content" class="tab-content">	
			<div id="tbl_container_demo_grid1" class="table-responsive">
				<table id="list" class="table table-bordered table-hover">
					<!-- TABLE HEAD -->
					<thead>
						<tr id="tbl_demo_grid1_tr_0">
							<th class="th-common">
								Cédula
							</th>
							<th class="th-common">
								Nombre
							</th>
							<th class="th-common">
								Apellidos
							</th>
							<th class="th-common">
								Teléfono 1
							</th>
							<th class="th-common">
								Teléfono 2
							</th>
							<th class="th-common">
								Correo electrónico
							</th>
							<th class="th-common">
								Ubicación
							</th>
							<th class="th-common">
								Monto de crédito
							</th>
							<th class="th-common">
								Días de crédito
							</th>
							<th class="th-common">
								Días de gracia
							</th>
						</tr>
					</thead>
					<!-- FINISH TABLE HEAD -->
					<!-- TABLE BODY -->
					<tbody id="tbody-client" style="cursor:pointer;">
						
					</tbody>
					<!-- FINISH TABLE BODY -->
				</table>

				<div class="col-xs-12 col-md-8 col-md-offset-2 accountsreceivable-clients-buttons">
		  			<div class="col-xs-12 col-md-6 col-lg-6">
		  				<button type="button" id="client-btn-modify" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
						  	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
							Modificar
						</button>
		  			</div>
		  			<div class="col-xs-12 col-md-6 col-lg-6">
		  				<button type="button" id="client-btn-delete" class="btn btn-danger btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#searchInvoice">
						  	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							Eliminar
						</button>
		  			</div>
		  		</div>
			</div>
		</div>
		<!-- FINISH TABLE RESPONSIVE -->
	</div>
	<!-- FINISH DATAGRIDVIEW PANEL -->
</div>