<!--Barra de arriba estática-->
	<div class="container-fluid session">
		<div class="row">
			<div class="col-md-8">
				<h4 class="text-left logo_type">
					InnotecSystem
				</h4>
			</div>

			<!--pendiente de revisión-->
			<div class="col-md-4 text-right">
				<button type="button" class="btn session-bottom" data-toggle="modal" data-target="#myModal">
				  	<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
				</button>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade options" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  	<div class="modal-dialog" role="document">
	    	<div class="modal-content">
		      	<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		        		<span aria-hidden="true">&times;</span>
		        	</button>
		        	<h4 class="modal-title" id="myModalLabel">Conectado como: <em>fvega</em></h4>
		      	</div>
		      	<div class="modal-body">
		        	<button type="button" class="btn btn-primary" aria-label="Left Align" onclick="function">
					  	<span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
					  	Cambiar mi contraseña
					</button>
					<br />
					<br />
					<form action="" class="text-center hidden">
						<div class="input-group">
						  	<span class="input-group-addon glyphicon glyphicon-lock" id="basic-addon1"></span>
						  	<input type="password" class="form-control" placeholder="Contraseña actual" aria-describedby="basic-addon1">
						</div>
						<br>
						<div class="input-group">
						  	<span class="input-group-addon glyphicon glyphicon-refresh" id="basic-addon1"></span>
						  	<input type="password" class="form-control" placeholder="Contraseña nueva" aria-describedby="basic-addon1">
						</div>
						<br>
						<div class="input-group">
						  	<button type="button" class="btn btn-success">
						  		<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
						  		Cambiar
						  	</button>
						</div>
					</form>
					<br /><br />
					<li class="list-group-item list-group-item-danger">
				    	<span class="badge">14</span>
				    	Registros realizados durante la sesión
				  	</li>
				  	<li class="list-group-item list-group-item-info">
				    	<span class="badge">10</span>
				    	Carreras actuales del día
				  	</li>
				  	<li class="list-group-item list-group-item-success">
				    	<span class="badge">4</span>
				    	Carreras pendientes de aplicar
				  	</li>
				  	<li class="list-group-item list-group-item-warning">
				    	<span class="badge">10,325,030.00</span>
				    	Total de ingresos a la fecha 
				  	</li>
				  	<li class="list-group-item list-group-item-default">
				    	<span class="badge">2,500,350.00</span>
				    	Total de gastos a la fecha 
				  	</li>
				  	<li class="list-group-item list-group-item-danger">
				    	<span class="badge">2</span>
				    	Licencias a punto de vencer 
				  	</li>
		      	</div>
		      	<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		        	<button type="button" class="btn btn-danger" onClick="location.href='http://localhost/chilsaca'">Salir del programa</button>
		      	</div>
	    	</div>
	  	</div>
	</div>
	<!-- FINISH MODAL -->